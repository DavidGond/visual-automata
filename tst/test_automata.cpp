#include <fstream>
#include "test_automata.hpp"
#include "state.hpp"
#include "transition.hpp"

#define TESTCASE(VALUE, EXPECTED) (VALUE == EXPECTED ? "\033[32;01mPASSED\033[00m\n": "\033[31;01mFAILED\033[00m\n")


using namespace VisualAutomata;

bool test_states_count(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	size_t count = automata.getStateCount();
	printf(TESTCASE(2, count));
	return 2 == count;
}

bool test_states_name(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	const State &state0 = *automata.getState(automata.getId(0));
	string name0 = state0.getLabel();
	const State &state1 = *automata.getState(automata.getId(1));
	string name1 = state1.getLabel();
	bool test = name0 == "s0" && name1 == "s1";
	printf(TESTCASE(true, test));
	return test;
}

bool test_transitions_count(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	size_t count = automata.getTransitionCount();
	printf(TESTCASE(5, count));
	return 5 == count;
}

bool test_transitions_source_destination(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	map<int, Transition *> transitions = automata.getTransitions();
	int count[2][2] = {{0, 0}, {0, 0}};
	for (auto &transition : transitions) {
		count[automata.getJsonId(transition.second
		                                   ->getSource())][automata.getJsonId(transition.second
		                                                                                ->getDestination())]++;
	}
	bool test = count[0][0] == 2 && count[0][1] == 1 && count[1][0] == 0 && count[1][1] == 2;
	printf(TESTCASE(true, test));
	return test;
}

bool test_groups_count(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	size_t count = automata.getGroupCount();
	printf(TESTCASE(1, count));
	return 1 == count;
}

bool test_groups_name(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	StateGroup &group = automata.getGroup(automata.getId(2));
	string name = group.getLabel();
	printf(TESTCASE("group", name));
	return name == "group";
}

bool test_groups_states(Automata &automata) {
	printf("%s", __func__);
	printf("\t");
	StateGroup &group = automata.getGroup(automata.getId(2));
	set<State *> states = group.getStates();
	int count[2] = {0, 0};
	for (auto state : states) {
		count[automata.getJsonId(*state)]++;
	}
	bool test = count[0] == 1 && count[1] == 1;
	printf(TESTCASE(true, test));
	return test;
}

void TestAutomata::test() {
	std::unique_ptr<Automata> automata = Parser::parse(std::string("fsm-json.json"));
	test_states_count(*automata);
	test_states_name(*automata);
	test_transitions_count(*automata);
	test_transitions_source_destination(*automata);
	test_groups_count(*automata);
	test_groups_name(*automata);
	test_groups_states(*automata);
	Parser::freeParser();
}
