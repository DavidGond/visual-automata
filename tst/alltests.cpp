#include <cstdlib>

#include "test_automata.hpp"
#include "test_json_writing.hpp"


void exec_test(int &countTest, void (*func)()) {
	countTest++;
	func();
}

int main() {

	int countTest = 0;
	printf("\n\n==== Testing Automata ====\n\n");
	exec_test(countTest, &TestAutomata::test);
	printf("\n\n==== Testing Writer ====\n\n");
	exec_test(countTest, &TestWriting::test);

	return EXIT_SUCCESS;
}
