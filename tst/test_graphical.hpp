#ifndef VISUAL_AUTOMATA_TEST_GRAPHICAL_HPP
#define VISUAL_AUTOMATA_TEST_GRAPHICAL_HPP

#include <QtWidgets>
#include <QtTest/QtTest>
#include <QApplication>
#include <style.hpp>
#include "../src/graphical_window/color_picker_widget.hpp"
#include "../src/graphical_window/preference_window.hpp"


class TestGraphical: public QObject {
Q_OBJECT
private slots:
	static void initTestCase();
	static void test_line_edit();
	static void test_color_picker();
	static void test_preference_window();
	void cleanupTestCase();
};

#endif
