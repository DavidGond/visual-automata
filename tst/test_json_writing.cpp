#include <cstdio>
#include <utils.hpp>
#include "test_json_writing.hpp"

#define TESTCASE(VALUE, EXPECTED) (VALUE == EXPECTED ? "\033[32;01mPASSED\033[00m\n": "\033[31;01mFAILED\033[00m\n")

using namespace std;
using namespace VisualAutomata;

bool testName() {
	printf("%s", __func__);
	printf("\t");
	Automata a;
	a.setLabel("test_automata");
	string aStr = removeSpace(Writer::writePrint(a));
	string resStr;
	resStr = "{\"format\":\n"
	         "{\"name\":\"test_automata\"}\n"
	         "}";
	resStr = removeSpace(resStr);
	std::cout << resStr << "EEE"<< endl;
	std::cout << aStr << "DDD"<< endl;
	printf(TESTCASE(aStr, resStr));
	return aStr == resStr;
}

bool testStates() {
	printf("%s", __func__);
	printf("\t");
	Automata a;
	State s1;
	State s2;
	State s3;
	s1.setLabel("State1");
	s2.setLabel("State2");
	s3.setLabel("State3");
	s1.setInitial(true);
	s2.setFinal(true);
	a.addState(s1);
	a.addState(s2);
	a.addState(s3);
	string aStr = removeSpace(Writer::writePrint(a));

	stringstream resStream;
	resStream << "{\"data\":" << endl;
	resStream << "{\"states\":[" << endl;
	resStream << "{\"id\":" << s1.getId() << R"(, "name":"State1", "initial":1},)" << endl;
	resStream << "{\"id\":" << s2.getId() << R"(, "name":"State2", "final":1},)" << endl;
	resStream << "{\"id\":" << s3.getId() << R"(, "name":"State3"}])" << endl;
	resStream << "}" << endl;
	resStream << "}";

	std::string resStr = removeSpace(resStream.str());
	printf(TESTCASE(aStr, resStr));
	return aStr == resStr;
}

bool testGroup() {
	printf("%s", __func__);
	printf("\t");
	Automata a;
	State s1;
	State s2;
	State s3;

	StateGroup g1;
	StateGroup g2;

	s1.setLabel("State1");
	s2.setLabel("State2");
	s3.setLabel("State3");

	s1.setInitial(true);
	s2.setFinal(true);

	a.addState(s1);
	a.addState(s2);
	a.addState(s3);

	g1.setLabel("Group1");
	g2.setLabel("Group2");

	a.addGroup(g1);
	a.addGroup(g2);

	VisualAutomata::Automata::addStateToGroup(g1, s1);
	VisualAutomata::Automata::addStateToGroup(g2, s2);
	VisualAutomata::Automata::addStateToGroup(g2, s3);

	string aStr = removeSpace(Writer::writePrint(a));

	stringstream resStream;
	resStream << "{\"data\":" << endl;
	resStream << "{\"states\":[" << endl;
	resStream << "{\"id\":" << s1.getId() << R"(, "name":"State1", "initial":1},)" << endl;
	resStream << "{\"id\":" << s2.getId() << R"(, "name":"State2", "final":1},)" << endl;
	resStream << "{\"id\":" << s3.getId() << R"(, "name":"State3"}],)" << endl;
	resStream << "\"groups\":[" << endl;
	resStream << "{\"id\":" << g1.getId() << R"(, "name":"Group1", "states":[)" << s1.getId()
	          << "]}," << endl;
	resStream << "{\"id\":" << g2.getId() << R"(, "name":"Group2", "states":[)" << s2.getId() << ","
	          << s3.getId() << "]}]" << endl;
	resStream << "}" << endl;
	resStream << "}";

	string resStr = removeSpace(resStream.str());
	printf(TESTCASE(aStr, resStr));
	return aStr == resStr;
}

bool testTransition() {
	printf("%s", __func__);
	printf("\t");
	Automata a;
	State s1;
	State s2;
	State s3;
	s1.setLabel("State1");
	s2.setLabel("State2");
	s3.setLabel("State3");

	a.addState(s1);
	a.addState(s2);
	a.addState(s3);


	Transition t1(s1, s2, "Transition1");
	Transition t2(s1, s3, "Transition2");
	Transition t3(s3, s1, "Transition3");

	a.addTransition(t1);
	a.addTransition(t2);
	a.addTransition(t3);

	string aStr = removeSpace(Writer::writePrint(a));

	stringstream resStream;
	resStream << "{\"data\":" << endl;
	resStream << "{\"states\":[" << endl;
	resStream << "{\"id\":" << s1.getId() << R"(, "name":"State1"},)" << endl;
	resStream << "{\"id\":" << s2.getId() << R"(, "name":"State2"},)" << endl;
	resStream << "{\"id\":" << s3.getId() << R"(, "name":"State3"}],)" << endl;
	resStream << "\"transitions\":[" << endl;
	resStream << "{\"source\":" << s1.getId() << ", \"destination\":" << s2.getId()
	          << R"(, "label":"Transition1"},)" << endl;
	resStream << "{\"source\":" << s1.getId() << ", \"destination\":" << s3.getId()
	          << R"(, "label":"Transition2"},)" << endl;
	resStream << "{\"source\":" << s3.getId() << ", \"destination\":" << s1.getId()
	          << R"(, "label":"Transition3"}])" << endl;
	resStream << "}" << endl;
	resStream << "}";

	string resStr = removeSpace(resStream.str());
	printf(TESTCASE(aStr, resStr));
	return aStr == resStr;
}

void TestWriting::test() {
	//testName();
	testStates();
	testGroup();
	testTransition();
}
