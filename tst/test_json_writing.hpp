#ifndef VISUAL_AUTOMATA_TEST_JSON_WRITING_HPP
#define VISUAL_AUTOMATA_TEST_JSON_WRITING_HPP

#include <cstdio>
#include <cassert>

#include "automata.hpp"
#include "parser.hpp"
#include "writer.hpp"
#include "utils.hpp"


class TestWriting {
public:
	static void test();
};

#endif
