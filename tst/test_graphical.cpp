#include "test_graphical.hpp"

using namespace VisualAutomata;

QApplication *app;

int argc = 1;

char *appname = (char *) "Apptest";

char *argv[1] = {appname};

void TestGraphical::test_line_edit() {
	QLineEdit lineEdit;

	QTest::keyClicks(&lineEdit, "hello world");

	QCOMPARE(lineEdit.text(), QString("hello world"));
}

void TestGraphical::test_color_picker() {
	// QSKIP("Skip Color Picker");
	auto *parent = new QWidget();
	auto *colorPicker = new ColorPickerWidget(parent);

	auto *color = new QColor(Qt::cyan);
	colorPicker->setCurrentColor(*color);

	QCOMPARE(colorPicker->getCurrentColor(), *color);

	delete color;
	delete colorPicker;
	delete parent;
}

void TestGraphical::test_preference_window() {
	// QSKIP("Skip Preference Window");
	auto *pref = new PreferenceWindow();
	ColorPickerWidget *stateColorPicker = pref->getStateColorPicker();
	QVERIFY(stateColorPicker != nullptr);

	delete pref;
}

void TestGraphical::initTestCase() {

}

void TestGraphical::cleanupTestCase() {

}

QTEST_MAIN(TestGraphical)

#include "test_graphical.moc"
