#ifndef VISUAL_AUTOMATA_TEST_AUTOMATA_HPP
#define VISUAL_AUTOMATA_TEST_AUTOMATA_HPP

#include <cstdio>
#include <cassert>

#include "../src/model/automata.hpp"
#include "parser.hpp"


class TestAutomata {
public:
	static void test();
};

#endif
