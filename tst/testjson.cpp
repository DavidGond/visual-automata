/**
 * Simple file used to gather all json tests
 * Duplicates alltests in order to be more ergonomic
 */

#include <cstdlib>

#include "test_automata.hpp"
#include "test_json_writing.hpp"

int main() {
	TestAutomata::test();
	TestWriting::test();
	return EXIT_SUCCESS;
}
