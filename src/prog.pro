TEMPLATE = app

INCLUDEPATH += . .. graphical_window/ viewport/ model/ json/

HEADERS += \
    main_window.hpp \
    automata_controller.hpp \

QT += widgets

SOURCES += \
    main_window.cpp \
    automata_controller.cpp \

LIBS += -Lgraphical_window -Lviewport -Lmodel -lviewport -lgraphical_window -lmodel

TARGET = prog
