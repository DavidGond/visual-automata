#include "parser.hpp"
#include <fstream>

namespace VisualAutomata {

vector<State *> Parser::heapStates = vector<State *>();

vector<Transition *> Parser::heapTransitions = vector<Transition *>();

vector<StateGroup *> Parser::heapGroups = vector<StateGroup *>();

std::unique_ptr<Automata> Parser::parse(const string &s) {
	std::unique_ptr<Automata> automata = std::make_unique<Automata>();
	jsObject *object = stringToObject(s);
	parseAutomata(*automata, object);
	delete object;
	return automata;
}

jsObject *Parser::stringToObject(const string &s) {
	ifstream ifs(s, ios::in);
	return jsObject::parse(ifs);
}

void Parser::parseAutomata(Automata &automata, jsObject *object) {
	unordered_map<string, jsValue *> fields = object->fields;
	for (auto &field : fields) {
		if (field.first == "context") {
			parseContext(automata, field.second);
		} else if (field.first == "metadata") {
			parseMetadata(automata, field.second);
		} else if (field.first == "format") {
			parseFormat(automata, field.second);
		} else if (field.first == "kind") {
			parseKind(automata, field.second);
		} else if (field.first == "data") {
			parseData(automata, field.second);
		} else if (field.first == "style") {
			parseStyle(automata, field.second);
		}
	}
}

void Parser::parseData(Automata &automata, jsValue *value) {
	auto *object = dynamic_cast<jsObject *>(value);
	unordered_map<string, jsValue *> fields = object->fields;
	auto states = fields.find("states");
	// parse the states before the transitions and the groups
	if (states != fields.end()) {
		parseStates(automata, fields["states"]);
	}
	for (auto &field : fields) {
		if (field.first == "transitions") {
			parseTransitions(automata, field.second);
		} else if (field.first == "groups") {
			parseGroups(automata, field.second);
		}
	}
}

void Parser::parseStates(Automata &automata, jsValue *value) {
	auto *array = dynamic_cast<jsArray *>(value);
	std::vector<jsValue *> values = array->values;
	for (auto &val : values) {
		parseState(automata, val);
	}
}

void Parser::parseState(Automata &automata, jsValue *value) {
	auto *object = dynamic_cast<jsObject *>(value);
	unordered_map<string, jsValue *> fields = object->fields;
	auto *state = new State();
	for (auto &field : fields) {
		if (field.first == "id") {
			parseId(automata, *state, field.second);
		} else if (field.first == "name") {
			parseLabel(*state, field.second);
		} else if (field.first == "initial") {
			parseInitial(*state, field.second);
		} else if (field.first == "final") {
			parseFinal(*state, field.second);
		} else if (field.first == "style") {
			parseStyle(*state, field.second);
		}
	}
	automata.addState(*state);
	heapStates.insert(heapStates.begin(), state);
}

void Parser::parseId(Automata &automata, AutomataObject &object, jsValue *value) {
	auto *val = dynamic_cast<jsInt *>(value);
	automata.addJsonId(object.getId(), val->value);
}

void Parser::parseLabel(AutomataObject &object, jsValue *value) {
	auto *val = dynamic_cast<jsString *>(value);
	object.setLabel(val->value);
}

void Parser::parseStyle(AutomataObject &object, jsValue *value) {
	auto *obj = dynamic_cast<jsObject *>(value);
	unordered_map<string, jsValue *> fields = obj->fields;
	string styleStr;
	stringstream ss = stringstream(styleStr);
	for (auto &field : fields) {
		auto *val = dynamic_cast<jsString *>(field.second);
		if (!ss.str().empty()) {
			ss << ';';
		}
		ss << field.first << ':' << val->value;
	}
	object.setStyle(ss.str());
}

void Parser::parseInitial(State &state, jsValue *value) {
	if (value->type() == jsValue::INTEGER) {
		auto *val = dynamic_cast<jsInt *>(value);
		if (val->value) {
			state.setInitial(true);
		}
	} else if (value->type() == jsValue::BOOLEAN) {
		auto *val = dynamic_cast<jsCst *>(value);
		if (val->value) {
			state.setInitial(true);
		}
	}
}

void Parser::parseFinal(State &state, jsValue *value) {
	if (value->type() == jsValue::INTEGER) {
		auto *val = dynamic_cast<jsInt *>(value);
		if (val->value) {
			state.setFinal(true);
		}
	} else if (value->type() == jsValue::BOOLEAN) {
		auto *val = dynamic_cast<jsCst *>(value);
		if (val->value) {
			state.setFinal(true);
		}
	}
}

void Parser::parseTransitions(Automata &automata, jsValue *value) {
	auto *array = dynamic_cast<jsArray *>(value);
	std::vector<jsValue *> values = array->values;
	for (auto &val : values) {
		parseTransition(automata, val);
	}
}

void Parser::parseTransition(Automata &automata, jsValue *value) {
	auto *object = dynamic_cast<jsObject *>(value);
	unordered_map<string, jsValue *> fields = object->fields;
	State *source, *dest;
	std::string label;
	std::string weight;
	for (auto &field : fields) {
		if (field.first == "source") {
			auto *val = dynamic_cast<jsInt *>(field.second);
			source = automata.getState(automata.getId(val->value));
		} else if (field.first == "destination") {
			auto *val = dynamic_cast<jsInt *>(field.second);
			dest = automata.getState(automata.getId(val->value));
		} else if (field.first == "label" && field.second->type() == jsValue::STRING) {
			auto *val = dynamic_cast<jsString *>(field.second);
			label = val->value;
		}

	}
	auto *transition = new Transition(*source, *dest, label);
	for (auto &field : fields) {
		if (field.first == "weight") {
			parseWeight(*transition, field.second);
		} else if (field.first == "style") {
			parseStyle(*transition, field.second);
		}
	}
	automata.addTransition(*transition);
	heapTransitions.insert(heapTransitions.begin(), transition);
}

void Parser::parseWeight(Transition &transition, jsValue *value) {
	if (value->type() == jsValue::INTEGER) {
		auto *val = dynamic_cast<jsInt *>(value);
		transition.setWeight(std::to_string(val->value));
		return;
	} else if (value->type() == jsValue::STRING) {
		auto *val = dynamic_cast<jsString *>(value);
		transition.setWeight(val->value);
	}

}

void Parser::parseGroups(Automata &automata, jsValue *value) {
	auto *array = dynamic_cast<jsArray *>(value);
	std::vector<jsValue *> values = array->values;
	for (auto &val : values) {
		parseGroup(automata, val);
	}
}

void Parser::parseGroup(Automata &automata, jsValue *value) {
	auto *object = dynamic_cast<jsObject *>(value);
	unordered_map<string, jsValue *> fields = object->fields;
	auto *group = new StateGroup();
	for (auto &field : fields) {
		if (field.first == "id") {
			parseId(automata, *group, field.second);
		} else if (field.first == "name") {
			parseLabel(*group, field.second);
		} else if (field.first == "states") {
			addStatesToGroup(automata, *group, field.second);
		} else if (field.first == "style") {
			parseStyle(*group, field.second);
		}
	}
	automata.addGroup(*group);
	heapGroups.insert(heapGroups.begin(), group);
}

void Parser::addStatesToGroup(Automata &automata, StateGroup &group, jsValue *value) {
	auto *array = dynamic_cast<jsArray *>(value);
	std::vector<jsValue *> values = array->values;
	for (auto &val : values) {
		addStateToGroup(automata, group, val);
	}
}

void Parser::addStateToGroup(Automata &automata, StateGroup &group, jsValue *value) {
	auto *val = dynamic_cast<jsInt *>(value);
	State *state = automata.getState(automata.getId(val->value));
	if (state != nullptr) {
		group.addState(*state);
	}
}

void Parser::parseMetadata(Automata &automata, jsValue *value) {
	(void) automata;
	(void) value;
}

void Parser::parseFormat(Automata &automata, jsValue *value) {
	(void) automata;
	(void) value;
}

void Parser::parseKind(Automata &automata, jsValue *value) {
	(void) automata;
	(void) value;
}

void Parser::parseContext(Automata &automata, jsValue *value) {
	(void) automata;
	(void) value;
}

template<typename T, typename std::enable_if<std::is_base_of<AutomataObject,
                                                             T>::value>::type * = nullptr>
void remove(vector<T *> vector) {
	for (T *field : vector) {
		delete field;
	}
}

void Parser::freeParser() {
	remove<State>(heapStates);
	heapStates = vector<State *>();
	remove<Transition>(heapTransitions);
	heapTransitions = vector<Transition *>();
	remove<StateGroup>(heapGroups);
	heapGroups = vector<StateGroup *>();
}
}  // namespace VisualAutomata
