// This file is part of Awali.
// Copyright 2016-2021 Sylvain Lombardy, Victor Marsault, Jacques Sakarovitch
//
// Awali is a free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
#ifndef AWALI_JSON_CC
#define AWALI_JSON_CC

#include "json.hpp"

namespace Awali {

jsObject *jsValue::object() {
	throw std::runtime_error("This jsObject is not a jsObject");
}

unsigned jsInt::type() const {
	return jsValue::INTEGER;
}

jsObject *jsObject::object() {
	return this;
}

unsigned jsFloat::type() const {
	return jsValue::FLOATING;
}

jsValue *jsFloat::parse(std::istream &i) {
	double y;
	i >> y;
	if (std::floor(y) == y) {
		return new jsInt((int) y);
	}
	return new jsFloat(y);
}

unsigned jsCst::type() const {
	if (value == -1) {
		return jsValue::TNULL;
	} else {
		return jsValue::BOOLEAN;
	}
}

jsCst *jsCst::parse(std::istream &i) {
	static const char *v[] = {"null", "false", "true"};
	char c;
	int k, l = 0;
	i >> c;
	switch (c) {
	case 'n' : k = 0;
		break;
	case 'f' : k = 1;
		break;
	case 't' : k = 2;
		break;
	default : k = 3;
	}
	if (k < 3) {
		for (l = 1; v[k][l] != '\0'; l++) {
			i >> c;
			if (v[k][l] != c) {
				break;
			}
		}
	}
	if (k == 3 || v[k][l] != '\0') {
		raiseParse(i, "json parsing");
	}
	return new jsCst(k - 1);
}

unsigned jsString::type() const {
	return jsValue::STRING;
}

jsString *jsString::parse(std::istream &i) {
	return new jsString(Internal::parseString(i));
}

unsigned jsArray::type() const {
	return jsValue::ARRAY;
}

jsArray *jsArray::parse(std::istream &i) {
	auto *a = new jsArray();
	Internal::check(i, '[');
	if (Internal::peek(i) == ']') {
		Internal::check(i, ']');
		return a;
	}
	while (true) {
		a->values.emplace_back(jsValue::parse(i));
		if (Internal::peek(i) != ',') {
			break;
		}
		Internal::check(i, ',');
	}
	Internal::check(i, ']');
	return a;
}

jsArray::~jsArray() {
	for (jsValue *p : values)
		delete p;
}

bool jsObject::hasField(const std::string &key) const {
	return fields.find(key) != fields.end();
}

jsValue *jsObject::value(const std::string &key) {
	return fields.find(key)->second;
}

jsObject *jsObject::object(const std::string &key) {
	return value(key)->object();
}

std::vector<jsValue *> &jsObject::vect(const std::string &key) {
	return dynamic_cast<jsArray *>(value(key))->values;
}

int jsObject::integer(const std::string &key) {
	auto *ji = dynamic_cast<jsInt *>(value(key));
	if (ji != nullptr) {
		return ji->value;
	}
	auto *js = dynamic_cast<jsString *>(value(key));
	return std::stoi(js->value);
}

std::string &jsObject::str(const std::string &key) {
	return dynamic_cast<jsString *>(value(key))->value;
}

unsigned jsObject::type() const {
	return jsValue::OBJECT;
}

jsObject *jsObject::parse(std::istream &i) {
	auto *o = new jsObject();
	Internal::check(i, '{');
	if (Internal::peek(i) == '}') {
		return o;
	}
	while (true) {
		std::string k(Internal::parseString(i));
		Internal::check(i, ':');
		jsValue *child = jsValue::parse(i);
		o->fields.emplace(std::make_pair(k, child));
		if (Internal::peek(i) != ',') {
			break;
		}
		Internal::check(i, ',');
	}
	Internal::check(i, '}');
	return o;
}

jsObject::~jsObject() {
	for (const auto &p : fields)
		delete p.second;
}

jsValue *jsValue::parse(std::istream &i) {
	char c;
	switch (c = Internal::peek(i)) {
	case '{' : return jsObject::parse(i);
	case '[' : return jsArray::parse(i);
	case '"' : return jsString::parse(i);
	default :
		if ((c >= '0' && c <= '9') || c == '-' || c == '.') {
			return jsFloat::parse(i);
		} else {
			return jsCst::parse(i);
		}
	}
}

jsValue *parse(std::istream &i) {
	return jsValue::parse(i);
}
}  // namespace Awali

#endif
