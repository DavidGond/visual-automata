#include "writer.hpp"
#include "utils.hpp"
#include <fstream>

namespace VisualAutomata {

int Writer::tabulationCount = 0;

bool Writer::comma = false;

string Writer::writePrint(const Automata &automata) {
	ostringstream stream = ostringstream(ios::out);
	writeAutomata(automata, stream);
	return stream.str();
}

void Writer::writeSave(const Automata &automata, const string &savePath) {
	ofstream stream;
	stream.open(savePath, ofstream::out | ofstream::trunc);
	writeAutomata(automata, stream);
	stream.close();
}

void Writer::lineBreak(ostream &stream) {
	stream << endl;
	for (int i = 0; i < tabulationCount; i++) {
		stream << "\t";
	}
}

void Writer::writeAutomata(const Automata &automata, ostream &stream) {
	stream << "{";
	comma = false;
	writeFormat(automata, stream);
	writeKind(automata, stream);
	writeMetadata(automata, stream);
	writeContext(automata, stream);
	writeData(automata, stream);
	writeStyle(automata, stream);
	lineBreak(stream);
	stream << "}";
	comma = true;
}

void Writer::writeData(const Automata &automata, ostream &stream) {
	stream << "\"data\":";
	tabulationCount++;
	lineBreak(stream);
	stream << "{";
	comma = false;
	writeStates(automata, stream);
	writeTransitions(automata, stream);
	writeGroups(automata, stream);
	lineBreak(stream);
	stream << "}";
	tabulationCount--;
	comma = true;
}

void Writer::writeStates(const Automata &automata, ostream &stream) {
	if (automata.getStateCount()) {
		if (comma) {
			stream << ",";
			lineBreak(stream);
		}
		stream << "\"states\":[";
		tabulationCount++;
		comma = false;
		lineBreak(stream);
		map<int, State *> states = automata.getStates();
		for (auto &val : states) {
			writeState(*val.second, stream);
		}
		stream << "]";
		tabulationCount--;
		comma = true;
	}
}

void Writer::writeState(const State &state, ostream &stream) {
	if (comma) {
		stream << ",";
		lineBreak(stream);
	}
	stream << "{";
	comma = false;
	writeId(state, stream);
	writeLabel(state, stream, "name");
	if (state.isFinal()) {
		writeFinal(stream);
	}
	if (state.isInitial()) {
		writeInitial(stream);
	}
	writeStyle(state, stream);
	stream << "}";
	comma = true;
}

void Writer::writeId(const AutomataObject &object, ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	stream << "\"id\":";
	stream << object.getId();
	comma = true;
}

void Writer::writeLabel(const AutomataObject &object, ostream &stream, const string &s) {
	if (!object.getLabel().empty()) {
		if (comma) {
			stream << ", ";
		}
		stream << "\"" << s << "\":";
		stream << "\"" << object.getLabel() << "\"";
		comma = true;
	}
}

void Writer::writeStyleAttribute(const string &attribute, ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	vector<string> value = split(attribute, ':');
	if (value.size() >= 2) {
		stream << "\"" << value.at(0) << "\":\"" << value.at(1) << "\"";
	}
	comma = true;
}

void Writer::writeStyle(const AutomataObject &object, ostream &stream) {
	if (!object.getStyle().isEmpty()) {
		if (comma) {
			stream << ", ";
		}
		stream << "\"style\":{";
		comma = false;
		vector<string> attributes = split(object.getStyle().getStr(), ';');
		for (auto &attribute : attributes) {
			writeStyleAttribute(attribute, stream);
		}
		stream << "}";
		comma = true;
	}
}

void Writer::writeInitial(ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	stream << "\"initial\":1";
	comma = true;
}

void Writer::writeFinal(ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	stream << "\"final\":1";
	comma = true;
}

void Writer::writeTransitions(const Automata &automata, ostream &stream) {
	if (automata.getTransitionCount()) {
		if (comma) {
			stream << ",";
			lineBreak(stream);
		}
		stream << "\"transitions\":[";
		tabulationCount++;
		comma = false;
		lineBreak(stream);
		map<int, Transition *> transitions = automata.getTransitions();
		for (auto &val : transitions) {
			writeTransition(*val.second, stream);
		}
		stream << "]";
		tabulationCount--;
		comma = true;
	}
}

void Writer::writeTransition(const Transition &transition, ostream &stream) {
	if (comma) {
		stream << ",";
		lineBreak(stream);
	}
	stream << "{";
	comma = false;
	writeSource(transition, stream);
	writeDestination(transition, stream);
	writeLabel(transition, stream, "label");
	writeWeight(transition, stream);
	writeStyle(transition, stream);
	stream << "}";
	comma = true;
}

void Writer::writeSource(const Transition &transition, ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	stream << "\"source\":";
	stream << transition.getSource().getId();
	comma = true;
}

void Writer::writeDestination(const Transition &transition, ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	stream << "\"destination\":";
	stream << transition.getDestination().getId();
	comma = true;
}

void Writer::writeWeight(const Transition &transition, ostream &stream) {
	if (!transition.getWeight().empty()) {
		if (comma) {
			stream << ", ";
		}
		stream << "\"weight\":";
		stream << transition.getWeight();
		comma = true;
	}
}

void Writer::writeGroups(const Automata &automata, ostream &stream) {
	if (automata.getGroupCount()) {
		if (comma) {
			stream << ",";
			lineBreak(stream);
		}
		stream << "\"groups\":[";
		comma = false;
		tabulationCount++;
		lineBreak(stream);
		map<int, StateGroup *> groups = automata.getGroups();
		for (auto &val : groups) {
			writeGroup(*val.second, stream);
		}
		stream << "]";
		tabulationCount--;
		comma = true;
	}
}

void Writer::writeGroup(const StateGroup &group, ostream &stream) {
	if (comma) {
		stream << ",";
		lineBreak(stream);
	}
	stream << "{";
	comma = false;
	writeId(group, stream);
	writeLabel(group, stream, "name");
	writeStatesToGroup(group, stream);
	writeStyle(group, stream);
	stream << "}";
	comma = true;
}

void Writer::writeStatesToGroup(const StateGroup &group, ostream &stream) {
	if (comma) {
		stream << ", ";
	}
	stream << "\"states\":[";
	comma = false;
	std::set<State *> states = group.getStates();
	for (auto &state : states) {
		writeStateToGroup(*state, stream);
	}
	stream << "]";
	comma = true;
}

void Writer::writeStateToGroup(const State &state, ostream &stream) {
	if (comma) {
		stream << ",";
	}
	stream << state.getId();
	comma = true;
}

void Writer::writeMetadata(const Automata &automata, ostream &stream) {
	(void) automata;
	(void) stream;
}

void Writer::writeFormat(const Automata &automata, ostream &stream) {
	(void) automata;
	(void) stream;
}

void Writer::writeKind(const Automata &automata, ostream &stream) {
	(void) automata;
	(void) stream;
}

void Writer::writeContext(const Automata &automata, ostream &stream) {
	(void) automata;
	(void) stream;
}
}  // namespace VisualAutomata
