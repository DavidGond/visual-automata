// This file is part of Awali.
// Copyright 2016-2021 Sylvain Lombardy, Victor Marsault, Jacques Sakarovitch
//
// Awali is a free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AWALI_JSON_HH
#define AWALI_JSON_HH

#include"json_utils.hpp"
#include <utility>
#include<vector>
#include<unordered_map>
#include<cmath>

namespace Awali {

struct jsInt;
struct jsFloat;
struct jsObject;

struct jsValue {

	enum Type {
		OBJECT, ARRAY, INTEGER, FLOATING, STRING, BOOLEAN, TNULL
	};

	virtual unsigned type() const = 0;

	static jsValue *parse(std::istream &i);

	virtual ~jsValue() = default;

	virtual jsObject *object();
};

struct jsInt: jsValue {
	int value;

	explicit jsInt(int v) : value(v) {
	}

	unsigned type() const override;

	~jsInt() override = default;
};

struct jsFloat: jsValue {
	double value;

	explicit jsFloat(double v) : value(v) {
	}

	unsigned type() const override;

	~jsFloat() override = default;

	static jsValue *parse(std::istream &i);
};

struct jsCst: jsValue {
	int value;

	explicit jsCst(int v) : value(v) {
	}

	unsigned type() const override;

	~jsCst() override = default;

	static jsCst *parse(std::istream &i);
};

struct jsString: jsValue {
	std::string value;

	explicit jsString(std::string v) : value(std::move(v)) {
	}

	unsigned type() const override;

	static jsString *parse(std::istream &i);

	~jsString() override = default;
};

struct jsArray: jsValue {
	std::vector<jsValue *> values;

	unsigned type() const override;

	static jsArray *parse(std::istream &i);

	~jsArray() override;
};

struct jsObject: jsValue {
	std::unordered_map<std::string, jsValue *> fields;

	bool hasField(const std::string &key) const;

	jsValue *value(const std::string &key);

	jsObject *object(const std::string &key);

	std::vector<jsValue *> &vect(const std::string &key);

	int integer(const std::string &key);

	std::string &str(const std::string &key);

	unsigned type() const override;

	static jsObject *parse(std::istream &i);

	jsObject *object() override;

	~jsObject() override;
};
}  // namespace Awali

#endif
