// This file is part of Awali.
// Copyright 2016-2021 Sylvain Lombardy, Victor Marsault, Jacques Sakarovitch
//
// Awali is a free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AWALI_JSON_UTILS_HH
#define AWALI_JSON_UTILS_HH

#include<iostream>
#include<istream>
#include<sstream>
#include"parse_exception.hpp"

#define __SUPPRESS_UNUSED_WARNING__(a) ((void)a);

namespace Awali {
namespace Internal {

/** prints spaces
 *
 *  prints \a n spaces in the stream \a o.
 *
 * @param o output stream
 * @param n number of spaces
 * return the output stream
 */
static inline std::ostream &tabs(std::ostream &o, int n) {
	__SUPPRESS_UNUSED_WARNING__(tabs)
	for (int i = 0; i < n; i++)
		o << "   ";
	return o;
}

/** formats a json file
 *
 * This function insert indentations and line breaks in order to pretty print
 * a json file.
 *
 * The rules are the following:
 * - the existing line breaks are preserved, without changing the indentation;
 * - there is always a line break before a left curly brace, the indentation is then incremented;
 * - if an object contains inner objects, the right curly brace is written on a new line with the same indentation as the left curly brace. In any case, the indentation is decremented;
 *
 * @param o the output stream
 * @param i the input stream sending a syntactically correct json file
 */
static inline std::ostream &jsFormat(std::ostream &o, std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(jsFormat)
	int depth = 0;
	char c;
	bool inOpen = false;
	bool inString = false;
	while (!i.eof()) {
		i.get(c);
		switch (c) {
		case '\t': break;
		case '{':
			if (!inString) {
				inOpen = true;
				if (depth > 0) {
					o << std::endl;
				}
				tabs(o, depth++);
			}
			o << c;
			break;
		case '}':
			if (!inString) {
				--depth;
				if (depth < 0) {
					return o;
				}
				if (!inOpen) {
					o << std::endl;
					tabs(o, depth);
				}
				inOpen = false;
			}
			o << c;
			break;
		case '\n' : o << std::endl << ' ';
			tabs(o, depth - 1);
			break;
		case '"' : inString = !inString;
		default : o << c;
		}
	}
	return o;
}

/** checks the next character
 *
 * @param i the input stream
 * @param e the expected character
 * return true if the next non blank character in \a i is \a e
 */
static inline void check(std::istream &i, char e) {
	__SUPPRESS_UNUSED_WARNING__(check)
	static char buffer[] = "parser expects . gets .";
	buffer[15] = e;
	char c;
	do {
		i >> c;
	}
	while (c == ' ' || c == '\n' || c == '\t');
	//std::cerr << c << '&' << std::endl;
	if (c != e) {
		buffer[22] = c;
		raiseParse(i, buffer);
	}
}

/** peeks the next character
 *
 * @param i the input stream
 * return the value of the next non blank character in \a i
 */
static inline char peek(std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(peek)
	char c;
	while (true) {
		c = i.peek();
		if (c != ' ' && c != '\n' && c != '\t') {
			return c;
		}
		i.get(c);
	}
}

static inline std::string parseString(std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(parseString)
	char c;
	i >> c;
	if (c != '"') {
		raiseParse(i, "parser \"");
	}
	std::ostringstream o;
	while (true) {
		i.get(c);
		if (c == '"') {
			break;
		}
		o << c;
		if (c == '\\') {
			i.get(c);
			o << c;
		}
	}
	//std::cout << '>' << o.str() << '<' << std::endl;
	return o.str();
}

static inline char parseCst(std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(parseCst)
	std::string s;
	int l;
	switch (i.peek()) {
	case 'n' :
	case 't': l = 4;
		break;
	case 'f' : l = 5;
		break;
	default: raiseParse(i, "parser cst");
		return '\0';
	}
	for (int n = 0; n < l; ++n) {
		char c;
		i.get(c);
		s.append(1, c);
	}
	if (s != "true" && s != "none" && s != "null" && s != "false") {
		raiseParse(i, "parser cst");
	}
	return s[0];
}

static inline int parseInt(std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(parseInt)
	int x;
	i >> x;
	return x;
}

static inline std::string getFirstAttr(std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(getFirstAttr)
	check(i, '{');
	std::string attr = parseString(i);
	check(i, ':');
	return attr;
}

static inline void parseIgnore(std::istream &i) {
	__SUPPRESS_UNUSED_WARNING__(parseIgnore)
	switch (peek(i)) {
	case '"': parseString(i);
		if (peek(i) != ':') {
			return;
		}
		check(i, ':');
		parseIgnore(i);
		return;
	case '{': check(i, '{');
		while (peek(i) != '}')
			parseIgnore(i);
		check(i, '}');
		return;
	case '[' : check(i, '[');
		while (true) {
			parseIgnore(i);
			if (peek(i) != ',') {
				break;
			}
			check(i, ',');
		}
		check(i, ']');
		return;
	case 't':
	case 'n' :
	case 'f': parseCst(i);
		return;
	default : raiseParse(i, "Parse ignore");
	}
}
}  // namespace Internal
}  // namespace Awali

#undef __SUPPRESS_UNUSED_WARNING__
#endif                 //AWALI_JSON_UTILS_HH
