// This file is part of Awali.
// Copyright 2016-2021 Sylvain Lombardy, Victor Marsault, Jacques Sakarovitch
//
// Awali is a free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#ifndef AWALI_PARSE_EXCEPTION_CC
#define AWALI_PARSE_EXCEPTION_CC

#include"parse_exception.hpp"
#include<sstream>

namespace Awali {

void raiseParse(std::istream &i, const char *s) {
	int lineCount = 1;
	int startLineCount = 0;
	i.clear();
	int currentPos = i.tellg();
	if (currentPos < 0) {
		throw parseException(s);
	}
	i.seekg(0);
	char c;
	while ((i.tellg() < currentPos) && i.get(c)) {
		if (c == '\n') {
			++lineCount;
			startLineCount = i.tellg();
		}
	}
	std::ostringstream os;
	os << "line " << lineCount << " col " << (currentPos - startLineCount) << " : " << s;
	throw parseException(os.str());
}

void raiseParse(size_t p, const char *s) {
	std::ostringstream os;
	os << "position " << p << " : " << s;
	throw parseException(os.str());
}
}  // namespace Awali

#endif
