set(JSON
    parse_exception.cpp
    parser.cpp
    json.cpp
    writer.cpp)

add_library(json STATIC ${JSON})

target_link_libraries(json Qt5::Gui)

target_include_directories(json PRIVATE ${SRC}/model ${SRC}/utils)
