#ifndef VISUAL_AUTOMATA_PARSER_HPP
#define VISUAL_AUTOMATA_PARSER_HPP

#include "automata.hpp"
#include <unordered_map>
#include <automata.hpp>
#include <state_group.hpp>
#include <memory>

#include "json.hpp"
#include "json_utils.hpp"
#include "parse_exception.hpp"

using namespace Awali;
using namespace std;
namespace VisualAutomata {
class Parser {
	static vector<State *> heapStates;
	static vector<Transition *> heapTransitions;
	static vector<StateGroup *> heapGroups;

	/**
	 * Builds the jsObject of the json file.
	 * @param s path of the json file.
	 */
	static jsObject *stringToObject(const string &s);

	/**
	 * Parses an automata.
	 * @param automata the automata to complete.
	 * @param object the jsObject where the data is
	 */
	static void parseAutomata(Automata &automata, jsObject *object);

	/**
	 * Parses the field "data".
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseData(Automata &automata, jsValue *value);

	/**
	 * Parses the states.
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseStates(Automata &automata, jsValue *value);

	/**
	 * Parses a state.
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseState(Automata &automata, jsValue *value);

	/**
	 * Parses the field "id".
	 * @param automata the automata to complete.
	 * @param object the object to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseId(Automata &automata, AutomataObject &object, jsValue *value);

	/**
	 * Parses the field "label".
	 * @param object the object to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseLabel(AutomataObject &object, jsValue *value);

	/**
	 * Parses the field "style".
	 * @param object the object to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseStyle(AutomataObject &object, jsValue *value);

	/**
	 * Parses the field "initial".
	 * @param state the state to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseInitial(State &state, jsValue *value);

	/**
	 * Parses the field "final".
	 * @param state the state to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseFinal(State &state, jsValue *value);

	/**
	 * Parses the transitions.
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseTransitions(Automata &automata, jsValue *value);

	/**
	 * Parses a transition.
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseTransition(Automata &automata, jsValue *value);

	/**
	 * Parses the field "weight".
	 * @param transition the transition to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseWeight(Transition &transition, jsValue *value);

	/**
	 * Parses the groups.
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseGroups(Automata &automata, jsValue *value);

	/**
	 * Parses a group.
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseGroup(Automata &automata, jsValue *value);

	/**
	 * Adds states to a group.
	 * @param automata the automata to complete.
	 * @param group the group to complete.
	 * @param value the jsValue where the data is.
	 */
	static void addStatesToGroup(Automata &automata, StateGroup &group, jsValue *value);

	/**
	 * Adds a state to a group.
	 * @param automata the automata to complete.
	 * @param group the group to complete.
	 * @param value the jsValue where the data is.
	 */
	static void addStateToGroup(Automata &automata, StateGroup &group, jsValue *value);

	/**
	 * Parses the field "metadata".
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseMetadata(Automata &automata, jsValue *value);

	/**
	 * Parses the field "format".
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseFormat(Automata &automata, jsValue *value);

	/**
	 * Parses the field "kind".
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseKind(Automata &automata, jsValue *value);

	/**
	 * Parses the field "context".
	 * @param automata the automata to complete.
	 * @param value the jsValue where the data is.
	 */
	static void parseContext(Automata &automata, jsValue *value);

public:

	/**
	 * Parses a file into an automata.
	 * @param s the path of the file.
	 */
	static std::unique_ptr<Automata> parse(const string &s);

	/**
	 * Deallocates the memory previously allocated with the Parser.
	 */
	static void freeParser();
};
}  // namespace VisualAutomata
#endif
