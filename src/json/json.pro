TEMPLATE = lib

INCLUDEPATH += . ../model/

HEADERS += \
    parseException.hpp \
    parser.hpp \
    writing.hpp \

SOURCES += \
    parseException.cpp \
    parser.cpp \
    json.cpp \
    writing.cpp \
