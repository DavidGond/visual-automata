#ifndef VISUAL_AUTOMATA_WRITER_HPP
#define VISUAL_AUTOMATA_WRITER_HPP

#include "automata.hpp"

#include "json.hpp"
#include "json_utils.hpp"
#include "parse_exception.hpp"

using namespace Awali;
using namespace std;
namespace VisualAutomata {
class Writer {
	static int tabulationCount;
	static bool comma;

	static void lineBreak(ostream &stream);

	/**
	 * Writes an automata in json format.
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeAutomata(const Automata &automata, ostream &stream);

	/**
	 * Writes the field "data".
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeData(const Automata &automata, ostream &stream);

	/**
	 * Writes the states.
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeStates(const Automata &automata, ostream &stream);

	/**
	 * Writes a state.
	 * @param state the state to save.
	 * @param stream the output stream to save data.
	 */
	static void writeState(const State &state, ostream &stream);

	/**
	 * Writes the field "id".
	 * @param object the object to save.
	 * @param stream the output stream to save data.
	 */
	static void writeId(const AutomataObject &object, ostream &stream);

	/**
	 * Writes a label.
	 * @param object the object to save.
	 * @param stream the output stream to save data.
	 * @param s the name of the label
	 */
	static void writeLabel(const AutomataObject &object, ostream &stream, const string &s);

	/**
	 * Writes the attributes of the field "style".
	 * @param attribute the attribute to save.
	 * @param stream the output stream to save data.
	 */
	static void writeStyleAttribute(const string &attribute, ostream &stream);

	/**
	 * Writes the field "style".
	 * @param object the object to save.
	 * @param stream the output stream to save data.
	 */
	static void writeStyle(const AutomataObject &object, ostream &stream);

	/**
	 * Writes the field "initial".
	 * @param stream the output stream to save data.
	 */
	static void writeInitial(ostream &stream);

	/**
	 * Writes the field "final".
	 * @param stream the output stream to save data.
	 */
	static void writeFinal(ostream &stream);

	/**
	 * Writes the transitions.
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeTransitions(const Automata &automata, ostream &stream);

	/**
	 * Writes a transition.
	 * @param transition the transition to save.
	 * @param stream the output stream to save data.
	 */
	static void writeTransition(const Transition &transition, ostream &stream);

	/**
	 * Writes the field "source".
	 * @param transition the transition to save.
	 * @param stream the output stream to save data.
	 */
	static void writeSource(const Transition &transition, ostream &stream);

	/**
	 * Writes the field "destination".
	 * @param transition the transition to save.
	 * @param stream the output stream to save data.
	 */
	static void writeDestination(const Transition &transition, ostream &stream);

	/**
	 * Writes the field "weight".
	 * @param transition the transition to save.
	 * @param stream the output stream to save data.
	 */
	static void writeWeight(const Transition &transition, ostream &stream);

	/**
	 * Writes the groups.
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeGroups(const Automata &automata, ostream &stream);

	/**
	 * Writes a group.
	 * @param group the group to save.
	 * @param stream the output stream to save data.
	 */
	static void writeGroup(const StateGroup &group, ostream &stream);

	/**
	 * Writes the states of a group.
	 * @param group the group to save.
	 * @param stream the output stream to save data.
	 */
	static void writeStatesToGroup(const StateGroup &group, ostream &stream);

	/**
	 * Writes a state of a group.
	 * @param state the state to save.
	 * @param stream the output stream to save data.
	 */
	static void writeStateToGroup(const State &state, ostream &stream);

	/**
	 * Writes the field "metadata".
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeMetadata(const Automata &automata, ostream &stream);

	/**
	 * Writes the field "format".
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeFormat(const Automata &automata, ostream &stream);

	/**
	 * Writes the field "kind".
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeKind(const Automata &automata, ostream &stream);

	/**
	 * Writes the field "context".
	 * @param automata the automata to save.
	 * @param stream the output stream to save data.
	 */
	static void writeContext(const Automata &automata, ostream &stream);

public:
	/**
	 * Saves an automata.
	 * @param automata the automata to save.
	 * @param savePath the path to save the automata.
	 */
	static void writeSave(const Automata &automata, const string &savePath);

	/**
	 * Returns a json string of an automata.
	 * @param automata the automata to save.
	 */
	static string writePrint(const Automata &automata);
};
}  // namespace VisualAutomata
#endif
