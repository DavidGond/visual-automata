#ifndef VISUAL_AUTOMATA_MAIN_WINDOW_HPP
#define VISUAL_AUTOMATA_MAIN_WINDOW_HPP

#include <QMainWindow>
#include <QTabWidget>
#include <QPointer>
#include <QMessageBox>
#include "automata_viewport.hpp"
#include "automata_window.hpp"

namespace VisualAutomata {

class MainWindow: public QMainWindow {
	QPointer<QTabWidget> tabWidget;
	QObject *controller;

public:
	explicit MainWindow(QObject *controller);

	~MainWindow() override;

	void addAutomataWindow(const QString &name, std::unique_ptr<Automata>);

	void deleteCurrentWindow();

	AutomataWindow *getCurrentAutomataWindow() const;
	QTabWidget *getTabWidget() const;

public slots:
	void quitApplication();

protected:
	void closeEvent(QCloseEvent *event) override;
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_MAIN_WINDOW_HPP
