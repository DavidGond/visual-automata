#include <iostream>
#include "main_window.hpp"
#include <cassert>

#define qApp_automata (dynamic_cast<QApplication *>(QCoreApplication::instance()))

namespace VisualAutomata {

MainWindow::MainWindow(QObject *controller) {
	this->controller = controller;

	tabWidget = new QTabWidget(this);
	tabWidget->setTabsClosable(true);
	setCentralWidget(tabWidget);
	std::unique_ptr<Automata> automata = std::make_unique<Automata>();
	addAutomataWindow("Sans Nom", std::move(automata));

	AutomataDrawable *automataDrawable = getCurrentAutomataWindow()->getAutomataViewportProjection()
	                                                               ->getScene()
	                                                               ->getAutomataDrawable();
	automataDrawable->setAutomata(getCurrentAutomataWindow()->getAutomata());
	automataDrawable->setScene(*getCurrentAutomataWindow()->getAutomataViewportProjection()
	                                                      ->getScene());

	QObject::connect(tabWidget, SIGNAL(currentChanged(int)), controller, SLOT(updateGroupList()));

	// Full screen
	QWidget::showMaximized();
}

MainWindow::~MainWindow() {
	for (int i = 0; i < tabWidget->count(); i++) {
		tabWidget->setCurrentIndex(i);
		auto *automataWindow = (AutomataWindow *) tabWidget->currentWidget();
		delete automataWindow;
	}
	if (tabWidget != nullptr) {
		delete (tabWidget);
		tabWidget = nullptr;
	}
}

void MainWindow::addAutomataWindow(const QString &name, std::unique_ptr<Automata> automaton) {

	auto *automataWindow = new AutomataWindow(std::move(automaton));
	tabWidget->addTab(automataWindow, name);
	tabWidget->setCurrentIndex(tabWidget->count() - 1);
	QObject::connect(automataWindow->getAutomataViewportProjection()
	                               ->getScene()
	                               ->getAutomataDrawable(),
	                 SIGNAL(selectedItemChanged(Selectable & )),
	                 controller,
	                 SLOT(updatesDockWidget(Selectable & )));
	QObject::connect(automataWindow->getAutomataViewportGeometry()
	                               ->getScene()
	                               ->getAutomataDrawable(),
	                 SIGNAL(selectedItemChanged(Selectable & )),
	                 controller,
	                 SLOT(updatesDockWidget(Selectable & )));
	QObject::connect(automataWindow->getAutomataViewportProjection()
	                               ->getScene()
	                               ->getAutomataDrawable(),
	                 SIGNAL(selectedItemEmpty()),
	                 controller,
	                 SLOT(switchToEmptyDockWidget()));
	QObject::connect(automataWindow->getAutomataViewportGeometry()
	                               ->getScene()
	                               ->getAutomataDrawable(),
	                 SIGNAL(selectedItemEmpty()),
	                 controller,
	                 SLOT(switchToEmptyDockWidget()));
	// No items are selected -> set group index to zero (and switch to empty dock widget except if the selectedGroup is editable)
	QObject::connect(automataWindow->getAutomataViewportProjection()
	                               ->getScene()
	                               ->getAutomataDrawable(),
	                 SIGNAL(selectedItemEmpty()),
	                 controller,
	                 SLOT(refreshGroupIndex()));
	QObject::connect(automataWindow->getAutomataViewportGeometry()
	                               ->getScene()
	                               ->getAutomataDrawable(),
	                 SIGNAL(selectedItemEmpty()),
	                 controller,
	                 SLOT(refreshGroupIndex()));
}

void MainWindow::deleteCurrentWindow() {
	tabWidget->removeTab(tabWidget->currentIndex());
	if (tabWidget->count() > 0) {
		tabWidget->setCurrentIndex(0);
	}
}

AutomataWindow *MainWindow::getCurrentAutomataWindow() const {
	return (AutomataWindow *) tabWidget->currentWidget();
}

QTabWidget *MainWindow::getTabWidget() const {
	return tabWidget;
}

void MainWindow::quitApplication() {
	QMessageBox msgBox(this);
	msgBox.setText("Voulez-vous vraiment fermer l'application ?");
	msgBox.setInformativeText("Les données non sauvegardées seront perdues.");
	msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
	msgBox.setDefaultButton(QMessageBox::Cancel);
	int ret = msgBox.exec();
	if (ret == QMessageBox::Cancel) {
		return;
	}
	assert(ret == QMessageBox::Ok);
	qApp_automata->quit();
}

void MainWindow::closeEvent(QCloseEvent *event) {
	event->ignore();
	quitApplication();
}

}  // namespace VisualAutomata
