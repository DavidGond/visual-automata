TEMPLATE = subdirs

SUBDIRS += prog \
    graphical_window \
    viewport \
    model \
    pictures \
    utils \
    json \

prog.file = prog.pro
prog.depends += \
    graphical_window \
    viewport \
    json

viewport.depends += \
    model \
    json \

