#ifndef VISUAL_AUTOMATA_UTILS_HPP
#define VISUAL_AUTOMATA_UTILS_HPP

#include <map>
#include <iostream>
#include "automata_object.hpp"

#include <memory>
#include <stdexcept>
#include <vector>
#include <sstream>

namespace VisualAutomata {

template<typename T, typename std::enable_if<std::is_base_of<AutomataObject,
                                                             T>::value>::type * = nullptr>
bool addObjectToMap(T &object, std::map<int, T *> &objects) {
	auto ret = objects.insert(std::pair<int, T *>(object.getId(), &object));
	if (ret.second == false) {
		std::cout << "addObjectToMap : object ID exist already" << std::endl;
		return false;
	}
	return true;
}

template<typename T, typename std::enable_if<std::is_base_of<AutomataObject,
                                                             T>::value>::type * = nullptr>
bool delObjectFromMap(T &object, std::map<int, T *> &objects) {
	auto search = objects.find(object.getId());
	if (search == objects.end()) {
		std::cout << "delObjectFromMap : Object ID doesn't exist" << endl;
		return false;
	}
	objects.erase(search);
	return true;
}

template<typename T, typename std::enable_if<std::is_base_of<AutomataObject,
                                                             T>::value>::type * = nullptr>
T &getObjectFromMap(const int id, std::map<int, T *> &objects) {
	auto search = objects.find(id);
	if (search == objects.end()) {
		std::cout << "getObjectFromMap : Object ID doesn't exist" << endl;
		return *(search->second);
	}
	return *(search->second);
}

vector<string> split(const string &styleStr, char c);

std::string removeSpace(const std::string &str);
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_UTILS_HPP
