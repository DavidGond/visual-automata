#include "utils.hpp"

namespace VisualAutomata {

std::string removeSpace(const std::string &str) {
	std::string resStr;
	for (char c: str) {
		if (c != '\t' && c != ' ') {
			resStr += c;
		}
	}
	return resStr;
}

vector<string> split(const string &styleStr, char c) {
	vector<string> v;
	stringstream ss(styleStr);
	string s;
	while (getline(ss, s, c)) {
		v.push_back(s);
	}
	return v;
}

}  // namespace VisualAutomata
