#ifndef VISUAL_AUTOMATA_DEBUG_HPP
#define VISUAL_AUTOMATA_DEBUG_HPP

#include <cstdio>
#include <cerrno>

/* UBUNTU/LINUX and MacOS ONLY terminal color codes */
#define RESET   "\033[0m"
#define BLACK   "\033[30m"      /* Black */
#define RED     "\033[31m"      /* Red */
#define GREEN   "\033[32m"      /* Green */
#define YELLOW  "\033[33m"      /* Yellow */
#define BLUE    "\033[34m"      /* Blue */
#define MAGENTA "\033[35m"      /* Magenta */
#define CYAN    "\033[36m"      /* Cyan */
#define WHITE   "\033[37m"      /* White */
#define BOLDBLACK   "\033[1m\033[30m"      /* Bold Black */
#define BOLDRED     "\033[1m\033[31m"      /* Bold Red */
#define BOLDGREEN   "\033[1m\033[32m"      /* Bold Green */
#define BOLDYELLOW  "\033[1m\033[33m"      /* Bold Yellow */
#define BOLDBLUE    "\033[1m\033[34m"      /* Bold Blue */
#define BOLDMAGENTA "\033[1m\033[35m"      /* Bold Magenta */
#define BOLDCYAN    "\033[1m\033[36m"      /* Bold Cyan */
#define BOLDWHITE   "\033[1m\033[37m"      /* Bold White */

enum LogLevel {
	ERROR, WARN, INFO, DEBUG
};

/**
 * LOG_MESSAGE prints a preformatted message to standard output
 */
#define LOG_MESSAGE(level, message, ...) do { \
    switch (level) { \
    case ERROR: \
        fprintf(stderr, RED"[ERROR] %s:%d " message"\n" RESET, __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    case WARN: \
        fprintf(stderr, YELLOW"[WARN] " message"\n" RESET, ##__VA_ARGS__); \
        break; \
    case INFO: \
        fprintf(stdout, WHITE"[INFO] " message"\n" RESET, ##__VA_ARGS__); \
        break; \
    case DEBUG: \
        fprintf(stdout, BLUE"[DEBUG] %s:%d " message"\n" RESET, __FILE__, __LINE__, ##__VA_ARGS__); \
        break; \
    default: \
        fprintf(stdout, message"\n", ##__VA_ARGS__); \
        break; \
    }                                              \
} while(0)

#define CHECK(condition, action, ...) do { \
    if (!(condition)) { \
        if (errno != 0) { \
            LOG_MESSAGE(ERROR, ##__VA_ARGS__); \
            perror(":"); \
        } else { \
        LOG_MESSAGE(ERROR, ##__VA_ARGS__); \
        } \
        action; \
    } \
} while(0)

#endif //VISUAL_AUTOMATA_DEBUG_HPP
