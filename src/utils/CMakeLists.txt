set(UTILS utils.cpp)

add_library(utils STATIC ${UTILS})

target_include_directories(utils PRIVATE ${SRC}/model)

target_link_libraries(utils Qt5::Gui)
