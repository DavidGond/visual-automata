#include <QComboBox>
#include "automata_controller.hpp"
#include "writer.hpp"

using namespace VisualAutomata;

AutomataController::AutomataController() {
	preferences = new PreferenceWindow();
	mainWindow = new MainWindow(this);
	stateDockWidget = new StateDockWidget(mainWindow);
	transitionDockWidget = new TransitionDockWidget(mainWindow);
	groupeDockWidget = new GroupDockWidget(mainWindow);
	groupeDockWidget->setHidden(true);
	rightDockWidget = new RightDockWidget(mainWindow);
	leftDockWidget = new LeftDockWidget(mainWindow);
	toolBar = new ToolBar(mainWindow);
	menuBar = new MenuBar(mainWindow);

	stateDockWidget->setHidden(true);
	transitionDockWidget->setHidden(true);

	//PreferenceWindow
	QObject::connect(preferences,
	                 &PreferenceWindow::preferenceDataChanged,
	                 this,
	                 &AutomataController::loadPreferenceData);
	// Loading data after doing the connexion, else it doesn't work
	preferences->loadPreferenceData();

	QObject::connect(preferences->getTextFontWidget()->getFontButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(openFontDialog()));
	QObject::connect(preferences->getTextFontWidget()
	                            ->getFontColorPicker()
	                            ->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences->getTextFontWidget()->getFontColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(preferences->getTextFontWidget()->getFontColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences->getTextFontWidget()->getFontColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(preferences->getStateColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences->getStateColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(preferences->getStateColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences->getStateColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(preferences->getStateBackgroundColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences->getStateBackgroundColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(preferences->getStateBackgroundColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences->getStateBackgroundColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(preferences->getTransitionColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences->getTransitionColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(preferences->getTransitionColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences->getTransitionColorPicker(),
	                 SLOT(editColor()));

	// Connexion for saving data after a change is made
	// Do not forget to add connexion to save data if you add a connexion from the preference window
	// These connexion must be done after the connexion that modify the preference window
	QObject::connect(preferences->getTextFontWidget()->getFontButton(),
	                 SIGNAL(clicked()),
	                 preferences,
	                 SLOT(savePreferenceData()));

	QObject::connect(preferences->getTextFontWidget()
	                            ->getFontColorPicker()
	                            ->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getStateColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getStateBackgroundColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getTransitionColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 preferences,
	                 SLOT(savePreferenceData()));

	QObject::connect(preferences->getTextFontWidget()->getFontColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getStateColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getStateBackgroundColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getTransitionColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 preferences,
	                 SLOT(savePreferenceData()));

	QObject::connect(preferences->getTransitionSlider()->getSlider(),
	                 SIGNAL(valueChanged(int)),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getTransitionLineWidget()->getLinePicker(),
	                 SIGNAL(currentIndexChanged(int)),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getStateLineWidget()->getLinePicker(),
	                 SIGNAL(currentIndexChanged(int)),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getTransitionLineWidget()->getLineSizePicker(),
	                 SIGNAL(valueChanged(int)),
	                 preferences,
	                 SLOT(savePreferenceData()));
	QObject::connect(preferences->getStateLineWidget()->getLineSizePicker(),
	                 SIGNAL(valueChanged(int)),
	                 preferences,
	                 SLOT(savePreferenceData()));

	// LeftDockWidget
	QObject::connect(leftDockWidget->getStateButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(selectPositionDefaultState()));
	QObject::connect(leftDockWidget->getInitialStateButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(selectPositionInitialState()));
	QObject::connect(leftDockWidget->getFinalStateButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(selectPositionFinalState()));
	QObject::connect(leftDockWidget->getLeftTransitionButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(addTransitionMode()));

	// MenuBar
	QObject::connect(menuBar->getActionPreferences(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(displayPreferencesWindow()));
	QObject::connect(menuBar->getActionZoomPlus(), SIGNAL(triggered()), this, SLOT(zoomIn()));
	QObject::connect(menuBar->getActionZoomMoins(), SIGNAL(triggered()), this, SLOT(zoomOut()));

	// StateDockWidget
	QObject::connect(stateDockWidget->getFontWidget()->getFontButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(openFontDialog()));
	QObject::connect(stateDockWidget->getFontWidget()
	                                ->getFontColorPicker()
	                                ->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 stateDockWidget->getFontWidget()->getFontColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(stateDockWidget->getFontWidget()->getFontColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 stateDockWidget->getFontWidget()->getFontColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(stateDockWidget->getBackgroundColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 stateDockWidget->getBackgroundColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(stateDockWidget->getBackgroundColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 stateDockWidget->getBackgroundColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(stateDockWidget->getLineColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 stateDockWidget->getLineColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(stateDockWidget->getLineColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 stateDockWidget->getLineColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(stateDockWidget->getDeleteButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(deleteSelectedItem()));
	// Sets the style of the selected state
	QObject::connect(stateDockWidget->getLineColorPicker(),
	                 SIGNAL(colorChanged(QColor)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(stateDockWidget->getBackgroundColorPicker(),
	                 SIGNAL(colorChanged(QColor)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(stateDockWidget->getLineWidget()->getLinePicker(),
	                 SIGNAL(currentIndexChanged(int)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(stateDockWidget->getLineWidget()->getLineSizePicker(),
	                 SIGNAL(valueChanged(int)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(stateDockWidget->getFinalState(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));
	QObject::connect(stateDockWidget->getInitialState(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));
	QObject::connect(stateDockWidget->getHidingCheckBox(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));
	QObject::connect(stateDockWidget->getEditText(),
	                 SIGNAL(returnPressed()),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));

	// TransitionDockWidget
	QObject::connect(transitionDockWidget->getLineColorPicker()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 transitionDockWidget->getLineColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(transitionDockWidget->getLineColorPicker()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 transitionDockWidget->getLineColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(transitionDockWidget->getTextFontWidget()->getFontButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(openFontDialog()));
	QObject::connect(transitionDockWidget->getTextFontWidget()
	                                     ->getFontColorPicker()
	                                     ->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 transitionDockWidget->getTextFontWidget()->getFontColorPicker(),
	                 SLOT(openColorDialog()));
	QObject::connect(transitionDockWidget->getTextFontWidget()
	                                     ->getFontColorPicker()
	                                     ->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 transitionDockWidget->getTextFontWidget()->getFontColorPicker(),
	                 SLOT(editColor()));
	QObject::connect(transitionDockWidget->getDeleteButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(deleteSelectedItem()));
	// Sets the style of the selected transition
	QObject::connect(transitionDockWidget->getLineColorPicker(),
	                 SIGNAL(colorChanged(QColor)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(transitionDockWidget->getLineWidget()->getLinePicker(),
	                 SIGNAL(currentIndexChanged(int)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(transitionDockWidget->getLineWidget()->getLineSizePicker(),
	                 SIGNAL(valueChanged(int)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(transitionDockWidget->getTransitionAccuracySlider()->getSlider(),
	                 SIGNAL(valueChanged(int)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(transitionDockWidget->getHidingCheckBox(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setStyleSelectedItem()));
	QObject::connect(transitionDockWidget->getTransitionText(),
	                 SIGNAL(returnPressed()),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));
	QObject::connect(transitionDockWidget->getWeightText(),
	                 SIGNAL(returnPressed()),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));
	QObject::connect(transitionDockWidget->getHidingCheckBox(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setPropertiesSelectedItem()));

	// Sets the new default Name for transition
	QObject::connect(transitionDockWidget->getTransitionText(),
	                 SIGNAL(returnPressed()),
	                 this,
	                 SLOT(setTransitionDefaultName()));

	// Group of states
	QObject::connect(groupeDockWidget->getPrioritySpinBox(),
	                 SIGNAL(valueChanged(int)),
	                 this,
	                 SLOT(changeSelectedGroupPriority(int)));
	QObject::connect(groupeDockWidget->getNameWidget(),
	                 SIGNAL(textChanged(const QString&)),
	                 this,
	                 SLOT(setSelectedGroupName(QString)));
	QObject::connect(groupeDockWidget->getEditableCheckBox(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setEditableSelectableGroup(int)));
	QObject::connect(groupeDockWidget->getApplyStyleCheckBox(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setDrawingStateSelectableGroup(int)));
	QObject::connect(groupeDockWidget->getStyleSizePen()->getLineSizePicker(),
	                 SIGNAL(valueChanged(int)),
	                 this,
	                 SLOT(setSelectableGroupPenSize(int)));
	QObject::connect(groupeDockWidget->getStyleSizePen()->getLinePicker(),
	                 SIGNAL(currentIndexChanged(int)),
	                 this,
	                 SLOT(setSelectableGroupPenStyle()));

	// PenColor connexions
	QObject::connect(groupeDockWidget->getColorPickerPen()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 groupeDockWidget->getColorPickerPen(),
	                 SLOT(openColorDialog()));
	QObject::connect(groupeDockWidget->getColorPickerPen()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(setSelectableGroupPenColor()));
	QObject::connect(groupeDockWidget->getColorPickerPen()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 groupeDockWidget->getColorPickerPen(),
	                 SLOT(editColor()));
	QObject::connect(groupeDockWidget->getColorPickerPen()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 this,
	                 SLOT(setSelectableGroupPenColor()));

	// BrushColor connexions
	QObject::connect(groupeDockWidget->getColorPickerBrush()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 groupeDockWidget->getColorPickerBrush(),
	                 SLOT(openColorDialog()));
	QObject::connect(groupeDockWidget->getColorPickerBrush()->getColorBackgroundButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(setSelectableGroupBrushColor()));
	QObject::connect(groupeDockWidget->getColorPickerBrush()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 groupeDockWidget->getColorPickerBrush(),
	                 SLOT(editColor()));
	QObject::connect(groupeDockWidget->getColorPickerBrush()->getEditBackground(),
	                 SIGNAL(returnPressed()),
	                 this,
	                 SLOT(setSelectableGroupBrushColor()));

	// Create a groupe style
	QObject::connect(toolBar->getActionAddGroupe(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(createStyleGroup()));

	// Delete a groupe style
	QObject::connect(groupeDockWidget->getDeleteGroupButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(deleteStyleGroup()));


	// GroupComboBoxIndex changed -> switch between groupeDock and emptyDock
	QObject::connect(toolBar->getAffichageGroupeBox(),
	                 SIGNAL(currentIndexChanged(int)),
	                 this,
	                 SLOT(switchBetweenEmptyAndGroupDock(int)));

	// Lock or unlock the addGroupButton according to the position of the GroupComboBox
	QObject::connect(toolBar->getAffichageGroupeBox(),
	                 SIGNAL(currentIndexChanged(int)),
	                 this,
	                 SLOT(setLockAddGroupButton(int)));

	// Lock or unlock the GroupComboBox according to the 'editable' state of the selected Group
	QObject::connect(groupeDockWidget->getEditableCheckBox(),
	                 SIGNAL(stateChanged(int)),
	                 this,
	                 SLOT(setLockChangeGroupButton(int)));

	// MenuBar
	QObject::connect(menuBar->getActionNouveau(), SIGNAL(triggered()), this, SLOT(newWindow()));
	QObject::connect(menuBar->getActionFermer(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(quitApplication()));
	QObject::connect(menuBar->getActionFermerOnglet(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(deleteCurrentWindow()));
	QObject::connect(menuBar->getActionOuvrir(), SIGNAL(triggered()), this, SLOT(openFile()));
	QObject::connect(menuBar->getActionEnregistrer(), SIGNAL(triggered()), this, SLOT(saveFile()));
	QObject::connect(menuBar->getActionEnregistrerSous(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(saveFileAt()));
	QObject::connect(menuBar->getActionLayoutLineaire(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(openLinearLayout()));
	QObject::connect(menuBar->getActionLayoutCirculaire(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(openCircularLayout()));

	// Zooming the view
	QObject::connect(toolBar->getActionZoomPlus(), SIGNAL(triggered()), this, SLOT(zoomIn()));
	QObject::connect(toolBar->getActionZoomMoins(), SIGNAL(triggered()), this, SLOT(zoomOut()));
	QObject::connect(toolBar->getActionTailleReelle(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(zoomActualSize()));

	// Updating the grid
	QObject::connect(toolBar->getAfficherGrille(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(updateMenuBarGrid()));
	QObject::connect(menuBar->getActionGrille(),
	                 SIGNAL(triggered()),
	                 this,
	                 SLOT(updateToolBarGrid()));
	QObject::connect(preferences->getSquareRadioButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(updateGrid()));
	QObject::connect(preferences->getTriangleRadioButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(updateGrid()));
	QObject::connect(preferences->getCrossRadioButton(),
	                 SIGNAL(clicked()),
	                 this,
	                 SLOT(updateGrid()));


	QObject::connect(mainWindow->getTabWidget(),
	                 SIGNAL(tabCloseRequested(int)),
	                 this,
	                 SLOT(closeTab(int)));
	QObject::connect(mainWindow->getTabWidget(),
	                 SIGNAL(currentChanged(int)),
	                 this,
	                 SLOT(tabSelected()));

	mainWindow->show();
}

AutomataController::~AutomataController() {
	delete toolBar;
	delete menuBar;
	delete stateDockWidget;
	delete leftDockWidget;
	delete transitionDockWidget;
	delete preferences;
}

void AutomataController::switchToTransitionDockWidget() {
	if (transitionDockWidget->isHidden()) {
		hideAllRightDockWidget();
		transitionDockWidget->setHidden(false);
	}
}

void AutomataController::switchToStateDockWidget() {
	if (stateDockWidget->isHidden()) {
		if (!isSelectedGroupEditable()) {
			hideAllRightDockWidget();
			stateDockWidget->setHidden(false);
		}
	}
}

void AutomataController::switchToEmptyDockWidget() {
	if (rightDockWidget->isHidden()) {
		if (!isSelectedGroupEditable()) {
			hideAllRightDockWidget();
			rightDockWidget->setHidden(false);
		}
	}
}

void AutomataController::switchToGroupDockWidget() {
	if (groupeDockWidget->isHidden()) {
		hideAllRightDockWidget();
		groupeDockWidget->setHidden(false);
	}
}

void AutomataController::hideAllRightDockWidget() {
	stateDockWidget->setHidden(true);
	transitionDockWidget->setHidden(true);
	rightDockWidget->setHidden(true);
	groupeDockWidget->setHidden(true);
}

void AutomataController::displayPreferencesWindow() {
	preferences->show();
}

void AutomataController::openFontDialog() {
	if (mainWindow->getTabWidget()->count() != 0) {
		bool button = false;
		QFont police = QFontDialog::getFont(&button, mainWindow->getCurrentAutomataWindow());
	}
}

// Changes the pen of the transition and state
void AutomataController::loadPreferenceData() {
	StateDrawable::setPreferencePenColor(preferences->getStateColorPicker()->getCurrentColor());
	StateDrawable::setPreferenceBrushColor(preferences->getStateBackgroundColorPicker()
	                                                  ->getCurrentColor());
	StateDrawable::setPreferencePenStyle(preferences->getStateLineWidget()
	                                                ->getLinePicker()
	                                                ->getFocusedStyle());
	StateDrawable::setPreferencePenSize(preferences->getStateLineWidget()
	                                               ->getLineSizePicker()
	                                               ->value());
	TransitionDrawable::setPreferencePenColor(preferences->getTransitionColorPicker()
	                                                     ->getCurrentColor());
	TransitionDrawable::setPreferencePenStyle(preferences->getTransitionLineWidget()
	                                                     ->getLinePicker()
	                                                     ->getFocusedStyle());
	TransitionDrawable::setPreferencePenSize(preferences->getTransitionLineWidget()
	                                                    ->getLineSizePicker()
	                                                    ->value());
	TransitionDrawable::preferenceAccuracy = preferences->getTransitionSlider()->getValue();

	// At the end of the function
	updateDrawing();
}

void AutomataController::updateDrawing() const {
	if (mainWindow->getTabWidget()->count() != 0) {
		int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
		if (viewportNumber == 0) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportProjection()->redraw();
		} else if (viewportNumber == 1) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportGeometry()->redraw();
		}
	}
}

void AutomataController::zoomIn() const {
	if (mainWindow->getTabWidget()->count() != 0) {
		int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
		if (viewportNumber == 0) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportProjection()->zoomIn();
		} else if (viewportNumber == 1) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportGeometry()->zoomIn();
		}
	}
}

void AutomataController::zoomOut() const {
	if (mainWindow->getTabWidget()->count() != 0) {
		int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
		if (viewportNumber == 0) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportProjection()->zoomOut();
		} else if (viewportNumber == 1) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportGeometry()->zoomOut();
		}
	}
}

void AutomataController::zoomActualSize() const {
	if (mainWindow->getTabWidget()->count() != 0) {
		int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
		if (viewportNumber == 0) {
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportProjection()
			          ->zoomActualSize();
		} else if (viewportNumber == 1) {
			mainWindow->getCurrentAutomataWindow()->getAutomataViewportGeometry()->zoomActualSize();
		}
	}
}

void AutomataController::newWindow() const {
	std::unique_ptr<Automata> automata = std::make_unique<Automata>();
	mainWindow->addAutomataWindow("Sans Nom", std::move(automata));
	AutomataDrawable *automataDrawable = mainWindow->getCurrentAutomataWindow()
	                                               ->getAutomataViewportProjection()
	                                               ->getScene()
	                                               ->getAutomataDrawable();
	automataDrawable->setAutomata(mainWindow->getCurrentAutomataWindow()->getAutomata());
	automataDrawable->setScene(*mainWindow->getCurrentAutomataWindow()
	                                      ->getAutomataViewportProjection()
	                                      ->getScene());
}

void AutomataController::deleteCurrentWindow() const {
	mainWindow->deleteCurrentWindow();
}

void AutomataController::openFile() {
	QString fileName = QFileDialog::getOpenFileName(mainWindow->getCurrentAutomataWindow(),
	                                                "Ouvrir fichier JSON",
	                                                "",
	                                                "JSON (*.json)");
	if (fileName != "") {
		try {
			std::unique_ptr<Automata> automata = Parser::parse(fileName.toStdString());
			automata->setPath(fileName.toStdString());
			mainWindow->addAutomataWindow(fileName, std::move(automata));
			AutomataDrawable *automataDrawable = mainWindow->getCurrentAutomataWindow()
			                                               ->getAutomataViewportProjection()
			                                               ->getScene()
			                                               ->getAutomataDrawable();
			automataDrawable->setAutomata(mainWindow->getCurrentAutomataWindow()->getAutomata());
			updateGroupList();
		}
		catch (Awali::parseException &e) {
			QMessageBox::critical(nullptr, "Erreur", e.what());
		}
	}
}

void AutomataController::openLinearLayout() {
	saveFile();
	if (!mainWindow->getCurrentAutomataWindow()->getAutomata()->getPath().empty()) {
		QString fileName = mainWindow->getCurrentAutomataWindow()->getAutomata()->getPath().c_str();
		try {
			std::unique_ptr<Automata> automata = Parser::parse(fileName.toStdString());
			automata->setPath(fileName.toStdString());
			mainWindow->addAutomataWindow(fileName, std::move(automata));
			AutomataDrawable *automataDrawable = mainWindow->getCurrentAutomataWindow()
			                                               ->getAutomataViewportProjection()
			                                               ->getScene()
			                                               ->getAutomataDrawable();
			Style::useCoordinates = false;
			automataDrawable->setAutomataLinear(mainWindow->getCurrentAutomataWindow()
			                                              ->getAutomata());
			Style::useCoordinates = true;
			updateGroupList();
		}
		catch (Awali::parseException &e) {
			QMessageBox::critical(nullptr, "Erreur", e.what());
		}
	}
}

void AutomataController::openCircularLayout() {
	saveFile();
	if (!mainWindow->getCurrentAutomataWindow()->getAutomata()->getPath().empty()) {
		QString fileName = mainWindow->getCurrentAutomataWindow()->getAutomata()->getPath().c_str();
		try {
			std::unique_ptr<Automata> automata = Parser::parse(fileName.toStdString());
			automata->setPath(fileName.toStdString());
			mainWindow->addAutomataWindow(fileName, std::move(automata));
			AutomataDrawable *automataDrawable = mainWindow->getCurrentAutomataWindow()
			                                               ->getAutomataViewportProjection()
			                                               ->getScene()
			                                               ->getAutomataDrawable();
			Style::useCoordinates = false;
			automataDrawable->setAutomataCircular(mainWindow->getCurrentAutomataWindow()
			                                                ->getAutomata());
			Style::useCoordinates = true;
			updateGroupList();
		}
		catch (Awali::parseException &e) {
			QMessageBox::critical(nullptr, "Erreur", e.what());
		}
	}
}

void AutomataController::saveFileAt() const {
	if (mainWindow->getTabWidget()->count() != 0) {
		QString fileName = QFileDialog::getSaveFileName(mainWindow->getCurrentAutomataWindow(),
		                                                "Enregistrer fichier JSON",
		                                                "",
		                                                "JSON (*.json)");
		if (fileName != "") {
			if (!fileName.endsWith(".json")) {
				fileName += ".json";
			}
			Automata &automata = *mainWindow->getCurrentAutomataWindow()
			                                ->getAutomataViewportProjection()
			                                ->getScene()
			                                ->getAutomataDrawable()
			                                ->getAutomata();
			Writer::writeSave(automata, fileName.toStdString());
			automata.setPath(fileName.toStdString());
			mainWindow->getTabWidget()
			          ->setTabText(mainWindow->getTabWidget()->currentIndex(), fileName);
		}
	}
}

void AutomataController::saveFile() const {
	if (mainWindow->getTabWidget()->count() != 0) {
		Automata &automata = *mainWindow->getCurrentAutomataWindow()
		                                ->getAutomataViewportProjection()
		                                ->getScene()
		                                ->getAutomataDrawable()
		                                ->getAutomata();
		string path = automata.getPath();
		if (!path.empty()) {
			Writer::writeSave(automata, path);
		} else {
			saveFileAt();
		}
	}
}

void AutomataController::selectPositionState(ViewportAction action) {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);
		disableTransitionMode();
		viewport->getScene()->setCurrentAction(action);
	}
}

void AutomataController::selectPositionDefaultState() {
	selectPositionState(action_add_state);
}

void AutomataController::selectPositionInitialState() {
	selectPositionState(action_add_initial_state);
}

void AutomataController::selectPositionFinalState() {
	selectPositionState(action_add_final_state);
}

void AutomataController::addTransitionMode() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (viewport->getScene()->getCurrentAction() == action_add_transition) {
			disableTransitionMode();
		} else {
			enableTransitionMode();
		}
	}
}

void AutomataController::setTransitionDefaultName() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		Selectable *selected = AutomataDrawable::getSelectedItem();
		if (selected->getType() == TRANSITION) {
			std::string
					name = transitionDockWidget->getTransitionText()->text().toUtf8().constData();
			viewport->getScene()->getAutomataDrawable()->setPreviewName(name);

		}
	}
}

void AutomataController::enableTransitionMode() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		// Updating the button
		QIcon leftTransitionIcon = QIcon(":/pictures/left_arrow_selected.png");
		QToolButton *leftTransitionButton = leftDockWidget->getLeftTransitionButton();
		leftTransitionButton->setIcon(leftTransitionIcon);
		leftTransitionButton->setIconSize(QSize(80, 80));

		// unselect all elements
		viewport->getScene()->getAutomataDrawable()->emptySelectedItems();

		// Setting the action of the scene
		viewport->getScene()->setCurrentAction(action_add_transition);
	}
}

void AutomataController::disableTransitionMode() {

	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		// Updating the button
		QIcon leftTransitionIcon = QIcon(":/pictures/left_arrow.png");
		QToolButton *leftTransitionButton = leftDockWidget->getLeftTransitionButton();
		leftTransitionButton->setIcon(leftTransitionIcon);
		leftTransitionButton->setIconSize(QSize(80, 80));

		// Setting the action of the scene
		viewport->getScene()->setCurrentAction(no_action);
	}
}

void AutomataController::deleteSelectedItem() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		Selectable *selected = AutomataDrawable::getSelectedItem();

		if (selected->getType() == STATE) {
			auto *state = (StateDrawable *) selected;
			if (state->getNbrLinked() > 0) {
				QMessageBox msgBox(mainWindow);
				msgBox.setText("Cet état est lié à des transitions.");
				msgBox.setInformativeText("Voulez vous les supprimer ?");
				msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Ok);
				msgBox.setDefaultButton(QMessageBox::Cancel);
				int ret = msgBox.exec();
				if (ret == QMessageBox::Cancel) {
					return;
				}
				assert(ret == QMessageBox::Ok);
			}
			viewport->removeStateFromAutomata(*state);

		} else if (selected->getType() == TRANSITION) {
			auto *transition = (TransitionDrawable *) selected;
			viewport->removeTransitionFromAutomata(*transition);
		}
		switchToEmptyDockWidget();
	}
}

AutomataViewport *AutomataController::getCurrentViewport() {
	if (mainWindow->getTabWidget()->count() == 0) {
		return nullptr;
	}
	int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
	if (viewportNumber == 0) {
		return mainWindow->getCurrentAutomataWindow()->getAutomataViewportProjection();
	} else if (viewportNumber == 1) {
		return mainWindow->getCurrentAutomataWindow()->getAutomataViewportGeometry();
	}
	return nullptr;
}

void AutomataController::updateMenuBarGrid() {
	if (toolBar->gridChecked()) {
		menuBar->checkGrid();
	} else {
		menuBar->uncheckGrid();
	}
	updateGrid();
}

void AutomataController::updateToolBarGrid() {
	if (menuBar->gridChecked()) {
		toolBar->checkGrid();
	} else {
		toolBar->uncheckGrid();
	}
	updateGrid();
}

void AutomataController::updateGrid() {
	if (mainWindow->getTabWidget()->count() != 0) {
		if (!toolBar->gridChecked() || !menuBar->gridChecked()) {
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportGeometry()
			          ->setBlankBackground();
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportProjection()
			          ->setBlankBackground();
		} else if (preferences->squareChecked()) {
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportGeometry()
			          ->setSquareBackground();
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportProjection()
			          ->setSquareBackground();
		} else if (preferences->triangleChecked()) {
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportGeometry()
			          ->setTriangularBackground();
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportProjection()
			          ->setTriangularBackground();
		} else {
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportGeometry()
			          ->setCrossBackground();
			mainWindow->getCurrentAutomataWindow()
			          ->getAutomataViewportProjection()
			          ->setCrossBackground();
		}
		loadPreferenceData();
		updateDrawing();
	}
}

void AutomataController::setStyleSelectedItem() {

	if (mainWindow->getTabWidget()->count() != 0) {
		Selectable *selected;
		int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
		if (viewportNumber == 0 || viewportNumber == 1) {
			selected = AutomataDrawable::getSelectedItem();
		} else {
			return;
		} // No correct value

		switch (selected->getType()) {
		case STATE: {
			auto *state = (StateDrawable *) selected;
			state->setLocalPenColor(stateDockWidget->getLineColorPicker()->getCurrentColor());
			state->setLocalPenSize(stateDockWidget->getLineWidget()->getLineSizePicker()->value());
			state->setLocalPenStyle(stateDockWidget->getLineWidget()
			                                       ->getLinePicker()
			                                       ->getFocusedStyle());
			state->setLocalBrushColor(stateDockWidget->getBackgroundColorPicker()
			                                         ->getCurrentColor());


			emit state->localStyleChanged();
			break;
		}
		case TRANSITION: {
			auto *transition = (TransitionDrawable *) selected;
			transition->setLocalPenColor(transitionDockWidget->getLineColorPicker()
			                                                 ->getCurrentColor());
			transition->setLocalPenSize(transitionDockWidget->getLineWidget()
			                                                ->getLineSizePicker()
			                                                ->value());
			transition->setLocalPenStyle(transitionDockWidget->getLineWidget()
			                                                 ->getLinePicker()
			                                                 ->getFocusedStyle());
			transition->setLocalAccuracy(transitionDockWidget->getTransitionAccuracySlider()
			                                                 ->getValue());

			emit transition->localStyleChanged();
			break;
		}
		}
		updateDrawing();
	}
}

void AutomataController::setPropertiesSelectedItem() {
	if (mainWindow->getTabWidget()->count() != 0) {
		Selectable *selected;
		int viewportNumber = mainWindow->getCurrentAutomataWindow()->getViewportNumber();
		if (viewportNumber == 0 || viewportNumber == 1) {
			selected = AutomataDrawable::getSelectedItem();
		} else {
			return;
		} // No correct value

		switch (selected->getType()) {
		case STATE: {
			auto *state = (StateDrawable *) selected;

			std::string name = stateDockWidget->getEditText()->text().toUtf8().constData();
			state->getData().setLabel(name);
			state->setIsFinal(stateDockWidget->getFinalState()->checkState() == Qt::Checked);
			state->setIsInitial(stateDockWidget->getInitialState()->checkState() == Qt::Checked);
			state->updateText();
			if (stateDockWidget->getHidingCheckBox()->checkState() == Qt::Checked) {
				state->removeFromScene();
			} else {
				state->addToScene();
			}
			break;
		}
		case TRANSITION: {
			auto *transition = (TransitionDrawable *) selected;
			std::string weight = transitionDockWidget->getWeightText()->text().toUtf8().constData();
			std::string
					name = transitionDockWidget->getTransitionText()->text().toUtf8().constData();
			transition->getData()->setWeight(weight);
			transition->getData()->setLabel(name);
			transition->updateText();

			if (transitionDockWidget->getHidingCheckBox()->checkState() == Qt::Checked) {
				transition->removeFromScene();
			} else {
				transition->addToScene();
			}
			break;
		}
		}
		updateDrawing();
	}
}

void AutomataController::updatesDockWidget(Selectable &selectedItem) {
	QPen *usedPen;
	QBrush *usedBrush;
	if (selectedItem.getType() == STATE) {
		switchToStateDockWidget();

		// Blocking signals to avoid the update of the selected item
		bool lineColorPickerOldBool = stateDockWidget->getLineColorPicker()->blockSignals(true);
		bool backgroundColorPickerOldBool =
				stateDockWidget->getBackgroundColorPicker()->blockSignals(true);
		bool linePickerOldBool =
				stateDockWidget->getLineWidget()->getLinePicker()->blockSignals(true);
		bool lineSizePickerOldBool =
				stateDockWidget->getLineWidget()->getLineSizePicker()->blockSignals(true);
		bool editTextOldBool = stateDockWidget->getEditText()->blockSignals(true);
		bool initialStateOldBool = stateDockWidget->getInitialState()->blockSignals(true);
		bool finalStateOldBool = stateDockWidget->getFinalState()->blockSignals(true);
		bool hidingCheckBoxOldBool = stateDockWidget->getHidingCheckBox()->blockSignals(true);

		auto *state = (StateDrawable *) &selectedItem;
		usedPen = state->getChosenPen();
		usedBrush = state->getChosenBrush();
		QColor lineColor = usedPen->color();
		QColor backgroundColor = usedBrush->color();
		Qt::PenStyle lineStyle = usedPen->style();
		QString name = QString::fromUtf8(state->getData().getLabel().c_str());
		stateDockWidget->getLineColorPicker()->setCurrentColor(lineColor);
		stateDockWidget->getBackgroundColorPicker()->setCurrentColor(backgroundColor);
		stateDockWidget->getLineWidget()->getLinePicker()->setFocusedStyle(lineStyle);
		stateDockWidget->getLineWidget()->getLineSizePicker()->setValue(usedPen->width());
		stateDockWidget->getEditText()->setText(name);
		if (state->getIsInitial()) {
			stateDockWidget->getInitialState()->setCheckState(Qt::Checked);
		} else {
			stateDockWidget->getInitialState()->setCheckState(Qt::Unchecked);
		}
		if (state->getIsFinal()) {
			stateDockWidget->getFinalState()->setCheckState(Qt::Checked);
		} else {
			stateDockWidget->getFinalState()->setCheckState(Qt::Unchecked);
		}

		if (state->getIsVisibleInScene()) {
			stateDockWidget->getHidingCheckBox()->setCheckState(Qt::Unchecked);
		} else {
			stateDockWidget->getHidingCheckBox()->setCheckState(Qt::Checked);
		}

		// Putting the signals at the same value as they was before
		stateDockWidget->getLineColorPicker()->blockSignals(lineColorPickerOldBool);
		stateDockWidget->getBackgroundColorPicker()->blockSignals(backgroundColorPickerOldBool);
		stateDockWidget->getLineWidget()->getLinePicker()->blockSignals(linePickerOldBool);
		stateDockWidget->getLineWidget()->getLineSizePicker()->blockSignals(lineSizePickerOldBool);
		stateDockWidget->getEditText()->blockSignals(editTextOldBool);
		stateDockWidget->getInitialState()->blockSignals(initialStateOldBool);
		stateDockWidget->getFinalState()->blockSignals(finalStateOldBool);
		stateDockWidget->getHidingCheckBox()->blockSignals(hidingCheckBoxOldBool);

		QVector<SelectableGroup *> attachedGroup = state->getAttachedGroups();
		stateDockWidget->getGroupComboBox()->clear();
		stateDockWidget->getGroupComboBox()->addItem("Aucun groupe");
		if (attachedGroup.size() > 1) {
			stateDockWidget->getGroupComboBox()->clear();
			for (auto i : attachedGroup) {
				if (strcmp(i->getName().c_str(), "selection") != 0) {
					stateDockWidget->getGroupComboBox()->addItem(i->getName().c_str());
				}
			}
		}
	}

	if (selectedItem.getType() == TRANSITION) {
		auto *transition = (TransitionDrawable *) &selectedItem;
		if (transition->getIsInitial()) {
			switchToEmptyDockWidget();
		} else {
			switchToTransitionDockWidget();

			// Blocking signals to avoid the update of the selected item
			bool lineColorPickerOldBool =
					transitionDockWidget->getLineColorPicker()->blockSignals(true);
			bool linePickerOldBool =
					transitionDockWidget->getLineWidget()->getLinePicker()->blockSignals(true);
			bool lineSizePickerOldBool =
					transitionDockWidget->getLineWidget()->getLineSizePicker()->blockSignals(true);
			bool transitionSliderOldBool = transitionDockWidget->getTransitionAccuracySlider()
			                                                   ->getSlider()
			                                                   ->blockSignals(true);
			bool hidingCheckBoxOldBool =
					transitionDockWidget->getHidingCheckBox()->blockSignals(true);
			bool textPickerOldBool = transitionDockWidget->getTransitionText()->blockSignals(true);
			bool weightPickerOldBool = transitionDockWidget->getWeightText()->blockSignals(true);


			usedPen = transition->getChosenPen();
			QColor lineColor = usedPen->color();
			Qt::PenStyle lineStyle = usedPen->style();
			QString label = QString::fromUtf8(transition->getData()->getLabel().c_str());
			QString weight = QString::fromUtf8(transition->getData()->getWeight().c_str());
			transitionDockWidget->getTransitionText()->setText(label);
			transitionDockWidget->getWeightText()->setText(weight);
			transitionDockWidget->getLineColorPicker()->setCurrentColor(lineColor);
			transitionDockWidget->getLineWidget()->getLinePicker()->setFocusedStyle(lineStyle);
			transitionDockWidget->getLineWidget()->getLineSizePicker()->setValue(usedPen->width());
			transitionDockWidget->getTransitionAccuracySlider()
			                    ->setValue(transition->getChosenAccuracy());

			if (transition->getIsVisibleInScene()) {
				transitionDockWidget->getHidingCheckBox()->setCheckState(Qt::Unchecked);
			} else {
				transitionDockWidget->getHidingCheckBox()->setCheckState(Qt::Checked);
			}

			// Putting the signals at the same value as they was before
			transitionDockWidget->getLineColorPicker()->blockSignals(lineColorPickerOldBool);
			transitionDockWidget->getLineWidget()->getLinePicker()->blockSignals(linePickerOldBool);
			transitionDockWidget->getLineWidget()
			                    ->getLineSizePicker()
			                    ->blockSignals(lineSizePickerOldBool);
			transitionDockWidget->getTransitionAccuracySlider()
			                    ->getSlider()
			                    ->blockSignals(transitionSliderOldBool);
			transitionDockWidget->getHidingCheckBox()->blockSignals(hidingCheckBoxOldBool);
			transitionDockWidget->getTransitionText()->blockSignals(textPickerOldBool);
			transitionDockWidget->getWeightText()->blockSignals(weightPickerOldBool);
		}
	}

}

void AutomataController::changeSelectedGroupPriority(int p) {

	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->changePriority(p);
			}
		}
	}
}

void AutomataController::setSelectedGroupName(const QString &name) {
	if (mainWindow->getTabWidget()->count() != 0) {
		QComboBox *groupeBox = toolBar->getAffichageGroupeBox();
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->setName(name.toStdString());

				groupeBox->setItemText(groupeBox->currentIndex(), name);
			}
		}
	}
}

void AutomataController::setEditableSelectableGroup(int b) {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->setEditable(b);
			}
		}
	}
}

void AutomataController::setDrawingStateSelectableGroup(int state) {

	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);
		bool isChecked = (state == Qt::Checked);
		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->setApplyStyle(isChecked);
			}
			viewport->getScene()->getAutomataDrawable()->update();
		}
	}
}

void AutomataController::setSelectableGroupPenSize(int size) {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->setPenSize(size);
			}
		}
	}

	updateDrawing();
}

void AutomataController::setSelectableGroupPenStyle() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				Qt::PenStyle penStyle =
						groupeDockWidget->getStyleSizePen()->getLinePicker()->getFocusedStyle();
				selectedGroup->setPenStyle(penStyle);
			}
		}
	}

	updateDrawing();
}

void AutomataController::setSelectableGroupPenColor() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->setPenColor(groupeDockWidget->getColorPickerPen()
				                                           ->getCurrentColor());
			}
		}
	}
	updateDrawing();
}

void AutomataController::setSelectableGroupBrushColor() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				selectedGroup->setBrushColor(groupeDockWidget->getColorPickerBrush()
				                                             ->getCurrentColor());
			}
		}
	}
}

void AutomataController::createStyleGroup() {

	if (mainWindow->getTabWidget()->count() != 0) {


		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);
		viewport->getScene()->getAutomataDrawable()->createGroupWithSelectedItems();

		updateGroupList();
	}
}

void AutomataController::updateGroupList() {


	if (mainWindow->getTabWidget()->count() != 0) {

		QComboBox *groupeBox = toolBar->getAffichageGroupeBox();
		AutomataViewport *viewport = getCurrentViewport();
		int length = viewport->getScene()->getAutomataDrawable()->getNumSelectableGroups();
		groupeBox->clear();
		groupeBox->addItem("Aucun groupe");
		for (int i = 0; i < length; i++) {
			string name =
					viewport->getScene()->getAutomataDrawable()->getSelectableGroup(i)->getName();
			groupeBox->addItem(QString(name.c_str()));
		}
		groupeBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
		int boxLength = groupeBox->count();
		groupeBox->setCurrentIndex(boxLength - 1);
	}
}

void AutomataController::deleteStyleGroup() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);
			viewport->getScene()->getAutomataDrawable()->delSelectedGroup();

		}
		int length = viewport->getScene()->getAutomataDrawable()->getNumSelectableGroups();
		QComboBox *groupeBox = toolBar->getAffichageGroupeBox();
		groupeBox->clear();
		groupeBox->addItem("Aucun groupe");
		for (int i = 0; i < length; i++) {
			string name =
					viewport->getScene()->getAutomataDrawable()->getSelectableGroup(i)->getName();
			groupeBox->addItem(QString(name.c_str()));
		}
		groupeBox->setSizeAdjustPolicy(QComboBox::AdjustToContents);
	}
}

void AutomataController::switchBetweenEmptyAndGroupDock(int currentIndex) {
	if (currentIndex == 0) {
		switchToEmptyDockWidget();
	} else {
		loadGroupDock();
		switchToGroupDockWidget();
	}
}

void AutomataController::loadGroupDock() {
	if (mainWindow->getTabWidget()->count() != 0) {
		AutomataViewport *viewport = getCurrentViewport();
		assert(viewport != nullptr);

		if (toolBar->getCurrentSelectedGroup() > 0) {
			viewport->getScene()
			        ->getAutomataDrawable()
			        ->loadGroup(toolBar->getCurrentSelectedGroup() - 1);

			SelectableGroup *selectedGroup = AutomataDrawable::getSelectedItems();
			if (selectedGroup != nullptr) {
				// loading all the properties of selectedGroup in the widget of the groupDock
				groupeDockWidget->setNameWidget(selectedGroup->getName());
				groupeDockWidget->setPrioritySpinBox(selectedGroup->getPriority());
				groupeDockWidget->setCheckEditable(selectedGroup->isEditable());
				groupeDockWidget->setCheckApplyStyle(selectedGroup->getApplyStyle());
				groupeDockWidget->setColorPickerPen(selectedGroup->getPen()->color());
				groupeDockWidget->setColorPickerBrush(selectedGroup->getBrush()->color());
				groupeDockWidget->setSizePen(selectedGroup->getPen()->width());
				groupeDockWidget->setStylePen(selectedGroup->getPen()->style());
			}
		}
	}
}

void AutomataController::setLockAddGroupButton(int i) {
	if (i == 0) {
		toolBar->getAffichageGroupeBox()->setEnabled(true);
		toolBar->getActionAddGroupe()->setEnabled(true);
		toolBar->getActionAddGroupeTexte()->setEnabled(true);
	} else {
		toolBar->getActionAddGroupe()->setEnabled(false);
		toolBar->getActionAddGroupeTexte()->setEnabled(false);
	}
}

void AutomataController::setLockChangeGroupButton(int editable) {
	if (editable) {
		toolBar->getAffichageGroupeBox()->setEnabled(false);
	} else if (!toolBar->getAffichageGroupeBox()->isEnabled()) {
		toolBar->getAffichageGroupeBox()->setEnabled(true);
	}
}

void AutomataController::refreshGroupIndex() {
	if (!isSelectedGroupEditable()) {
		toolBar->getAffichageGroupeBox()->setCurrentIndex(0);
	}
}

bool AutomataController::isSelectedGroupEditable() {

	if (mainWindow->getTabWidget()->count() == 0) {
		return false;
	}
	AutomataViewport *viewport = getCurrentViewport();
	assert(viewport != nullptr);

	if (toolBar->getCurrentSelectedGroup() > 0) {
		SelectableGroup *selectedGroup = viewport->getScene()
		                                         ->getAutomataDrawable()
		                                         ->getSelectableGroup(
				                                         toolBar->getCurrentSelectedGroup() - 1);
		return selectedGroup->isEditable();
	}
	return false;
}

void AutomataController::quitApplication() {
	mainWindow->quitApplication();
}

void AutomataController::closeTab(int index) {
	mainWindow->getTabWidget()->removeTab(index);
}

void AutomataController::tabSelected() {
	if (rightDockWidget->isHidden()) {
		hideAllRightDockWidget();
		rightDockWidget->setHidden(false);
	}
}

int main(int argc, char *argv[]) {
	QScopedPointer<QCoreApplication> app(new QApplication(argc, argv));
	QScopedPointer<AutomataController> controller(new AutomataController());

	return app->exec();
}
