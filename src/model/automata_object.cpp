#include "automata_object.hpp"

namespace VisualAutomata {

int AutomataObject::maxId = 0;

bool AutomataObject::setStyle(Style &newStyle) {
	this->style = &newStyle;
	return true;
}

bool AutomataObject::setStyle(string str) {
	this->style->setStyle(std::move(str));
	return true;
}

AutomataObject::AutomataObject(const AutomataObject &object) {
	label = object.label;
	style = object.style;
	id = object.id;
}

AutomataObject::AutomataObject() {
	id = maxId++;
	style = new Style();
}

AutomataObject::~AutomataObject() {
	delete style;
}

AutomataObject::AutomataObject(std::string label) : AutomataObject() {
	this->label = std::move(label);
}

int AutomataObject::getId() const {
	return this->id;
}

std::string AutomataObject::getLabel() const {
	return this->label;
}

Style &AutomataObject::getStyle() const {
	return *style;
}

bool AutomataObject::setLabel(std::string newLabel) {
	label = std::move(newLabel);
	return true;
}

bool AutomataObject::operator<(const AutomataObject &s) const {
	return this->getId() < s.getId();
}

AutomataObject &AutomataObject::operator=(const AutomataObject &object) {
	if (&object == this) {
		return *this;
	}
	label = object.label;
	style = object.style;
	id = object.id;
	return *this;
}
}  // namespace VisualAutomata
