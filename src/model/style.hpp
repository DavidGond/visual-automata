#ifndef VISUAL_AUTOMATA_STYLE_HPP
#define VISUAL_AUTOMATA_STYLE_HPP

#include <string>

#include <QPen>

using namespace std;

namespace VisualAutomata {

class Style {
	string styleStr;

	/**< The name of the attribute for a line color */
	static string lineColorAttribute;
	/**< The name of the attribute for a line style */
	static string lineStyleAttribute;
	/**< The name of the attribute for a line size */
	static string lineSizeAttribute;
	/**< The name of the attribute for a background color */
	static string backgroundColorAttribute;
	/**< The suffix which must be appended at the end of an array to save the length */
	static string qvectorLenSuffix;

	/**< The name of a true value */
	static string trueValue;
	/**< The name of a false value */
	static string falseValue;

public:
	/**< The name of the attribute for a the boolean saying if the object has a local style */
	static string hasLocalStyleAttribute;
	/**< The name of the attribute for a x coordinate of a state */
	static string stateXAttribute;
	/**< The name of the attribute for a y coordinate of a state */
	static string stateYAttribute;
	/**< The name of the attribute for a x coordinate of a control points of a transition */
	static string transitionXAttribute;
	/**< The name of the attribute for a y coordinate of a control points of a transition */
	static string transitionYAttribute;
	/**< The name of the attribute describing the priority of a group */
	static string groupPriorityAttribute;
	/**< The name of the attribute for a the boolean saying if the style of a group must be applied */
	static string groupApplyStyleAttribute;

	static bool useCoordinates;

public:
	/**
	 * Constructor.
	 */
	Style();

	/**
	 * Method.
	 *
	 * Sets the style.
	 * @param str the string representing the style.
	 */
	void setStyle(string str);

	/**
	 * Method.
	 *
	 * Returns the string representing the style.
	 */
	string getStr();

	/**
	 * Method.
	 *
	 * Returns the corresponding value of the attribute.
	 * @param attribute the attribute.
	 */
	string getValue(const string &attribute) const;

	/**
	 * Method.
	 *
	 * Returns the corresponding value of the attribute.
	 * @param attribute the attribute.
	 */
	int getValueInt(const string &attribute) const;

	/**
	 * Method.
	 *
	 * Removes an attribute.
	 */
	void deleteAttribute(const string &attribute);

	/**
	 * Method.
	 *
	 * Sets an attribute.
	 * @param attribute the attribute.
	 * @param value the value.
	 */
	void setAttribute(const string &attribute, const string &value);

	/**
	 * Returns whether the style has no attribute.
	 */
	bool isEmpty();

	/**
	 * Method.
	 * Converts a QString to an std::string
	 * @param string the QString to convert
	 * @return the std::string converted
	 */
	static string toString(const QString &string);

	/**
	 * Method.
	 * Converts an std::string to a QString
	 * @param string the std::string to convert
	 * @return the QString converted
	 */
	static QString toQString(const string &string);

	/**
	 * Method.
	 * Loads all values stored in the style for a pen
	 * @param toLoad the pen in which the data must be loaded
	 */
	void loadPen(QPen &toLoad) const;

	/**
	 * Method.
	 * Saves all values from a pen in the style
	 * @param toSave the pen in from which the data must be saved
	 */
	void savePen(QPen &toSave);

	/**
	 * Method.
	 * Loads all values stored in the style for a brush
	 * @param toLoad the brush in which the data must be loaded
	 */
	void loadBrush(QBrush &toLoad) const;

	/**
	 * Method.
	 * Saves all values from a brush in the style
	 * @param toSave the brush in from which the data must be saved
	 */
	void saveBrush(QBrush &toSave);

	/**
	 * Method.
	 * @return true if the value is valid
	 */
	static bool isValidValue(const string &value);

	/**
	 * Method.
	 * Saves a boolean in the style.
	 * @param attribute the name of the boolean to save.
	 * @param toSave the boolean to save.
	 */
	void saveBool(const string &attribute, bool toSave);

	/**
	 * Method.
	 * Loads a boolean from the style.
	 * @param attribute the name of the boolean to save.
	 * @param oldValue the default value of the boolean.
	 * @return the value stored if there is one, else it returns the default value.
	 */
	bool loadBool(const string &attribute, bool oldValue) const;

	/**
	 * Method.
	 * Saves an integer in the style.
	 * @param attribute the name of the integer to save.
	 * @param toSave the integer to save.
	 */
	void saveInt(const string &attribute, int toSave);

	/**
	 * Method.
	 * Loads an integer from the style.
	 * @param attribute the name of the integer to save.
	 * @param oldValue the default value of the integer.
	 * @return the value stored if there is one, else it returns the default value.
	 */
	int loadInt(const string &attribute, int oldValue) const;

	/**
	 * Method.
	 * Saves a float in the style.
	 * @param attribute the name of the float to save.
	 * @param toSave the float to save.
	 */
	void saveFloat(const string &attribute, float toSave);

	/**
	 * Method.
	 * Loads an float from the style.
	 * @param attribute the name of the float to save.
	 * @param oldValue the default value of the float.
	 * @return the value stored if there is one, else it returns the default value.
	 */
	float loadFloat(const string &attribute, float oldValue) const;

	/**
	 * Method.
	 * Saves a vector of QString in the style.
	 * Saves it's length by adding Style::qvectorLenSuffix to the name of the vector and all elements by adding the index the element to the name of the vector.
	 * @param attribute the name of the vector to save.
	 * @param toSave the vector to save.
	 */
	void saveQVectorQString(const string &attribute, QVector<QString> &toSave);

	/**
	 * Method.
	 * Loads a QVector of QString from the style.
	 * @param attribute the name of the vector to load.
	 * @param toLoad the vector in which the data must be loaded.
	 */
	void loadQVectorQString(const string &attribute, QVector<QString> &toLoad) const;

};
}  // namespace VisualAutomata
#endif //VISUAL_AUTOMATA_STYLE_HPP
