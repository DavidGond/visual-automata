#ifndef VISUAL_AUTOMATA_STATE_HPP
#define VISUAL_AUTOMATA_STATE_HPP

#include <string>
#include "automata_object.hpp"

namespace VisualAutomata {

class State: public AutomataObject {
	static Style *defaultStyle;
	std::string probability;
	bool initial;
	bool final;

public:
	/**
	 * Constructor.
	 */
	State();

	/**
	 * Constructor.
	 *
	 * @param state the state to copy.
	 */
	State(const State &state) = default;

	/**
	 * Sets the initial value.
	 * @param set the value to set.
	 */
	bool setInitial(bool set);

	/**
	 * Sets the final value.
	 * @param set the value to set.
	 */
	bool setFinal(bool set);

	/**
	 * Returns the initial value.
	 */
	bool isInitial() const;

	/**
	 * Returns the final value.
	 */
	bool isFinal() const;

	/**
	 * Sets the probability.
	 * @param p the probability.
	 */
	void setProbability(std::string p);

	/**
	 * Returns the probability.
	 */
	std::string getProbability() const;

	State &operator=(const State &s);
};
}  // namespace VisualAutomata

#endif
