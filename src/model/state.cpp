#include "state.hpp"

namespace VisualAutomata {

State::State() : AutomataObject() {
	initial = false;
	final = false;
}

State &State::operator=(const State &s) {
	if (&s == this) { // Handles self-assignment
		return *this;
	}
	AutomataObject::operator=(s);
	this->probability = s.probability;
	this->final = s.final;
	this->initial = s.initial;
	return *this;
}

bool State::setInitial(bool set) {
	initial = set;
	return EXIT_SUCCESS;
}

bool State::setFinal(bool set) {
	final = set;
	return EXIT_SUCCESS;
}

bool State::isInitial() const {
	return initial;
}

bool State::isFinal() const {
	return final;
}

void State::setProbability(std::string p) {
	probability = std::move(p);
}

std::string State::getProbability() const {
	return this->probability;
}
}  // namespace VisualAutomata
