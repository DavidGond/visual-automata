#include "automata.hpp"
#include <iostream>
#include "utils.hpp"


namespace VisualAutomata {

Automata::Automata() : AutomataObject() {
	initGroup = new StateGroup();
	finalGroup = new StateGroup();
}

Automata::~Automata() {
	delete initGroup;
	delete finalGroup;
}

bool Automata::addState(State &state) {
	return addObjectToMap<State>(state, states);
}

bool Automata::delState(State &state) {
	for (auto &transition : transitions) {
		if (&transition.second->getSource() == &state
				|| &transition.second->getSource() == &state) {
			cout << "delete transitions before states" << endl;
			assert(false);
		}
	}
	delJsonId(state.getId());
	return delObjectFromMap<State>(state, states);
}

bool Automata::addTransition(Transition &transition) {
	return addObjectToMap<Transition>(transition, transitions);
}

bool Automata::delTransition(Transition &transition) {
	return delObjectFromMap<Transition>(transition, transitions);
}

bool Automata::addInitialState(State &state) {
	bool r;
	r = initGroup->addState(state);
	return r;
}

bool Automata::addFinalState(State &state) {
	bool r;
	r = finalGroup->addState(state);
	return r;
}

bool Automata::addJsonId(int id, int jsonId) {
	jsonIds[id] = jsonId;
	return EXIT_SUCCESS;
}

bool Automata::delJsonId(int id) {
	auto search = jsonIds.find(id);
	if (search != jsonIds.end()) {
		jsonIds.erase(search);
		return true;
	}
	return false;
}

bool Automata::setInitialStyle(Style &style) const {
	getInitial().setStyle(style);
	return false;
}

bool Automata::setFinalStyle(Style &style) const {
	getFinal().setStyle(style);
	return false;
}

bool Automata::addGroup(StateGroup &group) {
	return addObjectToMap<StateGroup>(group, groups);
}

bool Automata::delGroup(StateGroup &group) {
	delJsonId(group.getId());
	return delObjectFromMap<StateGroup>(group, groups);
}

bool Automata::addStateToGroup(StateGroup &group, State &state) {
	return group.addState(state);
}

bool Automata::delStateFromGroup(StateGroup &group, State &state) {
	return group.delState(state);
}

int Automata::getId(int jsonId) const {
	for (auto &id : jsonIds)
		if (id.second == jsonId) {
			return id.first;
		}
	cout << "Json ID doesn't exist";
	return -1;
}

int Automata::getJsonId(const AutomataObject &object) const {
	auto search = jsonIds.find(object.getId());
	if (search == jsonIds.end()) {
		cout << "Json ID doesn't exist";
		return search->second;
	}
	return search->second;
}

size_t Automata::getStateCount() const {
	return (this->states).size();
}

map<int, State *> Automata::getStates() const {
	return states;
}

State *Automata::getState(int id) {
	if (states.find(id) != states.end()) {
		return &getObjectFromMap<State>(id, states);
	}
	return nullptr;
}

size_t Automata::getTransitionCount() const {
	return (this->transitions).size();
}

Transition &Automata::getTransition(int id) {
	return getObjectFromMap<Transition>(id, transitions);
}

size_t Automata::getGroupCount() const {
	return (this->groups).size();
}

map<int, Transition *> Automata::getTransitions() const {
	return this->transitions;
}

StateGroup &Automata::getGroup(int id) {
	return getObjectFromMap<StateGroup>(id, groups);
}

StateGroup &Automata::getInitial() const {
	return *initGroup;
}

StateGroup &Automata::getFinal() const {
	return *finalGroup;
}

map<int, StateGroup *> Automata::getGroups() const {
	return groups;
}

string Automata::getPath() const {
	return path;
}

void Automata::setPath(string newPath) {
	this->path = std::move(newPath);
}
}  // namespace VisualAutomata
