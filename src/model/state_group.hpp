#ifndef VISUAL_AUTOMATA_STATE_GROUP_HPP
#define VISUAL_AUTOMATA_STATE_GROUP_HPP

#include <set>
#include "automata_object.hpp"
#include "state.hpp"
#include "style.hpp"

namespace VisualAutomata {

class StateGroup: public AutomataObject {
	std::set<State *> states;

public:
	/**
	 * Constructor.
	 */
	StateGroup() = default;

	/**
	 * Method.
	 *
	 * Adds a state.
	 * Returns 0 if the state is added, then 1.
	 * @param state the state to add.
	 */
	bool addState(State &state);

	/**
	 * Method.
	 *
	 * Deletes a state.
	 * Returns 0 if the state is deleted, then 1.
	 * @param state the state to delete.
	 */
	bool delState(State &state);

	/**
	 * Method.
	 *
	 * Returns the states.
	 */
	std::set<State *> getStates() const;
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_STATE_GROUP_HPP
