TEMPLATE = lib

HEADERS += \
    style.hpp \
    state.hpp \
    state_group.hpp \
    automata.hpp \
    automata_object.hpp \
    transition.hpp \

SOURCES += \
    style.cpp \
    state.cpp \
    state_group.cpp \
    automata.cpp \
    automata_object.cpp \
    transition.cpp \