#ifndef VISUAL_AUTOMATA_TRANSITION_HPP
#define VISUAL_AUTOMATA_TRANSITION_HPP

#include <string>
#include "automata_object.hpp"
#include "style.hpp"
#include "state.hpp"

namespace VisualAutomata {

class Transition: public AutomataObject {
	static Style *defaultStyle;
	State &source;
	State &destination;
	std::string weight;

public:
	Transition() = delete;

	/**
	 * Constructor.
	 * @param source the source of the transition.
	 * @param destination the destination of the transition.
	 * @param label the label of the transition.
	 */
	Transition(State &source, State &destination, std::string label);

	/**
	 * Constructor.
	 * @param transition the transition to copy.
	 */
	Transition(const Transition &transition) = default;

	/**
	 * Returns the source.
	 */
	State &getSource() const;

	/**
	 * Returns the destination.
	 */
	State &getDestination() const;

	/**
	 * Returns the weight.
	 */
	std::string getWeight() const;

	/**
	 * Sets the weight.
	 * @param w the weight.
	 */
	bool setWeight(int w);

	/**
	 * Sets the source.
	 * @param state the source.
	 */
	bool setSource(State &state);

	/**
	 * Sets the destination.
	 * @param state the destination.
	 */
	bool setDestination(State &state);

	/**
	 * Sets the weight.
	 * @param w the weight.
	 */
	bool setWeight(std::string w);

	Transition &operator=(const Transition &t);
};
}  // namespace VisualAutomata

#endif
