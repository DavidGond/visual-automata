#include <vector>
#include <iostream>
#include <QDebug>
#include "style.hpp"
#include "utils.hpp"


namespace VisualAutomata {

string Style::lineColorAttribute = "lineColor";

string Style::hasLocalStyleAttribute = "hasLocalStyle";

string Style::lineStyleAttribute = "lineStyle";

string Style::lineSizeAttribute = "lineSize";

string Style::backgroundColorAttribute = "backgroundColor";

string Style::stateXAttribute = "x";

string Style::stateYAttribute = "y";

string Style::qvectorLenSuffix = "Len";

string Style::transitionXAttribute = "controlPointX";

string Style::transitionYAttribute = "controlPointY";

string Style::groupPriorityAttribute = "groupPriority";

string Style::groupApplyStyleAttribute = "groupApplyStyle";

string Style::trueValue = "1";

string Style::falseValue = "0";

bool Style::useCoordinates = true;

Style::Style() {
	styleStr = "";
}

void Style::setStyle(string str) {
	styleStr = move(str);
}

string Style::getStr() {
	return styleStr;
}

string Style::getValue(const string &attribute) const {
	vector<string> v = split(styleStr, ';');
	for (auto &s : v) {
		vector<string> att = split(s, ':');
		if (att.size() == 2 && att.at(0) == attribute) {
			return att.at(1);
		}
	}
	return "";
}

int Style::getValueInt(const string &attribute) const {
	string value = getValue(attribute);
	stringstream ss(value);
	int valueInt;
	ss >> valueInt;
	return valueInt;
}

void Style::deleteAttribute(const string &attribute) {
	stringstream ss;
	vector<string> v = split(styleStr, ';');
	for (auto &s : v) {
		vector<string> att = split(s, ':');
		if (att.size() == 2 && att.at(0) != attribute) {
			if (!ss.str().empty()) {
				ss << ";";
			}
			ss << s;
		}
	}
	styleStr = ss.str();
}

void Style::setAttribute(const string &attribute, const string &value) {
	stringstream ss;
	vector<string> v = split(styleStr, ';');
	for (const auto &s : v) {
		vector<string> att = split(s, ':');
		if (att.size() == 2 && att.at(0) != attribute) {
			if (!ss.str().empty()) {
				ss << ";";
			}
			ss << s;
		}
	}
	if (!ss.str().empty()) {
		ss << ";";
	}
	ss << attribute << ":" << value;
	styleStr = ss.str();
}

bool Style::isEmpty() {
	return styleStr.empty();
}

string Style::toString(const QString &string) {
	return string.toUtf8().constData();
}

QString Style::toQString(const string &string) {
	return QString::fromStdString(string);
}

void Style::loadPen(QPen &toLoad) const {
	string colorString = getValue(Style::lineColorAttribute);
	if (Style::isValidValue(colorString)) {
		QColor color(toQString(colorString));
		toLoad.setColor(color);
	}

	toLoad.setStyle((Qt::PenStyle) loadInt(Style::lineStyleAttribute, toLoad.style()));

	toLoad.setWidth(loadInt(Style::lineSizeAttribute, toLoad.width()));
}

void Style::savePen(QPen &toSave) {
	setAttribute(Style::lineColorAttribute, toString(toSave.color().name()));
	saveInt(Style::lineStyleAttribute, toSave.style());
	saveInt(Style::lineSizeAttribute, toSave.width());
}

void Style::saveBool(const string &attribute, bool toSave) {
	if (toSave) {
		setAttribute(attribute, Style::trueValue);
	} else {
		setAttribute(attribute, Style::falseValue);
	}
}

bool Style::loadBool(const string &attribute, bool oldValue) const {
	string boolValue = getValue(attribute);
	if (Style::isValidValue(boolValue)) {
		return boolValue == Style::trueValue;
	} else {
		return oldValue;
	}
}

bool Style::isValidValue(const string &value) {
	return !value.empty();
}

void Style::saveInt(const string &attribute, int toSave) {
	setAttribute(attribute, std::to_string(toSave));
}

int Style::loadInt(const string &attribute, int oldValue) const {
	string value = getValue(attribute);
	if (Style::isValidValue(value)) {
		return std::stoi(value);
	} else {
		return oldValue;
	}
}

void Style::saveFloat(const string &attribute, float toSave) {
	setAttribute(attribute, std::to_string(toSave));
}

float Style::loadFloat(const string &attribute, float oldValue) const {
	string value = getValue(attribute);
	if (Style::isValidValue(value)) {
		return std::stof(value);
	} else {
		return oldValue;
	}
}

void Style::loadBrush(QBrush &toLoad) const {
	string colorString = getValue(Style::backgroundColorAttribute);
	if (Style::isValidValue(colorString)) {
		QColor color(toQString(colorString));
		toLoad.setColor(color);
	}
}

void Style::saveBrush(QBrush &toSave) {
	setAttribute(Style::backgroundColorAttribute, toString(toSave.color().name()));
}

void Style::saveQVectorQString(const string &attribute, QVector<QString> &toSave) {
	int oldLen = loadInt(attribute + Style::qvectorLenSuffix, -1);
	if (oldLen != -1) { // The qvector was already stored
		// Deleting old values stored
		for (int k = oldLen; k < toSave.size(); k++) {
			deleteAttribute(attribute + std::to_string(k));
		}
	}

	for (int k = 0; k < toSave.size(); k++) {
		setAttribute(attribute + std::to_string(k), toString(toSave[k]));
	}

	saveInt(attribute + Style::qvectorLenSuffix, toSave.size());

}

void Style::loadQVectorQString(const string &attribute, QVector<QString> &toLoad) const {
	int len = loadInt(attribute + Style::qvectorLenSuffix, -1);
	if (len != -1) { // The qvector was already stored
		for (int k = 0; k < len; k++) {
			toLoad.append(toQString(getValue(attribute + std::to_string(k))));
		}
	}
}
}  // namespace VisualAutomata
