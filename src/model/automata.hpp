#ifndef VISUAL_AUTOMATA_AUTOMATA_HPP
#define VISUAL_AUTOMATA_AUTOMATA_HPP

#include "automata_object.hpp"
#include "state.hpp"
#include "transition.hpp"
#include "state_group.hpp"
#include "style.hpp"

#include <map>

using namespace std;

namespace VisualAutomata {

class Automata: public AutomataObject {
	map<int, State *> states;
	map<int, Transition *> transitions;
	StateGroup *initGroup;
	StateGroup *finalGroup;
	map<int, StateGroup *> groups;
	map<int, int> jsonIds;
	string path;

public:
	/**
	 * Constructor
	 */
	Automata();

	/**
	 * Destructor.
	 */
	~Automata();

	/**
	 * Adds a state.
	 * Returns 0 if the state is added, then 1.
	 * @param state the state to add.
	 */
	bool addState(State &state);

	/**
	 * Deletes a state.
	 * Returns 0 if the state is deleted, then 1.
	 * @param state the state to delete.
	 */
	bool delState(State &state);

	/**
	 * Adds a transition.
	 * Returns 0 if the transition is added, then 1.
	 * @param transition the transition to add.
	 */
	bool addTransition(Transition &transition);

	/**
	 * Deletes a transition.
	 * Returns 0 if the transition is deleted, then 1.
	 * @param transition the transition to delete.
	 */
	bool delTransition(Transition &transition);

	/**
	 * Adds an initial state.
	 * Returns 0 if the state is added, then 1.
	 * @param state the state to add.
	 */
	bool addInitialState(State &state);

	/**
	 * Adds a final state.
	 * Returns 0 if the state is added, then 1.
	 * @param state the state to add.
	 */
	bool addFinalState(State &state);

	/**
	 * Adds a json id.
	 * Returns 0 if the json id is added, then 1.
	 * @param id the id.
	 * @param jsonId the json id.
	 */
	bool addJsonId(int id, int jsonId);

	/**
	 * Deletes a json id.
	 * Returns 0 if the json id is deleted, then 1.
	 * @param id the id.
	 */
	bool delJsonId(int id);

	/**
	 * Sets an initial style.
	 * Returns 0 if the style is set, then 1.
	 * @param style the style.
	 */
	bool setInitialStyle(Style &style) const;

	/**
	 * Sets a final style.
	 * Returns 0 if the style is set, then 1.
	 * @param style the style.
	 */
	bool setFinalStyle(Style &style) const;

	/**
	 * Adds a group.
	 * Returns 0 if the group is added, then 1.
	 * @param group the group to add.
	 */
	bool addGroup(StateGroup &group);

	/**
	 * Deletes a group.
	 * Returns 0 if the group is deleted, then 1.
	 * @param group the group to delete.
	 */
	bool delGroup(StateGroup &group);

	/**
	 * Adds a state in a group.
	 * Returns 0 if the state is added, then 1.
	 * @param group the group.
	 * @param state the state to add.
	 */
	static bool addStateToGroup(StateGroup &group, State &state);

	/**
	 * Removes a state from a group.
	 * Returns 0 if the state is deleted, then 1.
	 * @param group the group.
	 * @param state the state to delete.
	 */
	bool delStateFromGroup(StateGroup &group, State &state);

	/**
	 * Returns the states.
	 */
	map<int, State *> getStates() const;

	/**
	 * Returns the state with the good id.
	 * @param id the id.
	 */
	State *getState(int id);

	/**
	 * Returns the state count.
	 */
	size_t getStateCount() const;

	/**
	 * Returns the transition count.
	 */
	size_t getTransitionCount() const;

	/**
	 * Returns the transition with the good id.
	 * @param id the id.
	 */
	Transition &getTransition(int id);

	/**
	 * Returns the group count.
	 */
	size_t getGroupCount() const;

	/**
	 * Returns the transitions.
	 */
	map<int, Transition *> getTransitions() const;

	/**
	 * Returns the group with the good id.
	 * @param id the id.
	 */
	StateGroup &getGroup(int id);

	/**
	 * Returns the initial group.
	 */
	StateGroup &getInitial() const;

	/**
	 * Returns the final group.
	 */
	StateGroup &getFinal() const;

	/**
	 * Returns the groups.
	 */
	map<int, StateGroup *> getGroups() const;

	/**
	 * Returns the id.
	 * @param jsonId the json id.
	 */
	int getId(int jsonId) const;

	/**
	 * Returns the json id of an object.
	 * @param object the object.
	 */
	int getJsonId(const AutomataObject &object) const;

	/**
	 * Returns the initial style.
	 */
	Style &getInitialStyle() const;

	/**
	 * Returns the final style.
	 */
	Style &getFinalStyle() const;

	/**
	 * Returns the path.
	 */
	string getPath() const;

	/**
	 * Sets the new path.
	 * @param newPath the new path.
	 */
	void setPath(string newPath);
};
}  // namespace VisualAutomata

#endif
