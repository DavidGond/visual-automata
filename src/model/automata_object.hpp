#ifndef VISUAL_AUTOMATA_AUTOMATA_OBJECT_HPP
#define VISUAL_AUTOMATA_AUTOMATA_OBJECT_HPP

#include "style.hpp"

namespace VisualAutomata {

class AutomataObject {
	static int maxId;

	std::string label;

protected:
	/**< Each object has an id **/
	int id;
	Style *style{};

public:

	/**
	 * Constructor.
	 * @param object the object to copy.
	 **/
	AutomataObject(const AutomataObject &object);

	/**
	 * Constructor.
	 * @param label the label of the object.
	 **/
	explicit AutomataObject(std::string label);

	/**
	 * Sets a new style.
	 * @param newStyle the new style of the object.
	 **/
	bool setStyle(Style &newStyle);

	/**
	 * Sets a new style.
	 * @param str the string representing the style.
	 **/
	bool setStyle(string str);

	/**
	 * Returns the style.
	 **/
	Style &getStyle() const;

	/**
	 * Returns the id.
	 **/
	int getId() const;

	/**
	 * Returns the label.
	 **/
	std::string getLabel() const;

	/**
	 * Sets a new label.
	 * @param newLabel the new label.
	 **/
	bool setLabel(std::string newLabel);

	bool operator<(const AutomataObject &s) const;

	AutomataObject &operator=(const AutomataObject &object);

	/**
	 * Constructor
	 **/
	AutomataObject();

	/**
	 * Destructor
	 **/
	~AutomataObject();
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_AUTOMATA_OBJECT_HPP
