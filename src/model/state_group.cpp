#include <iostream>
#include "state_group.hpp"

namespace VisualAutomata {

bool StateGroup::addState(State &state) {
	return states.insert(&state).second;
}

bool StateGroup::delState(State &state) {
	auto search = states.find(&state);
	if (search == states.end()) {
		cout << "delState : State doesn't exist in StateGroup" << endl;
		return false;
	}
	states.erase(search);
	return true;
}

std::set<State *> StateGroup::getStates() const {
	return this->states;
}

}  // namespace VisualAutomata
