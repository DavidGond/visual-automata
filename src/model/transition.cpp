#include "transition.hpp"

namespace VisualAutomata {

Transition::Transition(State &source, State &destination, std::string label) :
		AutomataObject(std::move(label)), source(source), destination(destination) {
}

bool Transition::setWeight(std::string w) {
	weight = std::move(w);
	return true;
}

State &Transition::getSource() const {
	return source;
}

State &Transition::getDestination() const {
	return destination;
}

std::string Transition::getWeight() const {
	return weight;
}

Transition &Transition::operator=(const Transition &t) {
	if (&t == this) { // Handles self-assignment
		return *this;
	}
	source = t.source;
	destination = t.destination;
	weight = t.weight;
	return *this;
}

}  // namespace VisualAutomata
