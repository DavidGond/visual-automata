#include "left_dock_widget.hpp"

LeftDockWidget::LeftDockWidget(QMainWindow *mainWindowP) : QDockWidget(), mainWindow(mainWindowP) {
	// Initializing the leftDock
	this->setAllowedAreas(Qt::LeftDockWidgetArea);
	this->setFeatures(QDockWidget::NoDockWidgetFeatures);
	this->setContentsMargins(10, 10, 10, 10);
	mainWindow->addDockWidget(Qt::LeftDockWidgetArea, this);

	// Adding a title to the LeftDock
	labelAjouter = new QLabel("Ajouter", mainWindow);
	labelAjouter->setAlignment(Qt::AlignCenter);
	this->setTitleBarWidget(labelAjouter);

	// Creating a bold Font for the subtitles
	QFont boldFont = QFont();
	boldFont.setBold(true);

	// Creating 2 labels for the subtitles
	labelEtats = new QLabel("États", this);
	labelEtats->setFont(boldFont);
	labelTransitions = new QLabel("\nTransitions", this);
	labelTransitions->setFont(boldFont);

	// Creating the 3 QToolButtons for the state, initialState and finalState
	QIcon stateIcon = QIcon(":/pictures/state.png");
	stateButton = new QToolButton(this);
	stateButton->setIcon(stateIcon);
	stateButton->setIconSize(QSize(80, 80));

	QIcon initialStateIcon = QIcon(":/pictures/initial_state.png");
	initialStateButton = new QToolButton(this);
	initialStateButton->setIcon(initialStateIcon);
	initialStateButton->setIconSize(QSize(80, 80));

	QIcon finalStateIcon = QIcon(":/pictures/final_state.png");
	finalStateButton = new QToolButton(this);
	finalStateButton->setIcon(finalStateIcon);
	finalStateButton->setIconSize(QSize(80, 80));

	// Creating the 3 QToolButtons for the leftArrow, rightArrow and leftRightArrow
	QIcon leftTransitionIcon = QIcon(":/pictures/left_arrow.png");
	leftTransitionButton = new QToolButton(this);
	leftTransitionButton->setIcon(leftTransitionIcon);
	leftTransitionButton->setIconSize(QSize(80, 80));

	// Creating a vertical layout for the leftDock
	verticalLayout = new QVBoxLayout();
	// Aligning the layout to Top Center
	verticalLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);

	// Adding the widgets to this layout
	verticalLayout->addWidget(labelEtats);
	verticalLayout->addWidget(stateButton);
	verticalLayout->addWidget(initialStateButton);
	verticalLayout->addWidget(finalStateButton);
	verticalLayout->addWidget(labelTransitions);
	verticalLayout->addWidget(leftTransitionButton);

	// Creating an another widget which contains all the widgets for the leftDock and adding to it the vertical layout
	widgetGauche = new QWidget(this);
	widgetGauche->setLayout(verticalLayout);

	// Creating a scrollArea for the leftWidget
	scrollArea = new QScrollArea();
	scrollArea->setAlignment(Qt::AlignCenter);
	scrollArea->setWidget(widgetGauche);
	scrollArea->setWidgetResizable(true);

	// Adding the scrollArea to the leftDock
	this->setWidget(scrollArea);
}

LeftDockWidget::~LeftDockWidget() {
	delete labelAjouter;
	delete labelEtats;
	delete labelTransitions;

	delete stateButton;
	delete initialStateButton;
	delete finalStateButton;
	delete leftTransitionButton;

	delete verticalLayout;
	delete widgetGauche;
	delete scrollArea;
}

QToolButton *LeftDockWidget::getStateButton() const {
	return stateButton;
}

QToolButton *LeftDockWidget::getInitialStateButton() const {
	return initialStateButton;
}

QToolButton *LeftDockWidget::getFinalStateButton() const {
	return finalStateButton;
}

QToolButton *LeftDockWidget::getLeftTransitionButton() const {
	return leftTransitionButton;
}
