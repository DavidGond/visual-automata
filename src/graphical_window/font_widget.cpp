#include "font_widget.hpp"

FontWidget::FontWidget(QWidget *parent) : QWidget(parent) {
	fontLayout = new QHBoxLayout();
	fontLayout->setSpacing(0);
	// Adding a FontButton and putting a "ColorPalette" icon next to it
	fontButton = new QPushButton("Police...");
	QIcon colorPaletteIcon = QIcon(":/pictures/palette.png");
	fontColorPicker = new ColorPickerWidget(this);
	// Adding the 2 widgets to the previous layout
	fontLayout->addWidget(fontButton);
	fontLayout->addWidget(fontColorPicker);
	setLayout(fontLayout);
}

FontWidget::~FontWidget() {
	delete fontButton;
	delete fontColorPicker;
	delete fontLayout;
}

QPushButton *FontWidget::getFontButton() {
	return fontButton;
}

ColorPickerWidget *FontWidget::getFontColorPicker() {
	return fontColorPicker;
}
