#include "line_widget.hpp"
#include "right_dock_widget.hpp"
#include "state_dock_widget.hpp"

StateDockWidget::StateDockWidget(QMainWindow *mainWindowP) :
		RightDockWidget(mainWindowP), mainWindow(mainWindowP) {
	// Creating a bold Font for the subtitles
	QFont boldFont = QFont();
	boldFont.setBold(true);

	// Creating 6 labels for the subtitles
	labelType = new QLabel("Type", this);
	labelType->setFont(boldFont);
	labelPosition = new QLabel("Position", this);
	labelPosition->setFont(boldFont);
	labelTexte = new QLabel("Texte", this);
	labelTexte->setFont(boldFont);
	labelGroupes = new QLabel("Appartenance \ngroupes", this);
	labelGroupes->setFont(boldFont);
	labelBackground = new QLabel("Arrière-plan", this);
	labelBackground->setFont(boldFont);
	labelTrait = new QLabel("Trait", this);
	labelTrait->setFont(boldFont);
	labelDelete = new QLabel("Suppression", this);
	labelDelete->setFont(boldFont);

	// Creating 2 QCheckBox for the Type part with a Layout to center them
	initialState = new QCheckBox("État initial");
	finalState = new QCheckBox("État final");

	layoutState = new QVBoxLayout();
	//layoutState->setAlignment(Qt::AlignLeft);
	layoutState->addWidget(initialState, Qt::AlignLeft);
	layoutState->addWidget(finalState);

	// Creating a new Widget for displaying the 2 QCheckBox
	WidgetState = new QWidget();
	WidgetState->setLayout(layoutState);

	// Creating 5 QLabel with 5 QSpinBox for the Position part

	labelX = new QLabel("X");
	labelX->setAlignment(Qt::AlignCenter);

	XSpinBox = new QSpinBox();
	XSpinBox->setRange(-10000, 10000);
	XWidget = new PairLabelWidget(labelX, XSpinBox, this);

	labelY = new QLabel("Y");
	labelY->setAlignment(Qt::AlignCenter);
	YSpinBox = new QSpinBox();
	YSpinBox->setRange(-10000, 10000);
	YWidget = new PairLabelWidget(labelY, YSpinBox, this);

	labelZ = new QLabel("Z");
	labelZ->setAlignment(Qt::AlignCenter);
	ZSpinBox = new QSpinBox();
	ZSpinBox->setRange(-10000, 10000);
	ZSpinBox->setEnabled(false);
	ZWidget = new PairLabelWidget(labelZ, ZSpinBox, this);

	labelH = new QLabel("Hauteur");
	labelH->setAlignment(Qt::AlignCenter);
	HSpinBox = new QSpinBox();
	HSpinBox->setRange(-10000, 10000);
	HWidget = new PairLabelWidget(labelH, HSpinBox, this);

	labelL = new QLabel("Largeur");
	labelL->setAlignment(Qt::AlignCenter);
	LSpinBox = new QSpinBox();
	LSpinBox->setRange(-10000, 10000);
	LWidget = new PairLabelWidget(labelL, LSpinBox, this);


	// Creating a colorPalette Icon (used 3 times)
	QIcon colorPaletteIcon = QIcon(":/pictures/palette.png");

	// Creating an EditText
	editText = new QLineEdit("E1");
	editText->setAlignment(Qt::AlignCenter);

	// Creating the widget for choosing the font and the color of the font
	fontWidget = new FontWidget(this);


	// Creating a QComboBox for the 'Group' part
	groupComboBox = new QComboBox();
	groupComboBox->addItem("Aucun groupe");

	backgroundColorPicker = new ColorPickerWidget(this);

	lineColorPicker = new ColorPickerWidget(this);

	hidingCheckBox = new QCheckBox("Cacher");
	hidingCheckBox->setCheckState(Qt::Unchecked);

	// Adding QFontComboBox

	lineWidget = new LineWidget(this);

	// Adding the delete button
	deleteButton = new QPushButton("Supprimer", this);

	// Adding the widgets to this layout
	addWidget(*labelType);
	addWidget(*initialState);
	addWidget(*finalState);

	addWidget(*labelPosition);
	addWidget(*XWidget);
	addWidget(*YWidget);
	addWidget(*ZWidget);
	addWidget(*HWidget);
	addWidget(*LWidget);


	addWidget(*labelTexte);
	addWidget(*editText);
	addWidget(*fontWidget);

	addWidget(*labelGroupes);
	addWidget(*groupComboBox);

	addWidget(*labelBackground);
	addWidget(*backgroundColorPicker);

	addWidget(*labelTrait);
	addWidget(*lineColorPicker);
	addWidget(*lineWidget);

	addWidget(*labelDelete);
	addWidget(*deleteButton);
	addWidget(*hidingCheckBox);
}

StateDockWidget::~StateDockWidget() {
	delete labelType;
	delete labelPosition;
	delete labelTexte;
	delete labelGroupes;
	delete labelBackground;
	delete labelTrait;
	delete labelDelete;

	delete initialState;
	delete finalState;
	delete layoutState;
	delete WidgetState;

	delete XWidget;
	delete YWidget;
	delete ZWidget;
	delete HWidget;
	delete LWidget;

	delete editText;
	delete fontWidget;
	delete groupComboBox;
	delete lineColorPicker;
	delete backgroundColorPicker;
	delete lineWidget;

	delete deleteButton;
}

QCheckBox *StateDockWidget::getInitialState() {
	return initialState;
}

QCheckBox *StateDockWidget::getFinalState() {
	return finalState;
}

QSpinBox *StateDockWidget::getXSpinBox() {
	return XSpinBox;
}

QSpinBox *StateDockWidget::getYSpinBox() {
	return YSpinBox;
}

QSpinBox *StateDockWidget::getZSpinBox() {
	return ZSpinBox;
}

QSpinBox *StateDockWidget::getHSpinBox() {
	return HSpinBox;
}

QSpinBox *StateDockWidget::getLSpinBox() {
	return LSpinBox;
}

QLineEdit *StateDockWidget::getEditText() {
	return editText;
}

FontWidget *StateDockWidget::getFontWidget() {
	return fontWidget;
}

QComboBox *StateDockWidget::getGroupComboBox() {
	return groupComboBox;
}

ColorPickerWidget *StateDockWidget::getLineColorPicker() {
	return lineColorPicker;
}

ColorPickerWidget *StateDockWidget::getBackgroundColorPicker() {
	return backgroundColorPicker;
}

LineWidget *StateDockWidget::getLineWidget() {
	return lineWidget;
}

QPushButton *StateDockWidget::getDeleteButton() {
	return deleteButton;
}

QCheckBox *StateDockWidget::getHidingCheckBox() {
	return hidingCheckBox;
}
