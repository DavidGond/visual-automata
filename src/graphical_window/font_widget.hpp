#ifndef VISUAL_AUTOMATA_FONT_WIDGET_HPP
#define VISUAL_AUTOMATA_FONT_WIDGET_HPP

#include <QWidget>
#include <QPushButton>
#include <QToolButton>
#include <QHBoxLayout>

#include "color_picker_widget.hpp"

/** 
 * FontWidget provides a widget allowing the user to choose the color and the font of the text
 */
class FontWidget: public QWidget {
private:
	/**< The button which must open the dialog for choosing the font */
	QPushButton *fontButton;
	/**< The ColorPickerWidget for choosing the color of the text */
	ColorPickerWidget *fontColorPicker;
	/**< The layout to place the widgets */
	QHBoxLayout *fontLayout;

public:
	/**
	 * Constructor
	 */
	explicit FontWidget(QWidget *parent);

	/**
	 * Destructor
	 */
	~FontWidget() override;

	/**
	 * Method. 
	 * @returns The ColorPickerWidget for choosing the color of the text
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getFontColorPicker();

	/**
	 * Method.
	 * @returns The QPushButton which must open the font dialog
	 */
	QPushButton *getFontButton();
};

#endif // VISUAL_AUTOMATA_FONT_WIDGET_HPP
