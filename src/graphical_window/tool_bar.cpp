﻿#include "tool_bar.hpp"

ToolBar::ToolBar(QMainWindow *mainWindowP) : QToolBar(), mainWindow(mainWindowP) {
	// Creating of the ToolBox and adding it to the mainWindow
	mainWindow->addToolBar(this);
	this->setAllowedAreas(Qt::BottomToolBarArea);
	this->setMovable(false);

	// Adding widgets to this QToolBox
	separator1 = this->addSeparator();

	// Adding Copy, Cut and Paste actions
	actionCopier = this->addAction("&Copier");
	actionCopier->setIcon(QIcon(":/pictures/copy.png"));
	actionCopier->setEnabled(false);
	actionCouper = this->addAction("&Couper");
	actionCouper->setIcon(QIcon(":/pictures/cut.png"));
	actionCouper->setEnabled(false);
	actionColler = this->addAction("&Coller");
	actionColler->setIcon(QIcon(":/pictures/paste.png"));
	actionColler->setEnabled(false);

	separator2 = this->addSeparator();

	// Adding Zoom+, Zoom- and realSize actions
	actionZoomPlus = this->addAction("&Zoom +");
	actionZoomPlus->setIcon(QIcon(":/pictures/zoom+.png"));
	actionTailleReelle = this->addAction("&Taille réelle");
	actionTailleReelle->setIcon(QIcon(":/pictures/screen.png"));
	actionZoomMoins = this->addAction("&Zoom -");
	actionZoomMoins->setIcon(QIcon(":/pictures/zoom-.png"));
	separator3 = this->addSeparator();

	// Adding Group textLabel
	actionGroupeTexte = this->addAction("Groupe d'états");

	// Adding Display Group QComboBox
	affichageGroupes = new QComboBox(mainWindow);
	affichageGroupes->addItem("Aucun groupe");
	this->addWidget(affichageGroupes);

	separator4 = this->addSeparator();

	// Adding AddGroup textLabel + AddGroup Action
	actionAddGroupeTexte = this->addAction("Ajouter un groupe   : ");
	actionAddGroupe = this->addAction("&Ajouter un groupe");
	actionAddGroupe->setIcon(QIcon(":/pictures/create_group.png"));

	separator5 = this->addSeparator();

	// Adding Display Weight QCheckBox
	afficherPoids = new QCheckBox("Afficher poids", mainWindow);
	afficherPoids->setEnabled(false);
	this->addWidget(afficherPoids);

	// Adding Display Grid QCheckBox
	afficherGrille = new QCheckBox("Afficher grille", mainWindow);
	afficherGrille->setChecked(true);
	this->addWidget(afficherGrille);

	separator6 = this->addSeparator();
	// Adding Rectangle selection

	actionRectangleSelection = this->addAction("&Rectangle de sélection");
	actionRectangleSelection->setIcon(QIcon(":/pictures/rectangle_selection.png"));

}

ToolBar::~ToolBar() {
	delete actionCopier;
	delete actionCouper;
	delete actionColler;
	delete actionZoomPlus;
	delete actionTailleReelle;
	delete actionZoomMoins;
	delete affichageGroupes;
	delete actionAddGroupeTexte;
	delete actionAddGroupe;
	delete afficherPoids;
	delete afficherGrille;
	delete actionRectangleSelection;

	delete separator1;
	delete separator2;
	delete separator3;
	delete separator4;
	delete separator5;
	delete separator6;
}

QAction *ToolBar::getActionZoomPlus() const {
	return actionZoomPlus;
}

QAction *ToolBar::getActionTailleReelle() const {
	return actionTailleReelle;
}

QAction *ToolBar::getActionZoomMoins() const {
	return actionZoomMoins;
}

QCheckBox *ToolBar::getAfficherGrille() const {
	return afficherGrille;
}

bool ToolBar::gridChecked() const {
	return afficherGrille->isChecked();
}

void ToolBar::checkGrid() {
	afficherGrille->setChecked(true);
}

void ToolBar::uncheckGrid() {
	afficherGrille->setChecked(false);
}

QComboBox *ToolBar::getAffichageGroupeBox() const {
	return affichageGroupes;
}

QAction *ToolBar::getActionAddGroupe() const {
	return actionAddGroupe;
}

int ToolBar::getCurrentSelectedGroup() const {
	return affichageGroupes->currentIndex();
}

QAction *ToolBar::getActionAddGroupeTexte() const {
	return actionAddGroupeTexte;
}
