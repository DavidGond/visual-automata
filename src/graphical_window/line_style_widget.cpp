#include "line_style_widget.hpp"

LineStyleWidget::LineStyleWidget(QWidget *parent) :
		parent(parent), styles(QVector<Qt::PenStyle>()) {
	QIcon dashLineIcon0 = QIcon(":/pictures/dash-line-0.png");
	QIcon dashLineIcon1 = QIcon(":/pictures/dash-line-3.png");
	QIcon dashLineIcon2 = QIcon(":/pictures/dash-line-1.png");
	QIcon dashLineIcon3 = QIcon(":/pictures/dash-line-5.png");

	this->setIconSize(QSize(100, 20));
	this->addItem(dashLineIcon0, "");
	this->addItem(dashLineIcon1, "");
	this->addItem(dashLineIcon2, "");
	this->addItem(dashLineIcon3, "");

	// MUST BE IN THE SAME ORDER AS THE ICON IN THE QCOMBOBOX
	styles.append(Qt::SolidLine);
	styles.append(Qt::DashLine);
	styles.append(Qt::DotLine);
	styles.append(Qt::DashDotLine);
}

Qt::PenStyle &LineStyleWidget::getFocusedStyle() {
	return styles[this->currentIndex()];
}

int LineStyleWidget::getPickerStyle() {
	return styles[this->currentIndex()];
}

void LineStyleWidget::setFocusedStyle(Qt::PenStyle &newStyle) {
	this->setCurrentIndex(styles.indexOf(newStyle));
}
