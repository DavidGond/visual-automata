#include "pair_label_widget.hpp"

PairLabelWidget::PairLabelWidget(QWidget *widget1, QWidget *widget2, QWidget *parent) : QWidget() {
	label = widget1;
	widget = widget2;
	// Creating a horizontal Layout for the pair : Label-Widget
	horizontalLayout = new QHBoxLayout();
	// Adding the pair Label-Widget
	horizontalLayout->addWidget(label, Qt::AlignCenter);
	horizontalLayout->addWidget(widget, Qt::AlignCenter);

	// Creating a new Widget for displaying the pair
	pairWidget = new QWidget(parent);
	this->setLayout(horizontalLayout);
}

PairLabelWidget::~PairLabelWidget() {
	if (label != nullptr) {
		delete label;
		label = nullptr;
	}
	if (widget != nullptr) {
		delete widget;
		widget = nullptr;
	}
	if (horizontalLayout != nullptr) {
		delete horizontalLayout;
		horizontalLayout = nullptr;
	}
	if (pairWidget != nullptr) {
		delete pairWidget;
		pairWidget = nullptr;
	}
}
