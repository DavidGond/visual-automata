#ifndef VISUAL_AUTOMATA_RIGHT_DOCK_WIDGET_HPP
#define VISUAL_AUTOMATA_RIGHT_DOCK_WIDGET_HPP

#include <QDockWidget>
#include <QLabel>
#include <QScrollArea>
#include <QWidget>
#include <QVBoxLayout>

/**
 * RightDockWidget provides an empty DockWidget with a label, a scroll area, a vertical layout
 * It can be inherited to create new DockWidget
 */
class RightDockWidget: public QDockWidget {
private:
	/**< The text for the title of the RightDockWidget */
	QLabel *labelAjouter;
	/**< Allow the user to scroll to see all widgets of a RightDockWidget */
	QScrollArea *scrollArea;
	/**< An empty widget to place in the vertical layout */
	QWidget *widgetDroit;
	/**< The vertical layout containing all widgets of the RightDockWidget */
	QVBoxLayout *verticalLayout;

public:
	/**
	 * Constructor
	 *
	 * Initializes and places all the components in the dockWidget
	 * @param mainWindow is the mainWindow of the application
	 */
	explicit RightDockWidget(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~RightDockWidget() override;

	/**
	 * Method.
	 *
	 * Adds a widget to the big widget of the dock
	 * @param widget is the widget to add to the dock
	 */
	virtual void addWidget(QWidget &widget);
	/**
	 * Method.
	 *
	 * Adds a widget to the big widget of the dock with an alignment flag
	 * @param widget is the widget to add to the dock
	 * @param alignmentFlag is the type of alignment for the widget to be added
	 */
	virtual void addWidget(QWidget &widget, Qt::AlignmentFlag alignmentFlag);
};

#endif
