#include "color_picker_widget.hpp"
#include <QToolButton>
#include <QColorDialog>
#include <QDebug>

ColorPickerWidget::ColorPickerWidget(QWidget *parent, const QColor &defaultColor) :
		QWidget(parent) {
	QIcon colorPaletteIcon = QIcon(":/pictures/palette.png");
	// Creating a horizontal layout for the editTextBackground and colorPalette
	backgroundLayout = new QHBoxLayout();
	backgroundLayout->setSpacing(0);
	// Adding a QLineEditText and putting a "ColorPalette" icon next to it
	editBackground = new QLineEdit(defaultColor.name(QColor::HexRgb));
	editBackground->setEnabled(true);
	colorBackgroundButton = new QToolButton(this);
	colorBackgroundButton->setIcon(colorPaletteIcon);
	rectColor = new QPushButton(this);
	rectColor->setFlat(true);
	rectColor->setStyleSheet("background-color: " + defaultColor.name(QColor::HexRgb) + ";"
	                                                                                    "border: 1px solid blacnonek;"  //outline
	                                                                                    "border-radius: 500px;"     //corners
	                                                                                    "font-size: 15px;");

	// Adding the 3 widgets to the previous layout
	backgroundLayout->addWidget(rectColor);
	backgroundLayout->addWidget(editBackground);
	backgroundLayout->addWidget(colorBackgroundButton);
	setLayout(backgroundLayout);
	currentColor = new QColor(); // added to prevent from uninitialized variable
}

ColorPickerWidget::~ColorPickerWidget() {
	delete editBackground;
	delete colorBackgroundButton;
	delete rectColor;
	delete currentColor;
	delete backgroundLayout;
}

QLineEdit *ColorPickerWidget::getEditBackground() const {
	return editBackground;
}

QToolButton *ColorPickerWidget::getColorBackgroundButton() const {

	return colorBackgroundButton;
}

void ColorPickerWidget::openColorDialog() {
	QColorDialog colorBox;
	QColor color = QColorDialog::getColor(*currentColor);
	if (color.isValid()) {
		setCurrentColor(color);
		emit colorChanged(color);
	}
}

void ColorPickerWidget::editColor() {
	QColor color = QColor(editBackground->text());
	if (color.isValid()) {
		setCurrentColor(color);
	} else {
		editBackground->setText(currentColor->name(QColor::HexRgb));
		editBackground->selectAll();
	}
}

QColor ColorPickerWidget::getCurrentColor() const {
	return *currentColor;
}

void ColorPickerWidget::setCurrentColor(QColor newColor) {
	*currentColor = std::move(newColor);
	editBackground->setText(currentColor->name(QColor::HexRgb));
	rectColor->setStyleSheet("background-color: " + currentColor->name(QColor::HexRgb) + ";"
	                                                                                     "border: 1px solid blacnonek;"  //outline
	                                                                                     "border-radius: 500px;"     //corners
	                                                                                     "font-size: 15px;");
}
