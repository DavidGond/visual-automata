#ifndef VISUAL_AUTOMATA_TRANSITION_DOCK_WIDGET_HPP
#define VISUAL_AUTOMATA_TRANSITION_DOCK_WIDGET_HPP

#include <QWidget>
#include <QDockWidget>
#include <QMainWindow>
#include <QtGui>
#include <QCheckBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QFontComboBox>
#include <QMessageBox>
#include <QFontDialog>
#include <QColorDialog>
#include <QGroupBox>
#include <QRadioButton>
#include <QScrollArea>

#include "line_widget.hpp"
#include "color_picker_widget.hpp"
#include "font_widget.hpp"
#include "custom_slider.hpp"
#include "right_dock_widget.hpp"

class RightDockWidget;

/**
 * The TransitionDockWidget class inherits from RightDockWidget to display all the transition properties.
 */
class TransitionDockWidget: public RightDockWidget {
Q_OBJECT
private:
	/**< Label for the transition type */
	QLabel *labelType;
	/**< Label for the transition trait */
	QLabel *labelTrait;
	/**< Label for the transition precision */
	QLabel *labelPrecision;
	/**< Label for the transition text */
	QLabel *labelTexte;
	/**< Label for the transition weight */
	QLabel *labelPoids;
	/**< Label for the transition delete button */
	QLabel *labelDelete;

	/**< CheckBox for left arrow type (not used) */
	QCheckBox *leftArrow;
	/**< CheckBox for right arrow type (not used) */
	QCheckBox *rightArrow;
	/**< Vertical layout to display the 2 previous checkboxes vertically */
	QVBoxLayout *layoutArrow;
	/**< Widget which uses the layoutArrow */
	QWidget *arrowWidget;

	/**< Color of the transition  */
	ColorPickerWidget *lineColorPicker;
	/**< Style and size of the transition */
	LineWidget *lineWidget;
	/**< Vertical layout to display the 2 previous widgets vertically */
	QVBoxLayout *traitLayout;
	/**< Widget which uses the traitLayout */
	QWidget *traitWidget;

	/**< Precision of the transition */
	CustomSlider *transitionAccuracySlider;
	/**< Label of the transition */
	QLineEdit *transitionText;
	/**< Font of the transition */
	FontWidget *textFontWidget;

	/**< Weight of the transition */
	QLineEdit *weightText;
	/**< Button for deleting the transition */
	QPushButton *deleteButton;
	/**< CheckBox to hide or not the transition */
	QCheckBox *hidingCheckBox;

public:
	/**
	 * Constructor.
	 *
	 * Initializes and places all the components in the dockWidget
	 * @param mainWindow is the mainWindow of the application
	 */
	explicit TransitionDockWidget(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~TransitionDockWidget() override;

	/**
	 * Method.
	 *
	 * @return the left arrow check box (not used)
	 */
	QCheckBox *getLeftArrow() const;

	/**
	 * Method.
	 *
	 * @return the right arrow check box (not used)
	 */
	QCheckBox *getRightArrow() const;

	/**
	 * Method.
	 *
	 * Returns the ColorPickerWidget of the transition
	 * @return the widget to choose the transition's color
	 */
	ColorPickerWidget *getLineColorPicker() const;

	/**
	 * Method.
	 *
	 * Returns the LineWidget of the transition (style + size)
	 * @return the widget to choose the transition's style and size
	 */
	LineWidget *getLineWidget() const;

	/**
	 * Method.
	 *
	 * Returns the LineEditWidget of the transition (text)
	 * @return the widget to choose the transition's text
	 */
	QLineEdit *getTransitionText() const;

	/**
	 * Method.
	 *
	 * Returns the FontWidget of the transition (font)
	 * @return the widget to choose the transition's font
	 */
	FontWidget *getTextFontWidget() const;

	/**
	 * Method.
	 *
	 * Returns the weight LineEdit of the transition
	 * @return the widget to choose the transition's weight
	 */
	QLineEdit *getWeightText() const;

	/**
	 * Method.
	 *
	 * Returns the transitionAccuracySlider of the transition
	 * @return the widget to choose the transition's precision
	 */
	CustomSlider *getTransitionAccuracySlider() const;

	/**
	 * Method.
	 *
	 * Returns the delete transition button
	 * @return the button which enables to delete the transition
	 */
	QPushButton *getDeleteButton() const;

	/**
	 * Method.
	 *
	 * Returns the hide transition check box
	 * @return the check box which enables to hide or not the transition
	 */
	QCheckBox *getHidingCheckBox() const;
};

#endif // VISUAL_AUTOMATA_TRANSITION_DOCK_WIDGET_HPP
