#ifndef VISUAL_AUTOMATA_STATE_DOCK_WIDGET_HPP
#define VISUAL_AUTOMATA_STATE_DOCK_WIDGET_HPP

#include <QWidget>
#include <QDockWidget>
#include <QMainWindow>
#include <QtGui>
#include <QCheckBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QFontComboBox>
#include <QMessageBox>
#include <QFontDialog>
#include <QColorDialog>
#include <QScrollArea>

#include <iostream>
#include <memory>

#include "font_widget.hpp"
#include "color_picker_widget.hpp"
#include "line_widget.hpp"
#include "state_dock_widget.hpp"
#include "right_dock_widget.hpp"
#include "pair_label_widget.hpp"

class RightDockWidget;

/**
 * The StateDockWidget class inherits from RightDockWidget to display all the state properties.
 */
class StateDockWidget: public RightDockWidget {
Q_OBJECT
private:
	/**< The main window of the application */
	QMainWindow *mainWindow;

	/**< Label for the state type */
	QLabel *labelType;
	/**< Label for the state position */
	QLabel *labelPosition;
	/**< Label for the state text */
	QLabel *labelTexte;
	/**< Label for the group text */
	QLabel *labelGroupes;
	/**< Label for the state background color */
	QLabel *labelBackground;
	/**< Label for the state pen color */
	QLabel *labelTrait;
	/**< Label for the delete state button */
	QLabel *labelDelete;

	/**< Checkbox to change the state to initial */
	QCheckBox *initialState;
	/**< Display the 2 checkboxes for initial and final state vertically */
	QVBoxLayout *layoutState;
	/**< Widget to use the vertical layout */
	QWidget *WidgetState;
	/**< Checkbox to change the state to final */
	QCheckBox *finalState;

	/**< Label for the X state coordinate (not used) */
	QLabel *labelX;
	/**< Choose the X coordinate */
	QSpinBox *XSpinBox;
	/**< Display the pair label-spinbox */
	PairLabelWidget *XWidget;

	/**< Label for the Y state coordinate (not used) */
	QLabel *labelY;
	/**< Choose the Y coordinate */
	QSpinBox *YSpinBox;
	/**< Display the pair label-spinbox */
	PairLabelWidget *YWidget;

	/**< Label for the Z state coordinate (not used) */
	QLabel *labelZ;
	/**< Choose the Z coordinate  */
	QSpinBox *ZSpinBox;
	/**< Display the pair label-spinbox */
	PairLabelWidget *ZWidget;

	/**< Label for the H state position (not used) */
	QLabel *labelH;
	/**< Choose the H position */
	QSpinBox *HSpinBox;
	/**< Display the pair label-spinbox */
	PairLabelWidget *HWidget;

	/**< Label for the L state position (not used) */
	QLabel *labelL;
	/**< Choose the L position */
	QSpinBox *LSpinBox;
	/**< Display the pair label-spinbox */
	PairLabelWidget *LWidget;

	/**< Text field to change the state label */
	QLineEdit *editText;

	/**< Widget to change the state label font */
	FontWidget *fontWidget;
	/**< Selection box to display the groups to which the state belongs */
	QComboBox *groupComboBox;
	/**< Widget to choose the state pen color */
	ColorPickerWidget *lineColorPicker;
	/**< Widget to choose the state brush color */
	ColorPickerWidget *backgroundColorPicker;
	/**< Widget to choose the state trait style and size */
	LineWidget *lineWidget;

	/**< Button to delete the current state */
	QPushButton *deleteButton;
	/**< Button to hide the current state */
	QCheckBox *hidingCheckBox;

public:
	/**
	 * Constructor.
	 * 
	 * Initializes and places all the components in the dockWidget
	 * @param mainWindow is the MainWindow of the application
	 */
	explicit StateDockWidget(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~StateDockWidget() override;

	/**
	 * Method.
	 * 
	 * @return The check box for the initial state
	 */
	QCheckBox *getInitialState();

	/**
	 * Method.
	 * 
	 * @return The check box for the final state
	 */
	QCheckBox *getFinalState();

	/**
	 * Method.
	 * 
	 * @return The spin box for the x coordinate
	 */
	QSpinBox *getXSpinBox();

	/**
	 * Method.
	 * 
	 * @return The spin box for the y coordinate
	 */
	QSpinBox *getYSpinBox();

	/**
	 * Method.
	 * 
	 * @return The spin box for the z coordinate
	 */
	QSpinBox *getZSpinBox();

	/**
	 * Method.
	 * 
	 * @return The spin box for the h position
	 */
	QSpinBox *getHSpinBox();

	/**
	 * Method.
	 * 
	 * @return The spin box for the l position
	 */
	QSpinBox *getLSpinBox();

	/**
	 * Method.
	 * 
	 * Returns text field to change the state name
	 * @return editName
	 */
	QLineEdit *getEditText();

	/**
	 * Method.
	 * 
	 * Returns the widget to change the state name font
	 * @return fontWidget
	 * @see FontWidget
	 */
	FontWidget *getFontWidget();

	/**
	 * Method.
	 * 
	 * Returns the box containing the group to which the state belongs
	 * @return groupComboBox
	 * @see Groupable
	 */
	QComboBox *getGroupComboBox();

	/**
	 * Method.
	 * 
	 * Returns the ColorPicker widget of the state trait
	 * @return the widget to choose the state's trait color
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getLineColorPicker();

	/**
	 * Method.
	 * 
	 * Returns the ColorPicker widget of the state background
	 * @return the widget to choose the state's background color
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getBackgroundColorPicker();

	/**
	 * Method.
	 * 
	 * Returns the LineWidget of the state (style + size)
	 * @return the widget to choose the state's style and size
	 * @see LineWidget
	 */
	LineWidget *getLineWidget();

	/**
	 * Method.
	 * 
	 * Returns the delete state button
	 * @return the button which enables to delete the state
	 */
	QPushButton *getDeleteButton();

	/**
	 * Method.
	 * 
	 * Returns the hide transition check box
	 * @return the check box which enables to hide or not the state
	 */
	QCheckBox *getHidingCheckBox();
};

#endif // VISUAL_AUTOMATA_STATE_DOCK_WIDGET_HPP
