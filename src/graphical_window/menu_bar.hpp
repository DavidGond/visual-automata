#ifndef VISUAL_AUTOMATA_MENU_BAR_HPP
#define VISUAL_AUTOMATA_MENU_BAR_HPP

#include <QApplication>
#include <QWidget>
#include <QGraphicsView>
#include <QGraphicsScene>

#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QFileDialog>

/**
 * This class is the menu bar of the main window of the application. It contains several menus/actions explained below
 */
class MenuBar: public QObject {
Q_OBJECT
private:
	/**< The main window of the application */
	QMainWindow *mainWindow;
	// 5 QMenus
	/**< File menu */
	QMenu *menuFichier
	/**< Editing menu */;
	QMenu *menuEditer;
	/**< Display menu */
	QMenu *menuAffichage
	/**< Tools menu */;
	QMenu *menuOutils;
	/**< Layout sub menu */
	QMenu *menuLayout;
	// 7 Actions for menuFichier
	/**< New tab action */
	QAction *actionNouveau;
	/**< Open file action */
	QAction *actionOuvrir;
	/**< Open recent file action (not implemented) */
	QAction *actionOuvrirRecent;
	/**< Save action */
	QAction *actionEnregistrer;
	/**< Save as action */
	QAction *actionEnregistrerSous;
	/**< Export the file action (not implemented) */
	QAction *actionExporter;
	/**< Close tab action */
	QAction *actionFermerOnglet;
	/**< Close application action */
	QAction *actionFermer;
	// 5 Actions for menuEditer
	/**< Cancel action (not implemented) */
	QAction *actionAnnuler;
	/**< Redo action (not implemented) */
	QAction *actionRefaire;
	/**< Cut action (not implemented) */
	QAction *actionCouper;
	/**< Copy action (not implemented) */
	QAction *actionCopier;
	/**< Paste action (not implemented) */
	QAction *actionColler;
	// 4 Actions for menuAffichage
	/**< Action for opening the preferences window */
	QAction *actionPreferences;
	/**< Action for zooming in the view */
	QAction *actionZoomPlus;
	/**< Action for zooming out the view */
	QAction *actionZoomMoins;
	/**< Action for displaying or not the grid */
	QAction *actionGrille;
	// 4 Actions for menuOutils
	/**< Action fo regenerating Json (not implemented) */
	QAction *actionRegenererJson;
	/**< Action for applying default style (not implemented) */
	QAction *actionAppliquerStyleDefaut;
	/**< Help action (not implemented) */
	QAction *actionAide;
	/**< Action for redirecting to AwaliJson document (not implemented) */
	QAction *actionAwaliJson;
	// 3 Actions for (sub)menuLayout
	/**< Action for choosing the linear layout */
	QAction *actionLayoutLineaire;
	/**< Action for choosing the circular layout */
	QAction *actionLayoutCirculaire;
	/**< Action for choosing the layout with coordinates (not implemented) */
	QAction *actionLayoutCoordonnees;

public:
	/**
	 * Constructor.
	 *
	 * Initializes and places all the menus in the MenuBar
	 * @param mainWindow is the mainWindow of the application
	 */
	explicit MenuBar(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~MenuBar() override;

	/**
	 * Method.
	 * Returns the action for creating a new tab. This does not create a new file.
	 * @return the new tab action
	 */
	QAction *getActionNouveau() const;

	/**
	 * Method.
	 *
	 * Returns the action for opening a file
	 * @return the open file action
	 */
	QAction *getActionOuvrir() const;

	/**
	 * Method.
	 *
	 * Returns the action for opening a recent file (not implemented)
	 * @return the open recent file action
	 */
	QAction *getActionOuvrirRecent() const;

	/**
	 * Method.
	 *
	 * Returns the action for saving file
	 * @return the save file action
	 */
	QAction *getActionEnregistrer() const;

	/**
	 * Method.
	 *
	 * Returns the action for saving file as
	 * @return the save file as action
	 */
	QAction *getActionEnregistrerSous() const;

	/**
	 * Method.
	 *
	 * Returns the action for exporting the file (not implemented)
	 * @return the export file action
	 */
	QAction *getActionExporter() const;

	/**
	 * Method.
	 *
	 * Returns the action for closing tab
	 * @return the close tab action
	 */
	QAction *getActionFermerOnglet() const;

	/**
	 * Method.
	 *
	 * Returns the action for closing the app
	 * @return the close app action
	 */
	QAction *getActionFermer() const;

	/**
	 * Method.
	 *
	 * Returns the action to cancel an action (not implemented)
	 * @return the cancel action
	 */
	QAction *getActionAnnuler() const;

	/**
	 * Method.
	 *
	 * Returns the action for redoing an action (not implemented)
	 * @return the redo action
	 */
	QAction *getActionRefaire() const;

	/**
	 * Method.
	 *
	 * Returns the action for cutting an element (not implemented)
	 * @return the cut action
	 */
	QAction *getActionCouper() const;

	/**
	 * Method.
	 *
	 * Returns the action for copying an element (not implemented)
	 * @return the copy action
	 */
	QAction *getActionCopier() const;

	/**
	 * Method.
	 *
	 * Returns the action for pasting an element (not implemented)
	 * @return the paste action
	 */
	QAction *getActionColler() const;

	/**
	 * Method.
	 *
	 * Returns the action for opening the preferences window
	 * @return the opening preferences window action
	 */
	QAction *getActionPreferences() const;

	/**
	 * Method.
	 *
	 * Returns the action for zooming in the view
	 * @return the zoom in action
	 */
	QAction *getActionZoomPlus() const;

	/**
	 * Method.
	 *
	 * Returns the action for zooming out the view
	 * @return the zoom out action
	 */
	QAction *getActionZoomMoins() const;

	/**
	 * Method.
	 *
	 * Returns the action for displaying or not the grid
	 * @return the grid action
	 */
	QAction *getActionGrille() const;

	/**
	 * Method.
	 *
	 * Returns if the grid button is check or not
	 * @return the state of the grid checkbox (checked or not)
	 */
	bool gridChecked() const;

	/**
	 * Method.
	 *
	 * Check the grid button to true
	 */
	void checkGrid();

	/**
	 * Method.
	 *
	 * Uncheck the grid button
	 */
	void uncheckGrid();

	/**
	 * Method.
	 *
	 * Returns the action for regenerating Json (not implemented)
	 * @return the regenerate Json action
	 */
	QAction *getActionRegenererJson() const;

	/**
	 * Method.
	 *
	 * Returns the action for applying default style (not implemented)
	 * @return the apply defaut style action
	 */
	QAction *getActionAppliquerStyleDefaut() const;

	/**
	 * Method.
	 *
	 * Returns the action for help (not implemented)
	 * @return the help action
	 */
	QAction *getActionAide() const;

	/**
	 * Method.
	 *
	 * Returns the action for redirecting to AwaliJson document (not implemented)
	 * @return the AwaliJson redirection action
	 */
	QAction *getActionAwaliJson() const;

	/**
	 * Method.
	 *
	 * Returns the action for choosing the linear layout
	 * @return the linear layout action
	 */
	QAction *getActionLayoutLineaire() const;

	/**
	 * Method.
	 *
	 * Returns the action for choosing the circular layout
	 * @return the circular layout action
	 */
	QAction *getActionLayoutCirculaire() const;

	/**
	 * Method.
	 *
	 * Returns the action for choosing the layout with coordinates (not implemented)
	 * @return the layout with coordinates action
	 */
	QAction *getActionLayoutCoordonnees() const;
};

#endif // VISUAL_AUTOMATA_MENU_BAR_HPP
