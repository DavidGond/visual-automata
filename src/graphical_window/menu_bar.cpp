#include "tool_bar.hpp"
#include "left_dock_widget.hpp"
#include "transition_dock_widget.hpp"

#include "menu_bar.hpp"

MenuBar::MenuBar(QMainWindow *mainWindow) : mainWindow(mainWindow) {
	// Creation of the 4 QMenu + 1 sub QMenu (Layout) for menuOutils
	menuFichier = mainWindow->menuBar()->addMenu("&Fichier");
	menuEditer = mainWindow->menuBar()->addMenu("&Éditer");
	menuAffichage = mainWindow->menuBar()->addMenu("&Affichage");
	menuOutils = mainWindow->menuBar()->addMenu("&Outils");

	menuLayout = menuOutils->addMenu("&Layout");

	// Adding actions to menuFichier
	actionNouveau = new QAction("&Nouveau...", mainWindow);
	actionOuvrir = new QAction("&Ouvrir...", mainWindow);
	actionOuvrirRecent = new QAction("&Ouvrir récent...", mainWindow);
	actionOuvrirRecent->setEnabled(false);
	actionEnregistrer = new QAction("&Enregistrer...", mainWindow);
	actionEnregistrerSous = new QAction("&Enregistrer sous...", mainWindow);
	actionExporter = new QAction("&Exporter...", mainWindow);
	actionExporter->setEnabled(false);
	actionFermerOnglet = new QAction("&Fermer onglet", mainWindow);
	actionFermer = new QAction("&Fermer", mainWindow);
	menuFichier->addAction(actionNouveau);
	menuFichier->addAction(actionOuvrir);
	menuFichier->addAction(actionOuvrirRecent);
	menuFichier->addAction(actionEnregistrer);
	menuFichier->addAction(actionEnregistrerSous);
	menuFichier->addAction(actionExporter);
	menuFichier->addAction(actionFermerOnglet);
	menuFichier->addAction(actionFermer);

	// Adding actions to menuEditer
	actionAnnuler = new QAction("&Annuler", mainWindow);
	actionAnnuler->setEnabled(false);
	actionRefaire = new QAction("&Refaire", mainWindow);
	actionRefaire->setEnabled(false);
	actionCouper = new QAction("&Couper", mainWindow);
	actionCouper->setEnabled(false);
	actionCopier = new QAction("&Copier", mainWindow);
	actionCopier->setEnabled(false);
	actionColler = new QAction("&Coller", mainWindow);
	actionColler->setEnabled(false);

	menuEditer->addAction(actionAnnuler);
	menuEditer->addAction(actionRefaire);
	menuEditer->addAction(actionCouper);
	menuEditer->addAction(actionCopier);
	menuEditer->addAction(actionColler);

	// Adding actions to menuAffichage
	actionPreferences = new QAction("&Préférences...", mainWindow);
	actionZoomPlus = new QAction("&Zoom +", mainWindow);
	actionZoomMoins = new QAction("&Zoom -", mainWindow);
	actionGrille = new QAction("&Grille...", mainWindow);
	actionGrille->setCheckable(true);
	actionGrille->setChecked(true);

	menuAffichage->addAction(actionPreferences);
	menuAffichage->addAction(actionZoomPlus);
	menuAffichage->addAction(actionZoomMoins);
	menuAffichage->addAction(actionGrille);

	// Adding actions to menuOutils
	actionRegenererJson = new QAction("&Regénérer JSON", mainWindow);
	actionRegenererJson->setEnabled(false);
	actionAppliquerStyleDefaut = new QAction("&Appliquer le style par défaut...", mainWindow);
	actionAppliquerStyleDefaut->setEnabled(false);
	actionAide = new QAction("&Aide...", mainWindow);
	actionAide->setEnabled(false);
	actionAwaliJson = new QAction("&Awali JSON", mainWindow);
	actionAwaliJson->setEnabled(false);


	menuOutils->addAction(actionRegenererJson);
	menuOutils->addAction(actionAppliquerStyleDefaut);
	menuOutils->addAction(actionAide);
	menuOutils->addAction(actionAwaliJson);

	// Adding actions to the sub QMenu Layout
	actionLayoutLineaire = new QAction("&Linéaire", mainWindow);
	actionLayoutCirculaire = new QAction("&Circulaire", mainWindow);
	actionLayoutCoordonnees = new QAction("&Depuis des coordonnées", mainWindow);
	actionLayoutCoordonnees->setEnabled(false);

	menuLayout->addAction(actionLayoutLineaire);
	menuLayout->addAction(actionLayoutCirculaire);
	menuLayout->addAction(actionLayoutCoordonnees);
}

MenuBar::~MenuBar() {
	delete actionNouveau;
	delete actionOuvrir;
	delete actionOuvrirRecent;
	delete actionEnregistrer;
	delete actionEnregistrerSous;
	delete actionExporter;
	delete actionFermer;
	delete actionAnnuler;
	delete actionRefaire;
	delete actionCouper;
	delete actionCopier;
	delete actionColler;
	delete actionPreferences;
	delete actionZoomPlus;
	delete actionZoomMoins;
	delete actionGrille;
	delete actionRegenererJson;
	delete actionAppliquerStyleDefaut;
	delete actionAide;
	delete actionAwaliJson;
	delete actionLayoutLineaire;
	delete actionLayoutCirculaire;
	delete actionLayoutCoordonnees;
	delete menuLayout;
	delete menuFichier;
	delete menuEditer;
	delete menuAffichage;
	delete menuOutils;
}

QAction *MenuBar::getActionNouveau() const {
	return actionNouveau;
}

QAction *MenuBar::getActionOuvrir() const {
	return actionOuvrir;
}

QAction *MenuBar::getActionOuvrirRecent() const {
	return actionOuvrirRecent;
}

QAction *MenuBar::getActionEnregistrer() const {
	return actionEnregistrer;
}

QAction *MenuBar::getActionEnregistrerSous() const {
	return actionEnregistrerSous;
}

QAction *MenuBar::getActionExporter() const {
	return actionExporter;
}

QAction *MenuBar::getActionFermerOnglet() const {
	return actionFermerOnglet;
}

QAction *MenuBar::getActionFermer() const {
	return actionFermer;
}

QAction *MenuBar::getActionAnnuler() const {
	return actionAnnuler;
}

QAction *MenuBar::getActionRefaire() const {
	return actionRefaire;
}

QAction *MenuBar::getActionCouper() const {
	return actionCouper;
}

QAction *MenuBar::getActionCopier() const {
	return actionCopier;
}

QAction *MenuBar::getActionColler() const {
	return actionColler;
}

QAction *MenuBar::getActionPreferences() const {
	return actionPreferences;
}

QAction *MenuBar::getActionZoomPlus() const {
	return actionZoomPlus;
}

QAction *MenuBar::getActionZoomMoins() const {
	return actionZoomMoins;
}

QAction *MenuBar::getActionGrille() const {
	return actionGrille;
}

bool MenuBar::gridChecked() const {
	return actionGrille->isChecked();
}

void MenuBar::checkGrid() {
	actionGrille->setChecked(true);
}

void MenuBar::uncheckGrid() {
	actionGrille->setChecked(false);
}

QAction *MenuBar::getActionRegenererJson() const {
	return actionRegenererJson;
}

QAction *MenuBar::getActionAppliquerStyleDefaut() const {
	return actionAppliquerStyleDefaut;
}

QAction *MenuBar::getActionAide() const {
	return actionAide;
}

QAction *MenuBar::getActionAwaliJson() const {
	return actionAwaliJson;
}

QAction *MenuBar::getActionLayoutLineaire() const {
	return actionLayoutLineaire;
}

QAction *MenuBar::getActionLayoutCirculaire() const {
	return actionLayoutCirculaire;
}

QAction *MenuBar::getActionLayoutCoordonnees() const {
	return actionLayoutCoordonnees;
}
