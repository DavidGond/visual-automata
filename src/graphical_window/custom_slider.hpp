#ifndef VISUAL_AUTOMATA_CUSTOM_SLIDER_HPP
#define VISUAL_AUTOMATA_CUSTOM_SLIDER_HPP

#include <QWidget>
#include <QSlider>
#include <QLabel>
#include <QGridLayout>
#include <iostream>

/**
 * CustomSlider provides a slider which allows the user to see the starting/final/current value of the slider
 */
class CustomSlider: public QWidget {
Q_OBJECT
private:
	/**< The slider from Qt */
	QSlider *slider;
	/**< The text showing the minimum value */
	QLabel *minValue;
	/**< The text showing the maximum value */
	QLabel *maxValue;
	/**< The text showing the current value */
	QLabel *currentValue;
	/**< The minimum value of the slider */
	int min;
	/**< The maximum value of the slider */
	int max;
	/**< The grid where the widget are placed */
	QGridLayout *grid;

public:
	/**
	 * Constructor.
	 * Placing all widgets in the grid 
	 */
	CustomSlider(int minvalue, int maxValue, int startValue, QWidget *parent);

	/**
	 * Destructor
	 */
	~CustomSlider() override;

	/**
	 * Method.
	 * Set the current value of the slider
	 */
	void setValue(int value);

	/**
	 * Method.
	 * Get the current value of the slider
	 */
	int getValue() const;

	/**
	 * Method.
	 * @return the QSlider of the CustomSlider
	 */
	QSlider *getSlider() const;

private slots:
	/**
	 * Method.
	 * This slots is connected to the the signal valueChanged() to the QSlider
	 * @param value the new value of the slider
	 */
	void onValueChanged(int value);
};

#endif // VISUAL_AUTOMATA_CUSTOM_SLIDER_HPP
