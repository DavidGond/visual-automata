#ifndef VISUAL_AUTOMATA_COLOR_PICKER_WIDGET_HPP
#define VISUAL_AUTOMATA_COLOR_PICKER_WIDGET_HPP

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QColor>
#include <QHBoxLayout>
#include <iostream>

/**
 * ColorPickerWidget provides a widget to allow user to choose a color
 */
class ColorPickerWidget: public QWidget {
Q_OBJECT
private:
	/**< Where the user can see or change the name of the current color */
	QLineEdit *editBackground;
	/**< When the user clicks on this button is must open the window to choose a color (if the connexion is made in the AutomataController) */
	QToolButton *colorBackgroundButton;
	/**< A rectangle of color showing the current color */
	QPushButton *rectColor;
	/**< The current color */
	QColor *currentColor;
	/**< A QHBoxLayout to display all the widget */
	QHBoxLayout *backgroundLayout;

public:
	/**
	 * Constructor.
	 * Places all components of the widget in the layout
	 * @param parent The parent of the widget
	 * @param defaultColor the color when the widget is initialized
	 */
	explicit ColorPickerWidget(QWidget *parent, const QColor &defaultColor = Qt::black);

	/**
	 * Destructor
	 */
	~ColorPickerWidget() override;

	/**
	 * Method.
	 * @return the QLineEdit of the ColorPickerWidget.
	 */
	QLineEdit *getEditBackground() const;

	/**
	 * Method.
	 * @return the QToolButton of the ColorPickerWidget.
	 */
	QToolButton *getColorBackgroundButton() const;

	/**
	 * Method.
	 * @return the current color of the ColorPickerWidget.
	 */
	QColor getCurrentColor() const;

	/**
	 * Method.
	 * Set the current color of the ColorPickerWidget.
	 */
	void setCurrentColor(QColor newColor);

signals:
	/** 
	 * Method.
	 * This signals is emitted when the color is changed
	 * @param newColor the new current color of the ColorPicker
	 */
	void colorChanged(QColor newColor);

public slots:
	/**
	 * Method.
	 * Opens the window to choose a color
	 * Set the current color to the color chosen by the user
	 */
	void openColorDialog();

	/**
	 * Set the current color the color written in the QLineEdit.
	 */
	void editColor();
};

#endif // VISUAL_AUTOMATA_COLOR_PICKER_WIDGET_HPP
