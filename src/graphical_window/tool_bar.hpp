#ifndef VISUAL_AUTOMATA_TOOL_BAR_HPP
#define VISUAL_AUTOMATA_TOOL_BAR_HPP

#include <QApplication>
#include <QWidget>
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QFileDialog>
#include <QToolBar>
#include <QIcon>
#include <QPixmap>
#include <QCheckBox>
#include <QComboBox>
#include <memory>
#include <iostream>


/**
 * This class is the tool bar for the main Window. It contains several icons associated with action
 */
class ToolBar: public QToolBar {
Q_OBJECT

private:
	/**< The main window of the application */
	QMainWindow *mainWindow;
	/**< Separator 1 */
	QAction *separator1;
	/**< Separator 2 */
	QAction *separator2;
	/**< Separator 3 */
	QAction *separator3;
	/**< Separator 4 */
	QAction *separator4;
	/**< Separator 5 */
	QAction *separator5;
	/**< Separator 6 */
	QAction *separator6;
	/**< Action for copying an element (state/transition) */
	QAction *actionCopier;
	/**< Action for cutting an element */
	QAction *actionCouper;
	/**< Action for pasting an element */
	QAction *actionColler;
	/**< Action for zooming in the view */
	QAction *actionZoomPlus;
	/**< Action for resetting zoom to 1 (actual size) */
	QAction *actionTailleReelle;
	/**< Action for zooming out the view */
	QAction *actionZoomMoins;
	/**< Label for the selection box */
	QAction *actionGroupeTexte;
	/**< selection box which shows the created groups */
	QComboBox *affichageGroupes;
	/**< Label for the add group action */
	QAction *actionAddGroupeTexte;
	/**< Action for adding groups */
	QAction *actionAddGroupe;
	/**< CheckBox for displaying or not the weight of transitions */
	QCheckBox *afficherPoids;
	/**< CheckBox for displaying or not the grid */
	QCheckBox *afficherGrille;
	/**< Action for the rectangle selection */
	QAction *actionRectangleSelection;

public:
	/**
	 * Constructor.
	 *
	 * Initializes and places all the icons in the toolBar
	 * @param mainWindow is the MainWindow of the application
	 */
	explicit ToolBar(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~ToolBar() override;

	/**
	 * Method.
	 *
	 * Returns the selection box which displays the created groups
	 * @return the group selection box
	 */
	QComboBox *getAffichageGroupeBox() const;

	/**
	 * Method.
	 *
	 * Returns the action for creating groups
	 * @return the action for adding groups
	 */
	QAction *getActionAddGroupe() const;

	/**
	 * Method.
	 *
	 * Returns the current index of the group selection box
	 * @return the index of the selection box
	 */
	int getCurrentSelectedGroup() const;

	/**
	 * Method.
	 *
	 * Returns the label Adding groups
	 * @return the label actionAddGroupeTexte
	 */
	QAction *getActionAddGroupeTexte() const;

	/**
	 * Method.
	 *
	 * Returns the action for zooming in the view
	 * @return the zooming in action
	 */
	QAction *getActionZoomPlus() const;

	/**
	 * Method.
	 *
	 * Returns the action for returning to actual size (zoom of 1)
	 * @return the actual size action
	 */
	QAction *getActionTailleReelle() const;

	/**
	 * Method.
	 *
	 * Returns the action for zooming out the view
	 * @return the zooming out action
	 */
	QAction *getActionZoomMoins() const;

	/**
	 * Method.
	 *
	 * Returns the action for displaying or not the grid
	 * @return the grid action
	 */
	QCheckBox *getAfficherGrille() const;

	/**
	 * Method.
	 *
	 * Returns if the grid button is check or not
	 * @return the state of the grid checkbox (checked or not)
	 */
	bool gridChecked() const;

	/**
	 * Method.
	 *
	 * Check the grid button to true
	 */
	void checkGrid();

	/**
	 * Method.
	 *
	 * Uncheck the grid button
	 */
	void uncheckGrid();
};

#endif // VISUAL_AUTOMATA_TOOL_BAR_HPP
