#include "right_dock_widget.hpp"

#include <QMainWindow>

RightDockWidget::RightDockWidget(QMainWindow *mainWindow) : QDockWidget() {
	// Initializing the stateDock
	this->setAllowedAreas(Qt::RightDockWidgetArea);
	this->setFeatures(QDockWidget::NoDockWidgetFeatures);
	this->setContentsMargins(10, 10, 10, 10);
	mainWindow->addDockWidget(Qt::RightDockWidgetArea, this);

	// Adding a title to the stateDock
	labelAjouter = new QLabel("Propriétés", mainWindow);
	labelAjouter->setAlignment(Qt::AlignCenter);
	this->setTitleBarWidget(labelAjouter);

	// Creating a vertical layout for the rightDock
	verticalLayout = new QVBoxLayout();
	// Aligning the layout to Top Center
	verticalLayout->setAlignment(Qt::AlignTop | Qt::AlignCenter);


	// Creating an another widget which contains all the widgets for the rightDock and adding to it the vertical layout
	widgetDroit = new QWidget(this);
	widgetDroit->setLayout(verticalLayout);

	// Creating a scrollArea for the rightWidget
	scrollArea = new QScrollArea();
	scrollArea->setWidget(widgetDroit);
	scrollArea->setWidgetResizable(true);

	setMinimumSize(350, 1);

	// Adding the scrollArea to the rightDock
	this->setWidget(scrollArea);
}

void RightDockWidget::addWidget(QWidget &widget) {
	verticalLayout->addWidget(&widget);
}

void RightDockWidget::addWidget(QWidget &widget, Qt::AlignmentFlag alignmentFlag) {
	verticalLayout->addWidget(&widget, alignmentFlag);
}

RightDockWidget::~RightDockWidget() {
	delete labelAjouter;
}
