#ifndef VISUAL_AUTOMATA_LEFT_DOCK_WIDGET_HPP
#define VISUAL_AUTOMATA_LEFT_DOCK_WIDGET_HPP

#include <QWidget>
#include <QDockWidget>
#include <QMainWindow>
#include <QtGui>
#include <QCheckBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QScrollArea>
#include <iostream>
#include <memory>

/**
 * LeftDockWidget represents the the left dock widget which contains the buttons to add states or transitions to the automata
 */
class LeftDockWidget: public QDockWidget {
Q_OBJECT
private:
	/**< The main window of the application */
	QMainWindow *mainWindow;
	/**< Label 'add' */
	QLabel *labelAjouter;
	/**< Label States */
	QLabel *labelEtats;
	/**< Label Transitions */
	QLabel *labelTransitions;
	/**< Button to add a state*/
	QToolButton *stateButton;
	/**< Button to add an initial state */
	QToolButton *initialStateButton;
	/**< Button to add a final state */
	QToolButton *finalStateButton;
	/**< Button to add a transition between 2 states */
	QToolButton *leftTransitionButton;

	QVBoxLayout *verticalLayout;
	/**< Vertical layout for display vertically the widgets */

	/**< Widget which uses the vertical layout */
	QWidget *widgetGauche;
	/**< Scroll area for the left dock widget */
	QScrollArea *scrollArea;

public:
	/**
	 * Constructor.
	 * Initializes and places all the components in the dockWidget
	 * @param mainWindow is the mainWindow of the application
	 */
	explicit LeftDockWidget(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~LeftDockWidget() override;

	/**
	 * Method.
	 * Returns the stateButton which enables to add a state to the automata
	 * @return the button to add a state
	 */
	QToolButton *getStateButton() const;

	/**
	 * Method.
	 * Returns the initialStateButton which enables to add an initial state to the automata
	 * @return the button to add an initial state
	 */
	QToolButton *getInitialStateButton() const;

	/**
	 * Method.
	 * Returns the finalStateButton which enables to add a final state to the automata
	 * @return the button to add a final state
	 */
	QToolButton *getFinalStateButton() const;

	/**
	 * Method.
	 * Returns the leftTransitionButton which enables to add a transition between 2 states of the automata
	 * @return the button to add a transition
	 */
	QToolButton *getLeftTransitionButton() const;
};

#endif // VISUAL_AUTOMATA_LEFT_DOCK_WIDGET_HPP
