#include "preference_window.hpp"
#include <QDebug>

#ifndef HOME_CONFIG
#define HOME_CONFIG /home/toto
#endif

#define STR(X) #X

PreferenceWindow::PreferenceWindow() : QDialog(), boldFont(QFont()) {
	setWindowTitle("Préférences");
	setFixedSize(800, 500);

	boldFont.setPointSize(11);
	boldFont.setBold(true);

	preferencesSettings = new QSettings(STR(HOME_CONFIG) "/.visual-automata", QSettings::IniFormat);

	textGrid = new QGridLayout();
	textLabel = new QLabel("Couleur et style de police");
	textLabel->setFont(boldFont);
	textFontWidget = new FontWidget(this);
	textGrid->addWidget(textLabel, 0, 0, Qt::AlignBottom);
	textGrid->addWidget(textFontWidget, 1, 0, Qt::AlignTop);

	textGroupBox = new QGroupBox("Texte");
	textGroupBox->setLayout(textGrid);

	textStackedLayout = new QStackedLayout();
	textStackedLayout->addWidget(textGroupBox);

	mainGrid = new QGridLayout(this);
	mainGrid->setColumnStretch(0, 1);
	mainGrid->setColumnStretch(1, 1);
	mainGrid->setColumnStretch(2, 1);
	mainGrid->setRowStretch(0, 0);
	mainGrid->setRowStretch(1, 1);
	mainGrid->setContentsMargins(20, 20, 20, 20);
	mainGrid->addLayout(textStackedLayout, 0, 0, 1, 1);

	squareRadioButton = new QRadioButton("Style carré");
	QIcon squareGridIcon = QIcon(":/pictures/choice-grid-square.png");
	squareRadioButton->setIcon(squareGridIcon);
	squareRadioButton->setIconSize(QSize(60, 60));
	squareRadioButton->setChecked(true);
	triangleRadioButton = new QRadioButton("Style triangle");
	QIcon triangleGridIcon = QIcon(":/pictures/choice-grid-triangle.png");
	triangleRadioButton->setIcon(triangleGridIcon);
	triangleRadioButton->setIconSize(QSize(60, 60));
	crossRadioButton = new QRadioButton("Style croix");
	QIcon crossGridIcon = QIcon(":/pictures/choice-grid-cross.png");
	crossRadioButton->setIcon(crossGridIcon);
	crossRadioButton->setIconSize(QSize(60, 60));

	gridGrid = new QHBoxLayout();
	gridGrid->addWidget(squareRadioButton);
	gridGrid->addWidget(triangleRadioButton);
	gridGrid->addWidget(crossRadioButton);

	gridGroupeBox = new QGroupBox("Grille");
	gridGroupeBox->setLayout(gridGrid);
	gridStackedLayout = new QStackedLayout();
	gridStackedLayout->addWidget(gridGroupeBox);

	mainGrid->addLayout(gridStackedLayout, 0, 1, 1, 2);

	background = new QLabel("Couleur de fond");
	background->setFont(boldFont);
	trait = new QLabel("Couleur du trait");
	trait->setFont(boldFont);
	traitStyleState = new QLabel("Style et épaisseur");
	traitStyleState->setFont(boldFont);
	stateBackgroundColorPicker = new ColorPickerWidget(this);
	stateColorPicker = new ColorPickerWidget(this);
	stateLineWidget = new LineWidget(this);

	initialState1RadioButton = new QRadioButton();
	initialState1RadioButton->setChecked(true);
	QIcon initialState1Icon = QIcon(":/pictures/initial_state.png");
	initialState1RadioButton->setIcon(initialState1Icon);
	initialState1RadioButton->setIconSize(QSize(70, 70));
	initialState2RadioButton = new QRadioButton();
	QIcon initialState2Icon = QIcon(":/pictures/initial_state2.png");
	initialState2RadioButton->setIcon(initialState2Icon);
	initialState2RadioButton->setIconSize(QSize(70, 70));
	initialState2RadioButton->setEnabled(false);
	initialStateLayout = new QHBoxLayout();
	initialStateLayout->addWidget(initialState1RadioButton);
	initialStateLayout->addWidget(initialState2RadioButton);

	initialStateGroupBox = new QGroupBox("Etat initial");
	initialStateGroupBox->setLayout(initialStateLayout);

	finalState1RadioButton = new QRadioButton();
	finalState1RadioButton->setChecked(true);
	QIcon finalState1Icon = QIcon(":/pictures/final_state.png");
	finalState1RadioButton->setIcon(finalState1Icon);
	finalState1RadioButton->setIconSize(QSize(55, 55));
	finalState2RadioButton = new QRadioButton();
	QIcon finalState2Icon = QIcon(":/pictures/final_state2.png");
	finalState2RadioButton->setIcon(finalState2Icon);
	finalState2RadioButton->setIconSize(QSize(70, 70));
	finalState2RadioButton->setEnabled(false);
	finalStateLayout = new QHBoxLayout();
	finalStateLayout->addWidget(finalState1RadioButton);
	finalStateLayout->addWidget(finalState2RadioButton);

	finalStateGroupBox = new QGroupBox("Etat final");
	finalStateGroupBox->setLayout(finalStateLayout);

	stateGrid = new QGridLayout();
	stateGrid->addWidget(background, 0, 0);
	stateGrid->addWidget(stateBackgroundColorPicker, 1, 0, Qt::AlignTop);
	stateGrid->addWidget(trait, 2, 0);
	stateGrid->addWidget(stateColorPicker, 3, 0, Qt::AlignTop);
	stateGrid->addWidget(traitStyleState, 4, 0);
	stateGrid->addWidget(stateLineWidget, 5, 0, Qt::AlignTop);
	stateGrid->addWidget(initialStateGroupBox, 0, 1, 2, 1);
	stateGrid->addWidget(finalStateGroupBox, 2, 1, 2, 1);

	stateGroupBox = new QGroupBox("Etats", this);
	stateGroupBox->setLayout(stateGrid);
	stateStackedLayout = new QStackedLayout();
	stateStackedLayout->addWidget(stateGroupBox);

	mainGrid->addLayout(stateStackedLayout, 1, 0, 1, 2);
	transitionSlider = new CustomSlider(1, 20, 3, this);
	transitionAccuracy = new QLabel("Précision");
	transitionAccuracy->setFont(boldFont);
	transitionLine = new QLabel("Couleur du trait");
	transitionLine->setFont(boldFont);
	transitionStyle = new QLabel("Style et épaisseur");
	transitionStyle->setFont(boldFont);
	transitionColorPicker = new ColorPickerWidget(this);
	transitionLineWidget = new LineWidget(this);

	transitionGrid = new QGridLayout();
	transitionGrid->addWidget(transitionAccuracy, 0, 0);
	transitionGrid->addWidget(transitionSlider, 1, 0, Qt::AlignTop);
	transitionGrid->addWidget(transitionLine, 2, 0);
	transitionGrid->addWidget(transitionColorPicker, 3, 0, Qt::AlignTop);
	transitionGrid->addWidget(transitionStyle, 4, 0);
	transitionGrid->addWidget(transitionLineWidget, 5, 0, Qt::AlignTop);

	transitionGroupBox = new QGroupBox("Transitions", this);
	transitionGroupBox->setLayout(transitionGrid);
	transitionStackedLayout = new QStackedLayout();
	transitionStackedLayout->addWidget(transitionGroupBox);

	mainGrid->addLayout(transitionStackedLayout, 1, 2);

	setLayout(mainGrid);
}

PreferenceWindow::~PreferenceWindow() {

	delete textLabel;
	delete textFontWidget;

	delete textGrid;
	delete textGroupBox;
	delete textStackedLayout;

	delete squareRadioButton;
	delete crossRadioButton;
	delete triangleRadioButton;

	delete gridGrid;
	delete gridGroupeBox;
	delete gridStackedLayout;

	delete background;
	delete trait;
	delete traitStyleState;

	delete stateBackgroundColorPicker;
	delete stateColorPicker;
	delete stateLineWidget;
	delete initialState1RadioButton;
	delete initialState2RadioButton;
	delete initialStateLayout;
	delete initialStateGroupBox;

	delete finalState1RadioButton;
	delete finalState2RadioButton;
	delete finalStateLayout;
	delete finalStateGroupBox;

	delete stateGrid;
	delete stateGroupBox;
	delete stateStackedLayout;

	delete transitionAccuracy;
	delete transitionLine;
	delete transitionStyle;
	delete transitionSlider;
	delete transitionColorPicker;
	delete transitionLineWidget;

	delete transitionGrid;
	delete transitionGroupBox;
	delete transitionStackedLayout;

	delete preferencesSettings;

	delete mainGrid;
}

bool PreferenceWindow::squareChecked() {
	return squareRadioButton->isChecked();
}

bool PreferenceWindow::triangleChecked() {
	return triangleRadioButton->isChecked();
}

bool PreferenceWindow::crossChecked() {
	return crossRadioButton->isChecked();
}

// Loads the data of the config file and updates the widget
void PreferenceWindow::loadPreferenceData() {
	QColor textFontColorPickerColor =
			preferencesSettings->value("font/color", QColor(Qt::black)).value<QColor>();
	textFontWidget->getFontColorPicker()->setCurrentColor(textFontColorPickerColor);

	QColor lineColorPickerColor =
			preferencesSettings->value("state/lineColor", QColor(Qt::darkBlue)).value<QColor>();
	stateColorPicker->setCurrentColor(lineColorPickerColor);
	QColor stateColorPickerColor =
			preferencesSettings->value("state/backgroundColor", QColor(37, 150, 190))
			                   .value<QColor>();
	stateBackgroundColorPicker->setCurrentColor(stateColorPickerColor);
	int statePenStyle = Qt::SolidLine;
	Qt::PenStyle stateLineWidgetStyle =
			Qt::PenStyle(preferencesSettings->value("state/lineStyle", statePenStyle).toInt());
	stateLineWidget->getLinePicker()->setFocusedStyle(stateLineWidgetStyle);
	int stateLineWidgetSize = preferencesSettings->value("state/lineSize", 3).toInt();
	stateLineWidget->getLineSizePicker()->setValue(stateLineWidgetSize);

	transitionSlider->setValue(preferencesSettings->value("transition/accuracy", 16).toInt());
	QColor transitionColorPickerColor =
			preferencesSettings->value("transition/color", QColor(Qt::black)).value<QColor>();
	transitionColorPicker->setCurrentColor(transitionColorPickerColor);
	int transitionPenStyle = Qt::SolidLine;
	Qt::PenStyle transitionLineWidgetStyle =
			Qt::PenStyle(preferencesSettings->value("transition/lineStyle", transitionPenStyle)
			                                .toInt());
	transitionLineWidget->getLinePicker()->setFocusedStyle(transitionLineWidgetStyle);
	int transitionLineWidgetSize = preferencesSettings->value("transition/lineSize", 3).toInt();
	transitionLineWidget->getLineSizePicker()->setValue(transitionLineWidgetSize);

	emit preferenceDataChanged();
}

// Removes the old config file and creates a new one with the new data
void PreferenceWindow::savePreferenceData() {
	emit preferenceDataChanged();

	preferencesSettings->setValue("font/color",
	                              textFontWidget->getFontColorPicker()->getCurrentColor());
	preferencesSettings->setValue("state/backgroundColor",
	                              stateBackgroundColorPicker->getCurrentColor());
	preferencesSettings->setValue("state/lineColor", stateColorPicker->getCurrentColor());
	preferencesSettings->setValue("state/lineSize", stateLineWidget->getLineSizePicker()->value());
	preferencesSettings->setValue("state/lineStyle",
	                              stateLineWidget->getLinePicker()->getPickerStyle());
	preferencesSettings->setValue("transition/color", transitionColorPicker->getCurrentColor());
	preferencesSettings->setValue("transition/lineSize",
	                              transitionLineWidget->getLineSizePicker()->value());
	preferencesSettings->setValue("transition/lineStyle",
	                              transitionLineWidget->getLinePicker()->getPickerStyle());
	preferencesSettings->setValue("transition/accuracy", transitionSlider->getValue());
}

FontWidget *PreferenceWindow::getTextFontWidget() {
	return textFontWidget;
}

QRadioButton *PreferenceWindow::getSquareRadioButton() {
	return squareRadioButton;
}

QRadioButton *PreferenceWindow::getTriangleRadioButton() {
	return triangleRadioButton;
}

QRadioButton *PreferenceWindow::getCrossRadioButton() {
	return crossRadioButton;
}

ColorPickerWidget *PreferenceWindow::getStateBackgroundColorPicker() {
	return stateBackgroundColorPicker;
}

ColorPickerWidget *PreferenceWindow::getStateColorPicker() {
	return stateColorPicker;
}

LineWidget *PreferenceWindow::getStateLineWidget() {
	return stateLineWidget;
}

QRadioButton *PreferenceWindow::getInitialState1RadioButton() {
	return initialState1RadioButton;
}

QRadioButton *PreferenceWindow::getInitialState2RadioButton() {
	return initialState2RadioButton;
}

QRadioButton *PreferenceWindow::getFinalState1RadioButton() {
	return finalState1RadioButton;
}

QRadioButton *PreferenceWindow::getFinalState2RadioButton() {
	return finalState2RadioButton;
}

CustomSlider *PreferenceWindow::getTransitionSlider() {
	return transitionSlider;
}

ColorPickerWidget *PreferenceWindow::getTransitionColorPicker() {
	return transitionColorPicker;
}

LineWidget *PreferenceWindow::getTransitionLineWidget() {
	return transitionLineWidget;
}
