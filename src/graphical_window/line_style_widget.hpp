#ifndef VISUAL_AUTOMATA_LINE_STYLE_WIDGET_HPP
#define VISUAL_AUTOMATA_LINE_STYLE_WIDGET_HPP

#include <QComboBox>
#include <QVector>

class LineStyleWidget: public QComboBox {
Q_OBJECT
private:
	QWidget parent;
	QVector<Qt::PenStyle> styles;

public:
	explicit LineStyleWidget(QWidget *parent);

	Qt::PenStyle &getFocusedStyle();

	int getPickerStyle();

	void setFocusedStyle(Qt::PenStyle &newStyle);
};

#endif // VISUAL_AUTOMATA_LINE_STYLE_WIDGET_HPP
