#ifndef VISUAL_AUTOMATA_PREFERENCE_WINDOW_HPP
#define VISUAL_AUTOMATA_PREFERENCE_WINDOW_HPP

#include <QWidget>
#include <QDialog>
#include <QColor>
#include <QPushButton>
#include <QRadioButton>
#include <QGroupBox>
#include <QStackedLayout>

#include "font_widget.hpp"
#include "color_picker_widget.hpp"
#include "custom_slider.hpp"
#include "line_widget.hpp"
#include <iostream>

#include <QSettings>

/**
 * This class is the window for the preferences
 */
class PreferenceWindow: public QDialog {
Q_OBJECT

private:
	/**< The QGridLayout for placing the widget in the category "Text" */
	QGridLayout *textGrid;
	/**< The text for the title of the category "Text" */
	QLabel *textLabel;
	/**< The widget to choose the font and the color of the text (not implemented) */
	FontWidget *textFontWidget;

	/**< The QGroupBox containing all widget in the category "Text" */
	QGroupBox *textGroupBox;
	/**< The layout inside the grid layout for the category "Text" */
	QStackedLayout *textStackedLayout;

	/**< Button for square grid */
	QRadioButton *squareRadioButton;
	/**< Button for the triangle grid */
	QRadioButton *triangleRadioButton;
	/**< Button for the cross grid */
	QRadioButton *crossRadioButton;

	/**< The QGridLayout for placing all the others QGridLayout (there are small grids for each categories in the mainGrid) */
	QGridLayout *mainGrid;
	/**< All horizontal layout for placing widgets */
	QHBoxLayout *gridGrid;
	/**< The QGroupBox containing all widget in the category "Grid" */
	QGroupBox *gridGroupeBox;
	/**< The layout inside the grid layout for the category "Grid" */
	QStackedLayout *gridStackedLayout;

	/**< The text for the background */
	QLabel *background;
	/**< The text for the line */
	QLabel *trait;
	/**< The text for the line style of states */
	QLabel *traitStyleState;

	/**< To choose a color for the background of states */
	ColorPickerWidget *stateBackgroundColorPicker;
	/**< To choose a color for the line of states */
	ColorPickerWidget *stateColorPicker;
	/**< To choose the style and width for line of states */
	LineWidget *stateLineWidget;

	/**< To choose the first style of initial transitions */
	QRadioButton *initialState1RadioButton;
	/**< To choose the second style of initial transitions (not implemented) */
	QRadioButton *initialState2RadioButton;
	/**< An horizontal layout for placing the widgets for initial transitions */
	QHBoxLayout *initialStateLayout;
	/**< The QGroupBox containing all widget in the category "Etat initial" */
	QGroupBox *initialStateGroupBox;

	/**< To choose the first style of final transitions */
	QRadioButton *finalState1RadioButton;
	/**< To choose the second style of final transitions (not implemented) */
	QRadioButton *finalState2RadioButton;
	/**< An horizontal layout for placing the widgets for final transitions */
	QHBoxLayout *finalStateLayout;
	/**< The QGroupBox containing all widget in the category "Etat final" */
	QGroupBox *finalStateGroupBox;

	/**< The QGridLayout for placing the widget in the category "Etat" */
	QGridLayout *stateGrid;
	/**< The QGroupBox containing all widget in the category "Etat" */
	QGroupBox *stateGroupBox;
	/**< The layout inside the grid layout for the category "Etat" */
	QStackedLayout *stateStackedLayout;

	/**< The text for the accuracy of transitions */
	QLabel *transitionAccuracy;
	/**< The text the lines of transitions */
	QLabel *transitionLine;
	/**< The text for the style of transitions */
	QLabel *transitionStyle;
	/**< The Custom slider to choose the accuracy of transitions */
	CustomSlider *transitionSlider;
	/**< The ColorPickerWidget to choose the color of transitions */
	ColorPickerWidget *transitionColorPicker;
	LineWidget *transitionLineWidget;

	/**< The QGridLayout for placing the widget in the category "Transition" */
	QGridLayout *transitionGrid;
	/**< The QGroupBox containing all widget in the category "Transition" */
	QGroupBox *transitionGroupBox;
	/**< The layout inside the grid layout for the category "Transition" */
	QStackedLayout *transitionStackedLayout;

	/**< Allows user to save and load data */
	QSettings *preferencesSettings;

	/**< A bold font used when we want to set the text in bold */
	QFont boldFont;

public:
	/**
	 * Constructor.
	 * Places all components in the different grids
	 */
	PreferenceWindow();

	/**
	 * Destructor.
	 */
	~PreferenceWindow() override;

	/**
	 * Method.
	 * @return The FontWidget for the category "Text"
	 * @see FontWidget
	 */
	FontWidget *getTextFontWidget();

	/**
	 * Method.
	 * @return The button for the square grid
	 */
	QRadioButton *getSquareRadioButton();

	/**
	 * Method.
	 * @return The button for the triangle grid
	 */
	QRadioButton *getTriangleRadioButton();

	/**
	 * Method.
	 * @return The button for the cross grid
	 */
	QRadioButton *getCrossRadioButton();

	/**
	 * Method.
	 * @return The ColorPickerWidget for choosing the line color of states
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getStateColorPicker();

	/**
	 * Method.
	 * @return The ColorPickerWidget for choosing the background color of states
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getStateBackgroundColorPicker();

	/**
	 * Method.
	 * @return The LineWidget for choosing the style and width of text of states
	 * @see LineWidget
	 */
	LineWidget *getStateLineWidget();

	/**
	 * Method.
	 * @return The QRadioButton for choosing the first style for initial states
	 * @see TransitionDrawable
	 * @see StateDrawable
	 */
	QRadioButton *getInitialState1RadioButton();

	/**
	 * Method.
	 * @return The QRadioButton for choosing the second style for initial states (not implemented)
	 */
	QRadioButton *getInitialState2RadioButton();

	/**
	 * Method.
	 * @return The QRadioButton for choosing the first style for final states
	 * @see StateDrawable
	 */
	QRadioButton *getFinalState1RadioButton();

	/**
	 * Method.
	 * @return The QRadioButton for choosing the first style for initial states (not implemented)
	 */
	QRadioButton *getFinalState2RadioButton();

	/**
	 * Method.
	 * @return The CustomSlider for choosing the accuracy of transitions
	 * @see CustomSlider
	 * @see TransitionDrawable
	 */
	CustomSlider *getTransitionSlider();

	/**
	 * Method.
	 * @return The ColorPickerWidget for choosing the line color of transitions
	 * @see ColorPickerWidget
	 * @see TransitionDrawable
	 */
	ColorPickerWidget *getTransitionColorPicker();

	/**
	 * Method.
	 * @return The LineWidget for choosing the style and width of transitions lines
	 * @see LineWidget
	 * @see TransitionDrawable
	 */
	LineWidget *getTransitionLineWidget();

	/**
	 * Method.
	 * Loads the data saved in the preferences file
	 */
	void loadPreferenceData();

	/**
	 * Method.
	 * @return true if the button for the square grid is checked
	 */
	bool squareChecked();

	/**
	 * Method.
	 * @return true if the button for the triangle grid is checked
	 */
	bool triangleChecked();

	/**
	 * Method.
	 * @return true if the button for the cross grid is checked
	 */
	bool crossChecked();

public slots:
	/**
	 * Method.
	 * Saves preferences data in the preferences file when they are changed by the user
	 * Must be connected to the signal preferenceDataChanged in the AutomataController
	 * @see AutomataController
	 */
	void savePreferenceData();

signals:
	/**
	 * Method.
	 * This signals is emitted in the AutomataController when a property of one widget is changed by the user.
	 * @see AutomataController
	 */
	void preferenceDataChanged();
};

#endif // VISUAL_AUTOMATA_PREFERENCE_WINDOW_HPP
