#include "line_widget.hpp"

LineWidget::LineWidget(QWidget *parent) : QWidget(parent) {
	lineLayout = new QHBoxLayout();
	lineLayout->setSpacing(0);

	linePicker = new LineStyleWidget(this);
	lineSizePicker = new QSpinBox(this);
	lineSizePicker->setRange(1, 10);

	lineLayout->addWidget(linePicker);
	lineLayout->addWidget(lineSizePicker);
	setLayout(lineLayout);
}

LineWidget::~LineWidget() {
	delete linePicker;
	delete lineSizePicker;
	delete lineLayout;
}

LineStyleWidget *LineWidget::getLinePicker() {
	return linePicker;
}

QSpinBox *LineWidget::getLineSizePicker() {
	return lineSizePicker;
}
