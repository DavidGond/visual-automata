#ifndef VISUAL_AUTOMATA_PAIR_LABEL_WIDGET_HPP
#define VISUAL_AUTOMATA_PAIR_LABEL_WIDGET_HPP
#include <QWidget>
#include <QDockWidget>
#include <QMainWindow>
#include <QtGui>
#include <QCheckBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QToolButton>
#include <QSpinBox>
#include <QLineEdit>
#include <QComboBox>
#include <QFontComboBox>
#include <QMessageBox>
#include <QFontDialog>
#include <QColorDialog>
#include <QScrollArea>

/**
 * The PairLabelWidget class provides a widget which display 2 widgets horizontally (often a label followed by a widget).
 */
class PairLabelWidget: public QWidget {
private:
	/**< The label of the pair of 2 widgets */
	QWidget *label;
	/**< The (2nd) widget of the pair of 2 widgets */
	QWidget *widget;
	/**< Horizontal layout to display the pair horizontally */
	QHBoxLayout *horizontalLayout;
	/**< The widget which takes the layout and so the pair of widget */
	QWidget *pairWidget;

public:
	/**
	 * Constructor
	 * @param widget1 is the first widget of the pair (often a label)
	 * @param widget2 is the second widget of the pair
	 * @param parent is the parent of PairLabelWidget
	 */
	PairLabelWidget(QWidget *widget1, QWidget *widget2, QWidget *parent);

	/**
	 * Destructor.
	 */
	~PairLabelWidget() override;
};

#endif // VISUAL_AUTOMATA_PAIR_LABEL_WIDGET_HPP
