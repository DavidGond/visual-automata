#include "custom_slider.hpp"
#include <QDebug>

CustomSlider::CustomSlider(int minValue, int maxValue, int startValue, QWidget *parent) :
		QWidget(parent),
		slider(new QSlider(this)),
		minValue(new QLabel(QString::number(minValue), this)),
		maxValue(new QLabel(QString::number(maxValue), this)),
		currentValue(new QLabel(QString::number(startValue), this)),
		min(minValue),
		max(maxValue),
		grid(new QGridLayout()) {
	if (startValue < minValue || startValue > maxValue) {
		qFatal("Wrong start value for this custom slider");
	}

	int range = max - min;

	if (range <= 0) {
		qFatal("The range of the slider isn't correct");
	}

	slider->setMinimum(minValue);
	slider->setMaximum(maxValue);
	slider->setSliderPosition(startValue);
	slider->setOrientation(Qt::Horizontal);

	grid->addWidget(this->minValue, 0, 0);
	grid->addWidget(this->maxValue, 0, range);
	grid->addWidget(slider, 2, 0, 1, range + 1);
	// The column span is the number of column available in the right of the label the avoid a graphical bug
	grid->addWidget(this->currentValue,
	                1,
	                startValue - minValue,
	                1,
	                range - (startValue - min) + 1,
	                Qt::AlignLeft);

	QObject::connect(slider, SIGNAL(valueChanged(int)), this, SLOT(onValueChanged(int)));

	setLayout(grid);
}

CustomSlider::~CustomSlider() {
	delete slider;
	delete minValue;
	delete maxValue;
	delete currentValue;
	delete grid;
}

void CustomSlider::onValueChanged(int value) {
	currentValue->setText(QString::number(value));

	// Replacing the label with the current value the the correct column
	// The column span is the number of column available in the right of the label the avoid a graphical bug
	grid->addWidget(currentValue,
	                1,
	                value - min,
	                1,
	                (max - min) - (value - min) + 1,
	                Qt::AlignLeft);
}

void CustomSlider::setValue(int value) {
	slider->setValue(value);
}

int CustomSlider::getValue() const {
	return slider->value();
}

QSlider *CustomSlider::getSlider() const {
	return slider;
}
