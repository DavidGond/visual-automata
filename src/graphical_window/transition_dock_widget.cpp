#include "transition_dock_widget.hpp"

TransitionDockWidget::TransitionDockWidget(QMainWindow *mainWindowP) :
		RightDockWidget(mainWindowP) {

	// Creating a bold Font for the subtitles
	QFont boldFont = QFont();
	boldFont.setBold(true);

	// Creating 4 labels for the subtitles
	labelType = new QLabel("Type", this);
	labelType->setFont(boldFont);
	labelTrait = new QLabel("Trait", this);
	labelTrait->setFont(boldFont);
	labelPrecision = new QLabel("Précision", this);
	labelPrecision->setFont(boldFont);
	labelTexte = new QLabel("Texte", this);
	labelTexte->setFont(boldFont);
	labelPoids = new QLabel("Poids", this);
	labelPoids->setFont(boldFont);
	labelDelete = new QLabel("Suppression", this);
	labelDelete->setFont(boldFont);

	// Initializing the 2 QCheckBox for the arrow type (left/right/both), with a Layout to center them
	leftArrow = new QCheckBox("Flèche gauche");
	rightArrow = new QCheckBox("Flèche droite");

	layoutArrow = new QVBoxLayout();
	layoutArrow->setAlignment(Qt::AlignLeft);
	layoutArrow->addWidget(leftArrow);
	layoutArrow->addWidget(rightArrow);

	// Creating a new Widget for displaying the 2 QCheckBox
	arrowWidget = new QWidget();
	arrowWidget->setLayout(layoutArrow);
	// Creating a colorPalette Icon (used 2 times)
	QIcon colorPaletteIcon = QIcon(":/pictures/palette.png");

	// Creating a horizontal layout for the Trait and colorPalette
	traitLayout = new QVBoxLayout();
	traitLayout->setSpacing(0);
	// Initializing the lineColorPicker widget
	lineColorPicker = new ColorPickerWidget(this);
	// Initializing the LineWidget
	lineWidget = new LineWidget(this);
	// Adding the widgets to the previous layout
	traitLayout->addWidget(lineColorPicker);
	traitLayout->addWidget(lineWidget);
	// Creating a new Widget for displaying the pair
	traitWidget = new QWidget(this);
	traitWidget->setLayout(traitLayout);

	// Initializing the customSlider widget
	transitionAccuracySlider = new CustomSlider(1, 20, 10, this);
	// Initializing the transitionText
	transitionText = new QLineEdit();
	transitionText->setAlignment(Qt::AlignCenter);

	// Initializing the textFontWidget
	textFontWidget = new FontWidget(this);

	// colorTraitText the SPinBox for the Weight of the transition
	weightText = new QLineEdit();
	weightText->setAlignment(Qt::AlignCenter);

	// Adding the delete button
	deleteButton = new QPushButton("Supprimer", this);
	hidingCheckBox = new QCheckBox("Cacher");
	hidingCheckBox->setCheckState(Qt::Unchecked);

	// Adding the widgets to this layout
	addWidget(*labelType);
	addWidget(*arrowWidget);

	addWidget(*labelTrait);
	addWidget(*traitWidget);
	addWidget(*labelPrecision);
	addWidget(*transitionAccuracySlider);

	addWidget(*labelTexte);
	addWidget(*transitionText);
	addWidget(*textFontWidget);

	addWidget(*labelPoids);
	addWidget(*weightText);

	addWidget(*labelDelete);
	addWidget(*deleteButton);
	addWidget(*hidingCheckBox);
}

TransitionDockWidget::~TransitionDockWidget() {
	delete labelType;
	delete labelTrait;
	delete labelPrecision;
	delete labelTexte;
	delete labelPoids;
	delete labelDelete;

	delete leftArrow;
	delete rightArrow;
	delete layoutArrow;
	delete arrowWidget;

	delete lineColorPicker;
	delete lineWidget;
	delete traitLayout;
	delete traitWidget;

	delete transitionAccuracySlider;
	delete transitionText;
	delete textFontWidget;

	delete weightText;
	delete deleteButton;
}

QCheckBox *TransitionDockWidget::getLeftArrow() const {
	return leftArrow;
}

QCheckBox *TransitionDockWidget::getRightArrow() const {
	return rightArrow;
}

ColorPickerWidget *TransitionDockWidget::getLineColorPicker() const {
	return lineColorPicker;
}

LineWidget *TransitionDockWidget::getLineWidget() const {
	return lineWidget;
}

QLineEdit *TransitionDockWidget::getTransitionText() const {
	return transitionText;
}

FontWidget *TransitionDockWidget::getTextFontWidget() const {
	return textFontWidget;
}

QLineEdit *TransitionDockWidget::getWeightText() const {
	return weightText;
}

CustomSlider *TransitionDockWidget::getTransitionAccuracySlider() const {
	return transitionAccuracySlider;
}

QPushButton *TransitionDockWidget::getDeleteButton() const {
	return deleteButton;
}

QCheckBox *TransitionDockWidget::getHidingCheckBox() const {
	return hidingCheckBox;
}
