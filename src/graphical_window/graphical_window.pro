TEMPLATE = lib

INCLUDEPATH += . ..

HEADERS += \
    color_picker_widget.hpp \
    font_widget.hpp \
    left_dock_widget.hpp \
    menu_bar.hpp \
    preference_window.hpp \
    state_dock_widget.hpp \
    line_widget.hpp \
    line_style_widget.hpp \
    tool_bar.hpp \
    transition_dock_widget.hpp \
    custom_slider.hpp \

QT += widgets

SOURCES += \
    color_picker_widget.cpp \
    font_widget.cpp \
    line_widget.cpp \
    line_style_widget.cpp \
    left_dock_widget.cpp \
    menu_bar.cpp \
    preference_window.cpp \
    state_dock_widget.cpp \
    tool_bar.cpp \
    transition_dock_widget.cpp \
    custom_slider.cpp \

RESOURCES += \
    Ressources.qrc
