#ifndef VISUAL_AUTOMATA_GROUP_DOCK_WIDGET_HPP
#define VISUAL_AUTOMATA_GROUP_DOCK_WIDGET_HPP

#include <QWidget>
#include <QDockWidget>
#include <QMainWindow>
#include <QtGui>
#include <QCheckBox>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QSpinBox>
#include <iostream>

#include "right_dock_widget.hpp"
#include "font_widget.hpp"
#include "color_picker_widget.hpp"
#include "line_widget.hpp"
#include "pair_label_widget.hpp"

class GroupDockWidget;

/**
 * The GroupDockWidget class inherits RightDockWidget and provides a dockWidget which displays the properties of a group of states of the automata
 */
class GroupDockWidget: public RightDockWidget {
Q_OBJECT
private:
	/**< The main window of the application */
	QMainWindow *mainWindow;
	/**< Label : characteristics of the group */
	QLabel *labelGroupCaracs;
	/**< Label : group drawing style */
	QLabel *labelGroupDrawing;
	/**< Label : delete the group */
	QLabel *labelDeleteGroup;
	/**< Label : name of the group */
	QLabel *labelName;
	/**< Label : priority of the group */
	QLabel *labelPriority;
	/**< Label : editable */
	QLabel *labelEditable;
	/**< Label : apply style */
	QLabel *labelApplyStyle;
	/**< Label : pen */
	QLabel *labelPen;
	/**< Label : brush */
	QLabel *labelBrush;
	/**< Label : length and style of the trait */
	QLabel *labelLineLength;
	/**< Label : style (never used) */
	QLabel *labelLineStyle{};

	/**< Text field for the group name */
	QLineEdit *editName;
	/**< SpinBox to change the group priority */
	QSpinBox *prioritySpinBox;
	/**< CheckBox to choose whether the group is editable or not */
	QCheckBox *editable;

	/**< CheckBox to choose whether the style group is applied or not */
	QCheckBox *applyStyle;
	/**< Widget to choose the group pen color */
	ColorPickerWidget *ColorPickerPen;
	/**< Widget to choose the group brush color */
	ColorPickerWidget *ColorPickerBrush;
	/**< Widget to choose the group pen size adn style */
	LineWidget *styleSizePen;
	/**< Button to delete the group */
	QPushButton *deleteGroupButton;

	/**< PairWidget name : label + text field */
	PairLabelWidget *namePairWidget;
	/**< PairWidget priority : label + spinBox */
	PairLabelWidget *priorityPairWidget;
	/**< PairWidget editable : label + checkbox */
	PairLabelWidget *editablePairWidget;
	/**< PairWidget editable : label + checkbox */
	PairLabelWidget *applyStylePairWidget;
	/**< PairWidget pen : label + colorpicker */
	PairLabelWidget *penPairWidget;
	/**< PairWidget brush : label + colorpicker */
	PairLabelWidget *brushPairWidget;
	/**< PairWidget style & size : label + spinboxes */
	PairLabelWidget *linePairWidget;


public:
	/**
	 * Constructor.
	 *
	 * @param mainWindow is the mainWindow of the application
	 */
	explicit GroupDockWidget(QMainWindow *mainWindow);

	/**
	 * Destructor.
	 */
	~GroupDockWidget() override;

	/**
	 * Method.
	 *
	 * Returns the text field representing the group name
	 * @return the text field of the group
	 */
	QLineEdit *getNameWidget();

	/**
	 * Method.
	 *
	 * Sets the name of the group in the text field
	 * @param name is the name of the group
	 */
	void setNameWidget(const std::string &name);

	/**
	 * Method.
	 *
	 * Returns the group priority spin box
	 * @return the group priority spin box
	 */
	QSpinBox *getPrioritySpinBox();

	/**
	 * Method.
	 *
	 * Sets the priority of the group to p
	 * @param p is the priority of the group
	 */
	void setPrioritySpinBox(int p);

	/**
	 * Method.
	 *
	 * Returns the 'editable' check box of the group
	 * @return editable checkbox
	 */
	QCheckBox *getEditableCheckBox();

	/**
	  Method.

	 * Sets the 'editable' group state to b (true of false)
	 * @param b is the state 'editable' or not of the group
	 */
	void setCheckEditable(bool b);

	/**
	 * Method.
	 *
	 * Returns the 'applyStyle' check box of the group
	 * @return applyStyle checkbox
	 */
	QCheckBox *getApplyStyleCheckBox();

	/**
	  Method.

	 * Sets the 'applyStyle' group state to b (true of false)
	 * @param b is the state 'applyStyle' or not of the group
	 */
	void setCheckApplyStyle(bool b);

	/**
	 * Method.
	 *
	 * Returns the ColorPickerWidget to change the group pen color
	 * @return the widget to change the group pen color
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getColorPickerPen();

	/**
	 * Method.
	 *
	 * Sets the group pen color to a new color
	 * @param color is the new color of the group pen
	 */
	void setColorPickerPen(QColor color);

	/**
	 * Method.
	 *
	 * Returns the ColorPickerWidget to change the group brush color
	 * @return the widget to change the group brush color
	 * @see ColorPickerWidget
	 */
	ColorPickerWidget *getColorPickerBrush();

	/**
	 * Method.
	 *
	 * Sets the group brush color to a new color
	 * @param color is the new color of the group brush
	 */
	void setColorPickerBrush(QColor color);

	/**
	 * Method.
	 *
	 * Returns the LineWidget to change the group pen size and style
	 * @return the widget to change the group pen style and size
	 * @see LineWidget
	 */
	LineWidget *getStyleSizePen();

	/**
	 * Method.
	 *
	 * Sets the group pen style to a new style
	 * @param style is the new style of the group pen
	 */
	void setStylePen(Qt::PenStyle style);

	/**
	 * Method.
	 *
	 * Sets the group pen size to a new size
	 * @param s is the new size of the group pen
	 */
	void setSizePen(int s);

	/**
	 * Method.
	 *
	 * Returns the deleteGroup button to delete the group
	 * @return the button to delete the group
	 */
	QPushButton *getDeleteGroupButton();
};

#endif // VISUAL_AUTOMATA_GROUP_DOCK_WIDGET_HPP
