#ifndef VISUAL_AUTOMATA_LINE_WIDGET_HPP
#define VISUAL_AUTOMATA_LINE_WIDGET_HPP

#include <QWidget>
#include <QSpinBox>
#include <iostream>
#include <QHBoxLayout>

#include "line_style_widget.hpp"

class LineWidget: public QWidget {
Q_OBJECT
private:
	QHBoxLayout *lineLayout;
	LineStyleWidget *linePicker;
	QSpinBox *lineSizePicker;

public:
	explicit LineWidget(QWidget *parent);

	~LineWidget() override;

	LineStyleWidget *getLinePicker();

	QSpinBox *getLineSizePicker();
};

#endif // VISUAL_AUTOMATA_COLOR_PICKER_WIDGET_HPP
