#include "group_dock_widget.hpp"

GroupDockWidget::GroupDockWidget(QMainWindow *mainWindow) :
		RightDockWidget(mainWindow), mainWindow(mainWindow) {
	// Creating a bold Font for the subtitles
	QFont boldFont = QFont();
	boldFont.setPointSize(12);
	boldFont.setBold(true);

	// Creating 3 labels for the subtitles
	labelGroupCaracs = new QLabel("Informations sur le groupe", this);
	labelGroupCaracs->setFont(boldFont);
	labelGroupDrawing = new QLabel("Style du groupe pour le dessin", this);
	labelGroupDrawing->setFont(boldFont);
	labelDeleteGroup = new QLabel("Supprimer le groupe");
	labelDeleteGroup->setFont(boldFont);
	labelDeleteGroup->setMargin(20);

	// NameWidget
	labelName = new QLabel("Nom");
	labelName->setAlignment(Qt::AlignCenter);
	editName = new QLineEdit();
	namePairWidget = new PairLabelWidget(labelName, editName, this);

	// PriorityWidget
	labelPriority = new QLabel("Priorité");
	labelPriority->setAlignment(Qt::AlignCenter);
	prioritySpinBox = new QSpinBox();
	prioritySpinBox->setRange(-100, 100);
	priorityPairWidget = new PairLabelWidget(labelPriority, prioritySpinBox, this);


	// EditableWidget
	labelEditable = new QLabel("Éditable");
	labelEditable->setAlignment(Qt::AlignCenter);
	editable = new QCheckBox();
	editablePairWidget = new PairLabelWidget(labelEditable, editable, this);

	// ApplyStyleWidget
	labelApplyStyle = new QLabel("Appliquer style");
	labelApplyStyle->setAlignment(Qt::AlignCenter);
	applyStyle = new QCheckBox();
	applyStylePairWidget = new PairLabelWidget(labelApplyStyle, applyStyle, this);

	// PenWidget
	labelPen = new QLabel("Couleur du trait");
	labelPen->setAlignment(Qt::AlignCenter);
	ColorPickerPen = new ColorPickerWidget(this);
	penPairWidget = new PairLabelWidget(labelPen, ColorPickerPen, this);

	// PenBrush
	labelBrush = new QLabel("Couleur de fond");
	labelBrush->setAlignment(Qt::AlignCenter);
	ColorPickerBrush = new ColorPickerWidget(this);
	brushPairWidget = new PairLabelWidget(labelBrush, ColorPickerBrush, this);

	// Line Length
	labelLineLength = new QLabel("Style et épaisseur");
	labelLineLength->setAlignment(Qt::AlignCenter);
	styleSizePen = new LineWidget(this);
	styleSizePen->getLineSizePicker()->setValue(5);
	linePairWidget = new PairLabelWidget(labelLineLength, styleSizePen, this);

	// DeleteGroupButton
	deleteGroupButton = new QPushButton("Supprimer", this);
	deleteGroupButton->setFixedWidth(185);

	// Adding the widgets to the layout
	addWidget(*labelGroupCaracs);
	addWidget(*namePairWidget);
	addWidget(*priorityPairWidget);
	addWidget(*editablePairWidget);
	addWidget(*labelGroupDrawing);
	addWidget(*applyStylePairWidget);
	addWidget(*brushPairWidget);
	addWidget(*penPairWidget);
	addWidget(*linePairWidget);
	addWidget(*labelDeleteGroup);
	addWidget(*deleteGroupButton);
}

GroupDockWidget::~GroupDockWidget() {
	if (labelGroupCaracs != nullptr) {
		delete labelGroupCaracs;
		labelGroupCaracs = nullptr;
	}
	if (labelGroupDrawing != nullptr) {
		delete labelGroupDrawing;
		labelGroupDrawing = nullptr;
	}
	if (labelDeleteGroup != nullptr) {
		delete labelDeleteGroup;
		labelDeleteGroup = nullptr;
	}
	if (namePairWidget != nullptr) {
		delete namePairWidget;
		namePairWidget = nullptr;
	}
	if (priorityPairWidget != nullptr) {
		delete priorityPairWidget;
		priorityPairWidget = nullptr;
	}
	if (editablePairWidget != nullptr) {
		delete editablePairWidget;
		editablePairWidget = nullptr;
	}
	if (applyStylePairWidget != nullptr) {
		delete applyStylePairWidget;
		applyStylePairWidget = nullptr;
	}
	if (penPairWidget != nullptr) {
		delete penPairWidget;
		penPairWidget = nullptr;
	}
	if (brushPairWidget != nullptr) {
		delete brushPairWidget;
		brushPairWidget = nullptr;
	}
	if (linePairWidget != nullptr) {
		delete linePairWidget;
		linePairWidget = nullptr;
	}
	if (deleteGroupButton != nullptr) {
		delete deleteGroupButton;
		deleteGroupButton = nullptr;
	}
}

QLineEdit *GroupDockWidget::getNameWidget() {
	return editName;
}

void GroupDockWidget::setNameWidget(const std::string &name) {
	editName->setText(QString(name.c_str()));
}

QSpinBox *GroupDockWidget::getPrioritySpinBox() {
	return prioritySpinBox;
}

void GroupDockWidget::setPrioritySpinBox(int p) {
	prioritySpinBox->setValue(p);
}

QCheckBox *GroupDockWidget::getEditableCheckBox() {
	return editable;
}

void GroupDockWidget::setCheckEditable(bool b) {
	editable->setChecked(b);
}

QCheckBox *GroupDockWidget::getApplyStyleCheckBox() {
	return applyStyle;
}

void GroupDockWidget::setCheckApplyStyle(bool b) {
	applyStyle->setChecked(b);
}

ColorPickerWidget *GroupDockWidget::getColorPickerPen() {
	return ColorPickerPen;
}

void GroupDockWidget::setColorPickerPen(QColor color) {
	ColorPickerPen->setCurrentColor(std::move(color));
}

ColorPickerWidget *GroupDockWidget::getColorPickerBrush() {
	return ColorPickerBrush;
}

void GroupDockWidget::setColorPickerBrush(QColor color) {
	ColorPickerBrush->setCurrentColor(std::move(color));
}

LineWidget *GroupDockWidget::getStyleSizePen() {
	return styleSizePen;
}

void GroupDockWidget::setStylePen(Qt::PenStyle style) {
	styleSizePen->getLinePicker()->setFocusedStyle(style);
}

QPushButton *GroupDockWidget::getDeleteGroupButton() {
	return deleteGroupButton;
}

void GroupDockWidget::setSizePen(int s) {
	styleSizePen->getLineSizePicker()->setValue(s);
}
