set(EXEC visual_automata)

add_subdirectory(json)
add_subdirectory(utils)
add_subdirectory(model)
add_subdirectory(viewport)
add_subdirectory(graphical_window)
add_subdirectory(userdoc)
add_subdirectory(technicaldoc)


add_executable(${EXEC} automata_controller.cpp
               main_window.cpp
               automata_window.cpp
               Ressources.qrc)

target_include_directories(${EXEC} PRIVATE ${SRC}/model ${SRC}/viewport ${SRC}/graphical_window ${SRC}/json ${SRC}/utils)

target_link_libraries(${EXEC} model utils view window json Qt5::Widgets Qt5::Gui)

add_custom_command(TARGET ${EXEC}
                   POST_BUILD
                   COMMAND ${CMAKE_COMMAND} -E copy $<TARGET_FILE:${EXEC}> ${BUILD})
