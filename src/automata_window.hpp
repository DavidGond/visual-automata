#ifndef VISUAL_AUTOMATA_AUTOMATA_WINDOW_HPP
#define VISUAL_AUTOMATA_AUTOMATA_WINDOW_HPP

#include <QMainWindow>
#include <QTabWidget>
#include "automata_viewport.hpp"
#include "parser.hpp"

namespace VisualAutomata {

class AutomataWindow: public QMainWindow {
	AutomataViewport *automata_viewport_projection;
	AutomataViewport *automata_viewport_geometry;
	QTabWidget *tabWidget;
	std::unique_ptr<Automata> automaton;

public:
	explicit AutomataWindow(std::unique_ptr<Automata>);
	~AutomataWindow() override;
	AutomataViewport *getAutomataViewportProjection() const;
	AutomataViewport *getAutomataViewportGeometry() const;
	int getViewportNumber() const;
	Automata *getAutomata();
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_AUTOMATA_WINDOW_HPP
