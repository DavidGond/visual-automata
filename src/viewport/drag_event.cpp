#include "drag_event.hpp"

namespace VisualAutomata {

DragEvent::~DragEvent() {
	if (dragButton != nullptr) {
		delete (dragButton);
		dragButton = nullptr;
	}
}

void DragEvent::setValues(int addX, int addY, const DragButton &button) {
	this->dx = addX;
	this->dy = addY;
	this->dragButton = &button;
}

int DragEvent::getDeltaX() const {
	return dx;
}

int DragEvent::getDeltaY() const {
	return dy;
}

const DragButton *DragEvent::getSource() const {
	return dragButton;
}
}  // namespace VisualAutomata
