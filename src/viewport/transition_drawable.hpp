#ifndef VISUAL_AUTOMATA_TRANSITION_DRAWABLE_HPP
#define VISUAL_AUTOMATA_TRANSITION_DRAWABLE_HPP

#include <QPointF>
#include <QColor>

#include "transition.hpp"
#include "selectable.hpp"
#include "drawable.hpp"
#include "automata_drawable.hpp"
#include "drag_button.hpp"
#include "selectable_group.hpp"
#include "style.hpp"

namespace VisualAutomata {
class StateDrawable;

class AutomataDrawable;

/**
 * TransitionDrawable is the graphical representation of a Transition.
 * @see Transition
 */
class TransitionDrawable: public Drawable, public Draggable {

	/**< The drawing priority in the scene */
	static const int drawingPriority = 1;

	/**
	 * Method.
	 * Computes the binomial coefficient between k and n.
	 * @param k.
	 * @param n.
	 */
	static int binomialCoefficient(int n, int k);

private:
	/**< The size of the buttons associated with the TransitionDrawable*/
	static const int buttonSize = 10;

	/**< Transition linked with this object. */
	Transition *transition;

	/**< The control points of the TransitionDrawable. (By convention, the first and last points are the points of the center of the StateDrawables linked with the TransitionDrawable) */
	QVector<QPointF *> points;
	/**< The color of the TransitionDrawable. */
	QColor transitionColor;
	/**< StateDrawables linked with the TransitionDrawable */
	QVector<StateDrawable *> states;
	/**< The index of the destination in the vector of states */
	int destinationIndex{};
	/**< AutomataDrawable with which the TransitionDrawable is link to */
	AutomataDrawable *automataDrawable;

	/**< Angle of the arrow of the TransitionDrawable in radian */
	double arrowAngle;
	/**< Length of the arrow of the TransitionDrawable */
	float arrowLength;

	/**< The scene is which the TransitionDrawable is drawn. */
	QGraphicsScene *scene{};
	/**< The lines which create the bezier curb. */
	QVector<QGraphicsLineItem *> lines;
	/**< The lines which create the arrow. */
	QVector<QGraphicsLineItem *> arrowLines;
	/**< The dot lines from the curve to the control point */
	QVector<QGraphicsLineItem *> dotLines;
	/**< The text representing the weight of the transition. */
	QGraphicsTextItem *weightText;
	/**< The integer representing the local accuracy of the transition. Accuracy is a part of the style, this is saved in the json. */
	int localAccuracy;

	/**< If it's an initial TransitionDrawable */
	bool isInitial;
	/**< If the TransitionDrawable is initial this point is the control point at the beginning of the TransitionDrawable (not the point in the center of the StateDrawable) */
	QPointF *initialPoint{};
	std::string getText();

public:
	/**
	 * Constructor.
	 * @param points are the control points of the TransitionDrawable
	 * @param transition is the Transition associated with the TransitionDrawable
	 * @param automataDrawable is the AutomataDrawable in which the Transition is
	 * @see Transition
	 */
	TransitionDrawable(QVector<QPointF *> &points,
	                   Transition *transition,
	                   AutomataDrawable &automataDrawable);

	/**
	 * Destructor.
	 */
	~TransitionDrawable() override;

	/**
	 * An integer used to choose the number of points for the drawing of the curve.
	 * The higher this int is, the more points there are (performance decrease while the number of points increase).
	 * The number of points depends also on the length of the curve.
	 */
	static int preferenceAccuracy;

	/**
	 * Method.
	 * Returns the coordinate of a point on the Bezier curve
	 * @param t the parameter of the point, t must be in [0,1]
	 * @return the point on the bezier curve with the parameter t
	 */
	QPointF getCoordinate(double t) const;

	/**
	 * Method.
	 * Returns the coordinates of the text which designed the weight of the TransitionDrawable.
	 * @return the coordinates of the text which designed the weight of the TransitionDrawable
	 */
	QPointF getWeightTextCoordinate() const;

	/**
	 * Method.
	 * Returns the number of control points of the TransitionDrawable.
	 * @return the number of control points of the TransitionDrawable
	 */
	int getNumPoints() const;

	/**
	 * Method.
	 * Adds a StateDrawable to the vector of linked StateDrawables.
	 * @param state is the StateDrawable to link with the TransitionDrawable
	 * @see StateDrawable
	 */
	void addLinkedState(StateDrawable &state, bool isDestination = false);

	/**
	 * Method
	 * Unlink the TransitionDrawable with all the StateDrawables linked with it.
	 * @see StateDrawable
	 */
	void unlink();

	/**
	 * Method.
	 * Returns the QPoint at the index i in the QVector of control points.
	 * @return the QPoint at the index i in the QVector of control points
	 * @param i is the index of the QPoint in the QVector of control points
	 */
	QPointF &getPoint(int i) const;

	/**
	 * Method.
	 * Returns the transition associated with the TransitionDrawable.
   * @return the transition associated with the TransitionDrawable
	 */
	Transition *getData() const;

	/**
	 * Method
	 * Returns an approximation of the length of the curve in pixels.
	 * This approximates is computed using the distance between the control points
	 * @return the approximated length of the curve
	 */
	int getLength();

	/**
	 * Method.
	 * Moves the TransitionDrawable according to the DragEvent
	 * @param dragEvent stores information about the movement
   * @see DragEvent
	 */
	void move(DragEvent &dragEvent) override;

	/**
     * Method.
     * Increases the position of all the points of the TransitionDrawable with dx and dy.
     * @param dx is the change on the x coordinate.
     * @param dy is the change on the y coordinate.
     */
	void moveAllTransition(int dx, int dy);

	/**
	 * Method.
	 * Increases the position of the control points of the transition with dx and dy. The points which are linked with states don't move.
	 * @param dx is the change on the x coordinate.
	 * @param dy is the change on the y coordinate.
	 */
	void directedMove(int dx, int dy);

	/**
	 * Method.
	 * Increases only the position of the control point linked with the button source with dx and dy.
	 * @param dx is the change on the x coordinate.
	 * @param dy is the change on the y coordinate.
	 * @see DragButton
	 */
	void directedMove(int dx, int dy, const DragButton *buttonSource);

	/**
	 * Method.
	 * Changes the position of the control points of the TransitionDrawable according to the position of the linked StateDrawables. This function is used to move TransitionDrawable which have the same source and destination StateDrawable.
	 * @param dx is the change on the x coordinate.
	 * @param dy is the change on the y coordinate.
	 * @param buttonSource is the DragButton which initiates the movement
	 * @see DragButton
	 * @see StateDrawable
	 *
	 */
	void moveRelativelyToStates(int dx, int dy, const DragButton &source);

	/**
	 * Method.
	 * Changes the position of the control points according to the position of the linked StateDrawables. This function is used when a StateDrawable is moved and TransitionDrawables must be moved accordingly
	 * @param dx is the change on the x coordinate.
	 * @param dy is the change on the y coordinate.
	 * @param source is the DragButton of the StateDrawable which initiates the movement
	 * @see DragButton
	 * @see StateDrawable
	 */
	void moveRelativelyToStates(int dx, int dy, StateDrawable &source);

	/**
	 * Method.
	 * Changes the positions of an initial TransitionDrawable.
	 * @param dx is the change on the x coordinate.
	 * @param dy is the change on the y coordinate.
	 * @param source is the DragButton of the StateDrawable which initiates the movement
	 * @see DragButton
	 * @see StateDrawable
	 */
	void moveInitialTransition(int dx, int dy, const DragButton &source);

	/**
	 * Method.
	 * Updates the position and the characteristics of the TransitionDrawable.
	 */
	void update() override;

	/**
	 * Method.
	   * Updates the position and the characteristics of the arrows of the TransitionDrawable.
	 */
	void updateArrow();

	/**
	 * Method.
	   * Updates the position and the characteristics of the dot lines of the TransitionDrawable.
	 */
	void updateDotLines();

	/**
	 * Method.
	   * Updates the text which represents the weight of the TransitionDrawable.
	 */
	void updateText();

	/**
	 * Method.
	 * Returns the distance between two points.
	 * @param p1 is the source point
	 * @param p2 is the destination point
	 * @return the distance between two points
	 */
	static double distance(QPointF p1, QPointF p2);

	/**
	 * Method.
	 * Returns the Pen chosen for drawing the TransitionDrawable lines.
	 * If there is no local Pen returns the SelectableGroup Pen with the higher priority, and if there is no group, returns the preference Pen.
	 * @return the Pen chosen for drawing the TransitionDrawable
	 */
	QPen *getChosenPen() const override;

	/**
	 * Method.
	 * Returns the pen chosen for the background of the TransitionDrawable
	 * If there is no local Brush returns the SelectableGroup Brush with the higher priority, and if there is no group, returns the preference Brush.
	 * @return the Pen chosen for drawing the TransitionDrawable
	 */
	QBrush *getChosenBrush() const override;

	/**
	 * Method.
	 * Returns the accuracy of the TransitionDrawable.
	 * @return the accuracy of the TransitionDrawable
	 */
	int getChosenAccuracy() const;

	/**
	 * Method.
	 * Attaches the TransitionDrawable to a SelectableGroup.
	 * @param group is the SelectableGroup in which the TransitionDrawable will be attached
	 * @see SelectableGroup
	 */
	void addAttachedGroup(SelectableGroup &group) override;

	/**
	 * Method.
	 * Removes the TransitionDrawable from a SelectableGroup.
	 * @param group is the SelectableGroup from which the TransitionDrawable will be removed
	 * @see SelectableGroup
	 */
	void removeAttachedGroup(SelectableGroup &group) override;

	/**
	 * Method.
	 * Changes the local accuracy of the TransitionDrawable.
	 * @param accuracy is the new accuracy
	 */
	void setLocalAccuracy(int accuracy);

	/**
	 * Virtual Method.
	 * Returns if the TransitionDrawable has a local Style or not.
	 * @return true if the TransitionDrawable has a local Style, false otherwise
	 */
	bool getHasLocalStyle() const override;

	/**
	 * Method.
	 * Initialises and draw the components of the TransitionDrawable.
	 * @param newScene is the scene in which components are drawn
	 */
	void setScene(QGraphicsScene &newScene);

	/**
	 * Method.
	 * Calls the buttonMoved function of the linked AutomataDrawable and its update function.
	 * @param event stores information about the movement
	 */
	void buttonMoved(DragEvent &event) override;

	/**
	 * Method.
	 * Notifies the AutomataDrawable linked that this element is selected.
	 * @see AutomataDrawable
	 */
	void buttonSelected() override;

	/**
   * Method.
   * Returns the number of lines used to draw the curve according to the accuracy and the length of the curve.
	 * @return the number of lines used to draw the curve according to the accuracy and the length of the curve
	 */
	int computesPointsNumber();

	/** 
	 * Method.
	 * Removes the TransitionDrawable from the AutomataScene and the Automata. The object is destruct
	 * @see AutomataScene
	 * @see Automata
	 */
	void removeFromAutomata();

	/**
	 * Method.
	 * Sets if the TransitionDrawable is an initial TransitionDrawable or not.
	 * @param is isInitial defines the state of the TransitionDrawable
	 */
	void setIsInitial(bool initial);

	/**
	 * Method.
	 * Returns if the TransitionDrawable is an initial TransitionDrawable or not.
	 * @returns if the TransitionDrawable is an initial TransitionDrawable or not
	 */
	bool getIsInitial() const;

	/**
	 * Method.
	 * If the TransitionDrawable is an initial TransitionDrawable, returns its only control point.
	 * @return the control point of the initial TransitionDrawable
	 */
	QPointF &getInitialPoint() const;

	/**
	 * Virtual Method
	 * Makes the transition visible in the scene.
	 */
	void addToScene() override;

	/**
	 * Virtual Methods
	 * Makes the transition invisible in the scene.
	 */
	void removeFromScene() override;

	/**
	 * Returns StateDrawables linked with the TransitionDrawable.
	 * @return StateDrawables linked with the TransitionDrawable
	 */
	QVector<StateDrawable *> &getStatesDrawable();

	/**
	 * Method.
	 * Changes the color of the preference Pen.
	 * @param color is the new color of the preference Pen
	 */
	static void setPreferencePenColor(const QColor &color);

	/**
	 * Method.
	 * Changes the size of the preference Pen.
	 * @param size is the new size of the preference Pen
	 */
	static void setPreferencePenSize(int size);

	/**
	 * Method.
	 * Changes the PenStyle of the preference Pen.
	 * @param style is the new style of the preference Pen
	 */
	static void setPreferencePenStyle(Qt::PenStyle &style);

	/** 
	 * Method.
	 * Updates the local style of the transition (Line Color, Accuracy, Line Style, Line Width and positions of the control points)
	 * It is called when a property of the transition is changed in the AutomataController.
	 * @see AutomataController
	 */
	void updateLocalStyle() override;

	/**
	 * Method.
	 * Loads the local style of the transition (Line Color, Accuracy, Line Style, Line Width and positions of the control points)
	 * It is called is in the constructor of the TransitionDrawable
	 */
	void loadLocalStyle() override;

private:
	// The pen is defined in the preference window
	/**< Default Pen shared by all TransitionDrawables */
	static QPen *preferencePen;
};
}  // namespace VisualAutomata
#endif //VISUAL_AUTOMATA_TRANSITION_DRAWABLE_HPP
