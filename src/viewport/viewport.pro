TEMPLATE = lib

INCLUDEPATH += . ../model/ \
                ../json/

HEADERS += \
    automata_scene.hpp \
    drag_button.hpp \
    drag_event.hpp \
    dragable.hpp \
    drawable.hpp \
    moveable.hpp \
    state_drawable.hpp \
    automata_drawable.hpp \
    transition_drawable.hpp \
    viewport_action.hpp \
    selectable_group.hpp \
    groupable.hpp \
    
QT += widgets


SOURCES += \
    automata_scene.cpp \
    automata_drawable.cpp \
    automata_viewport.cpp \
    drag_button.cpp \
    drag_event.cpp \
    dragable.cpp \
    drawable.cpp \
    moveable.cpp \
    state_drawable.cpp \
    transition_drawable.cpp \
    selectable_group.cpp \
    groupable.cpp \
