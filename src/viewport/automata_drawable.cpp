#include <QPointF>
#include <cmath>
#include "automata_drawable.hpp"
#include "automata_scene.hpp"

#define PI 3.141592
namespace VisualAutomata {

SelectableGroup *AutomataDrawable::selectedItems;

AutomataDrawable::AutomataDrawable(QGraphicsScene &scene) :
		previewName("a"),
		previewSource(nullptr),
		previewDestination(nullptr),
		emptySelection(0, "selection"),
		scene(&scene),
		automata(nullptr) {
	selectedItems = &emptySelection;
}

AutomataDrawable::~AutomataDrawable() {
	while (!states.empty()) {
		StateDrawable *s = states.last();
		states.pop_back();
		if (s != nullptr) {
			delete (s);
		}
	}

	while (!transitions.empty()) {
		TransitionDrawable *t = transitions.last();
		transitions.pop_back();
		if (t != nullptr) {
			delete (t);
		}
	}
}

void AutomataDrawable::update() {
	for (auto &transition : transitions) {
		transition->update();
	}
	for (auto &state : states) {
		state->update();
	}
}

void AutomataDrawable::addToScene() {
	for (auto &transition : transitions) {
		if (!transition->getIsVisibleInScene()) {
			transition->addToScene();
		}
	}
	for (auto &state : states) {
		if (!state->getIsVisibleInScene()) {
			state->addToScene();
		}
	}

	Drawable::addToScene();
}

void AutomataDrawable::removeFromScene() {
	for (auto &transition : transitions) {
		if (transition->getIsVisibleInScene()) {
			transition->removeFromScene();
		}
	}
	for (auto &state : states) {
		if (state->getIsVisibleInScene()) {
			state->removeFromScene();
		}
	}

	Drawable::removeFromScene();
}

void AutomataDrawable::setAutomata(Automata *newAutomata) {
	this->automata = newAutomata;
	// Creating States
	auto *center = new QPointF(100, 100);
	float radius = 50;

	auto mapState = newAutomata->getStates();
	int i = 0;
	double x, y;
	for (auto &it : mapState) {
		double j = sqrt(i);
		if (i - j * j < j) {
			x = j;
			y = i - j * j;
		} else {
			x = j - ((j + 1) * (j + 1) - 1 - i);
			y = j;
		}
		addStateToScene(*new QPointF(center->x() * 2 * (x + 1), center->y() * 2 * (y + 1)),
		                radius,
		                *it.second);
		i++;
	}

	// Creating Transitions
	bool flag = true;
	auto mapTransition = newAutomata->getTransitions();
	QVector < QPointF * > vec;
	for (auto &it : mapTransition) {
		addTransitionToScene(vec, flag, *it.second);
		flag = !flag;
	}

	// Creating Groups
	auto mapGroup = newAutomata->getGroups();
	for (auto &it1 : mapGroup) {
		auto *sg = new SelectableGroup(*it1.second);

		auto mapStateInGroup = it1.second->getStates();
		for (auto &it2 : mapStateInGroup) {
			sg->addItem(*stateDictionary[it2]);
		}
		selectableGroups.push_back(sg);
	}

	update();
}

void AutomataDrawable::setAutomataLinear(VisualAutomata::Automata *oldAutomata) {
	this->automata = oldAutomata;

	auto mapState = oldAutomata->getStates();
	auto mapTransition = oldAutomata->getTransitions();
	vector<int> statesPlace;

	auto firstElement = mapState.begin();
	statesPlace.push_back((*firstElement).second->getId());
	float radius = 50;
	addStateToScene(*new QPointF(100, 100), radius, (*(*firstElement).second));
	mapState.erase(firstElement->first);

	size_t distMax;
	size_t currentDist;
	int idStateMax;
	State *stateMax;
	int nbPlaced = 1;
	int pos;
	while (!mapState.empty()) {
		idStateMax = (*mapState.begin()).first;
		stateMax = (*mapState.begin()).second;
		distMax = 0;
		for (auto &itState : mapState) {
			currentDist = 0;
			for (auto &itTransition : mapTransition) {
				pos = 0;
				for (auto &placeId : statesPlace) {
					//if there is a transition between itState and placeId, add the distance of this transition to the total distance of itState
					if ((itTransition.second->getSource().getId() == itState.second->getId()
							&& itTransition.second->getDestination().getId() == placeId)
							|| (itTransition.second->getDestination().getId()
									== itState.second->getId()
									&& itTransition.second->getSource().getId() == placeId)) {
						currentDist += statesPlace.size() - pos;
					}
					pos++;
				}
			}
			if (currentDist > distMax) {
				distMax = currentDist;
				stateMax = itState.second;
				idStateMax = itState.first;
			}
		}
		statesPlace.push_back(idStateMax);
		addStateToScene(*new QPointF(100 + 150 * nbPlaced, 100), radius, *stateMax);
		nbPlaced++;
		mapState.erase(idStateMax);
	}

	bool flag = true;
	QVector < QPointF * > vec;
	for (auto &it : mapTransition) {
		addTransitionToScene(vec, flag, *it.second);
		flag = !flag;
	}

	// Creating Groups
	auto mapGroup = oldAutomata->getGroups();
	for (auto &it1 : mapGroup) {
		auto *sg = new SelectableGroup(*it1.second);

		auto mapStateInGroup = it1.second->getStates();
		for (auto &it2 : mapStateInGroup) {
			sg->addItem(*stateDictionary[it2]);
		}
		selectableGroups.push_back(sg);
	}
}

void AutomataDrawable::setAutomataCircular(VisualAutomata::Automata *oldAutomata) {
	this->automata = oldAutomata;

	auto mapState = oldAutomata->getStates();
	auto mapTransition = oldAutomata->getTransitions();

	size_t nbState = mapState.size();
	float rCircle = (float) nbState * 33;
	float radius = 50;
	float centerX = 100 + rCircle;
	float centerY = 100 + rCircle;

	vector<int> statesPlace;

	auto firstElement = mapState.begin();
	statesPlace.push_back((*firstElement).second->getId());

	addStateToScene(*new QPointF(centerX + rCircle * cos(PI / 2), centerY + rCircle * sin(PI / 2)),
	                radius,
	                (*(*firstElement).second));
	mapState.erase(firstElement->first);

	size_t distMax;
	size_t currentDist;
	int idStateMax;
	State *stateMax;
	int nbPlaced = 1;
	int pos;
	while (!mapState.empty()) {
		idStateMax = (*mapState.begin()).first;
		stateMax = (*mapState.begin()).second;
		distMax = 0;
		for (auto &itState : mapState) {
			currentDist = 0;
			for (auto &itTransition : mapTransition) {
				pos = 0;
				for (auto &placeId : statesPlace) {
					//if there is a transition between itState and placeId, add the distance of this transition to the total distance of itState
					if ((itTransition.second->getSource().getId() == itState.second->getId()
							&& itTransition.second->getDestination().getId() == placeId)
							|| (itTransition.second->getDestination().getId()
									== itState.second->getId()
									&& itTransition.second->getSource().getId() == placeId)) {
						currentDist += statesPlace.size() - pos;
					}
					pos++;
				}
			}
			if (currentDist > distMax) {
				distMax = currentDist;
				stateMax = itState.second;
				idStateMax = itState.first;
			}
		}
		statesPlace.push_back(idStateMax);
		addStateToScene(*new QPointF(centerX + rCircle * cos(PI / 2 + 2 * PI * nbPlaced / nbState),
		                             centerY + rCircle * sin(PI / 2 + 2 * PI * nbPlaced / nbState)),
		                radius,
		                *stateMax);
		nbPlaced++;
		mapState.erase(idStateMax);
	}

	bool flag = true;
	QVector<QPointF *> vec;
	for (auto &it : mapTransition) {
		addTransitionToScene(vec, flag, *it.second);
		flag = !flag;
	}


	// Creating Groups
	auto mapGroup = oldAutomata->getGroups();
	for (auto &it1 : mapGroup) {
		auto *sg = new SelectableGroup(*it1.second);

		auto mapStateInGroup = it1.second->getStates();
		for (auto &it2 : mapStateInGroup) {
			sg->addItem(*stateDictionary[it2]);
		}
		selectableGroups.push_back(sg);
	}
}

void AutomataDrawable::addStateToAutomata(State &state) {
	automata->addState(state);
}

Automata *AutomataDrawable::getAutomata() const {
	return automata;
}

void AutomataDrawable::addTransitionToAutomata(Transition &transition) {
	automata->addTransition(transition);
}

StateDrawable *AutomataDrawable::addStateToScene(QPointF &center, float radius, State &state) {
	if (stateDictionary.find(&state) == stateDictionary.end()) {
		auto stateDrawable = new StateDrawable(center, radius, state, *this);

		stateDictionary.emplace(&state, stateDrawable);
		states.push_back(stateDrawable);
		stateDrawable->setScene(*scene);
		return stateDrawable;
	}
	return nullptr;
}

void AutomataDrawable::removeStateFromAutomata(StateDrawable &stateDrawable) {
	auto s = stateDrawable.getData();
	stateDrawable.deleteTransitionsLinked();
	stateDictionary.erase(&s);
	int i = states.indexOf(&stateDrawable);
	states.remove(i);
	automata->delState(s);
	stateDrawable.removeFromAutomata();
}

void AutomataDrawable::setScene(QGraphicsScene &newScene) {
	this->scene = &newScene;
}

TransitionDrawable *AutomataDrawable::addInitialTransitionToScene(QPointF *controlPoint,
                                                                  StateDrawable &state) {
	QVector < QPointF * > points(0);
	points.append(&state.getCenter());
	if (controlPoint == nullptr) {
		points.append(new QPointF(state.getCenter().x() - 2 * state.getRadius(),
		                          state.getCenter().y()));
	} else {
		points.append(controlPoint);
	}
	auto transitionDrawable = new TransitionDrawable(points, nullptr, *this);

	transitions.push_back(transitionDrawable);

	// Adds links between TransitionDrawable and StateDrawables

	transitionDrawable->setIsInitial(true);
	transitionDrawable->setScene(*scene);
	transitionDrawable->addLinkedState(state);
	state.linkWith(*transitionDrawable);
	return transitionDrawable;
}

TransitionDrawable *AutomataDrawable::addTransitionToScene(QVector<QPointF *> &controlPoints,
                                                           bool up,
                                                           Transition &transition) {
	int direction = 1;
	if (!up) {
		direction = -1;
	}

	if (transitionDictionary.find(&transition) == transitionDictionary.end()) {
		StateDrawable *s1 = stateDictionary[&transition.getSource()];
		StateDrawable *s2 = stateDictionary[&transition.getDestination()];

		QVector < QPointF * > points(0);
		QPointF &center1 = s1->getCenter();

		if (s1 != s2) {
			QPointF &center2 = s2->getCenter();
			int midX1, midY1;
			midX1 = (int) round((center1.x() + center2.x()) / 2.);
			midY1 = (int) round((center1.y() + center2.y()) / 2.);
			if (abs(center1.x() - center2.x()) < abs(center1.y() - center2.y())) {
				midX1 += direction * (int) round(s1->getRadius());
			} else {
				midY1 += direction * (int) round(s1->getRadius());
			}
			points.append(&s1->getCenter());
			if (controlPoints.empty()) {
				points.append(new QPointF(midX1, midY1));
			} else {
				for (QPointF *p : controlPoints) {
					points.append(p);
				}
			}
			points.append(&s2->getCenter());
		} else {
			points.append(&s1->getCenter());
			if (controlPoints.empty()) {
				points.append(new QPointF(center1.x() + s1->getRadius(),
				                          center1.y() + 3 * s1->getRadius()));
				points.append(new QPointF(center1.x() - s1->getRadius(),
				                          center1.y() + 3 * s1->getRadius()));
			} else {
				for (QPointF *p : controlPoints) {
					points.append(p);
				}
			}
			points.append(&s1->getCenter());
		}

		auto transitionDrawable = new TransitionDrawable(points, &transition, *this);

		transitionDictionary.emplace(&transition, transitionDrawable);
		transitions.push_back(transitionDrawable);

		// Adds links between TransitionDrawable and StateDrawables
		transitionDrawable->addLinkedState(*s1);
		if (s1 != s2) {
			transitionDrawable->addLinkedState(*s2, true);
		}

		s1->linkWith(*transitionDrawable);
		if (s1 != s2) {
			s2->linkWith(*transitionDrawable);
		}

		transitionDrawable->setScene(*scene);

		return transitionDrawable;
	}
	return nullptr;
}

void AutomataDrawable::removeTransitionFromAutomata(TransitionDrawable &transitionDrawable) {
	Transition &t = *transitionDrawable.getData();
	transitionDictionary.erase(&t);
	int i = transitions.indexOf(&transitionDrawable);
	transitions.remove(i);
	if (!transitionDrawable.getIsInitial()) {
		automata->delTransition(t);
	}
	transitionDrawable.removeFromAutomata();
}

void AutomataDrawable::addLeftTransition() {
	auto *t = new Transition(previewSource->getData(), previewDestination->getData(), previewName);
	addTransitionToAutomata(*t);
	addTransitionToScene(previewControlPoints, true, *t);

	endTransitionPreview();
}

void AutomataDrawable::setPreviewName(std::string name) {
	previewName = std::move(name);
}

void AutomataDrawable::setPreviewSource(StateDrawable &s) {
	previewSource = &s;
}

void AutomataDrawable::setPreviewDestination(StateDrawable &s) {
	previewDestination = &s;
}

bool AutomataDrawable::isDefinePreviewSource() {
	return previewSource != nullptr;
}

bool AutomataDrawable::isDefinePreviewDestination() {
	return previewDestination != nullptr;
}

void AutomataDrawable::endTransitionPreview() {
	// Reinitialize preview Points
	previewSource = nullptr;
	previewDestination = nullptr;
	previewControlPoints.clear();
}

bool AutomataDrawable::contains(const QRectF &selectionRect, const StateDrawable &state) {
	for (int i = 0; i < state.getNbDragButton(); i++) {
		if (selectionRect.contains(state.getDragButtonHitBox(i))) {
			return true;
		}
	}
	return false;
}

void AutomataDrawable::emptySelectedItems() {
	int n = selectedItems->getNumItems();
	for (int i = 0; i < n; i++) {
		Groupable *item = &selectedItems->getItem(i);
		item->setSelected(false);
	}

	if (selectedItems != &emptySelection) {
		selectedItems = &emptySelection;
	} else {
		selectedItems->removeAll();
	}

	emit selectedItemEmpty();
}

void AutomataDrawable::setSelectedItem(const QRectF &selectionRect) {
	if (!AutomataScene::controlKey && !selectedItems->isEditable()) {
		emptySelectedItems();
	}

	for (auto &state : states) {
		if (contains(selectionRect, *state)) {
			if (!selectedItems->contains(*state)) {
				state->setSelected(true);
				selectedItems->addItem(*state);
			} else {
				state->setSelected(false);
				selectedItems->removeItem(*state);
			}
			emit selectedItemChanged(*state);
		}
	}

	if (selectedItems->getNumItems() == 0 || selectedItems->getNumItems() > 1) {
		emit selectedItemEmpty();
	}
	update();
}

void AutomataDrawable::setSelectedItem(Groupable &item) {
	if (!AutomataScene::controlKey && !selectedItems->isEditable()) {
		emptySelectedItems();
	}

	if (!selectedItems->contains(item)) {
		item.setSelected(true);
		selectedItems->addItem(item);
	} else {
		item.setSelected(false);
		selectedItems->removeItem(item);
	}

	// TO DO: When several items are selected adds the possibility to change the style of all items
	if (selectedItems->getNumItems() == 0 || selectedItems->getNumItems() > 1)
		emit{
				selectedItemEmpty();
		}
	else
		emit{
				selectedItemChanged(item);
		}
	update();
}

void AutomataDrawable::createGroupWithSelectedItems() {
	if (selectedItems->getNumItems() == 0) {
		return;
	}

	static int groupId = 0;
	std::string name = "groupe " + std::to_string(groupId);
	groupId++;

	auto *sg = new SelectableGroup(0, name);

	for (int i = 0; i < selectedItems->getNumItems(); i++) {
		sg->addItem(selectedItems->getItem(i));
	}

	automata->addGroup(sg->getStateGroup());

	selectableGroups.push_back(sg);
	selectedItems = sg;

	emptySelection.removeAll();

	update();
}

void AutomataDrawable::delSelectedGroup() {
	if (selectedItems == &emptySelection) {
		return;
	}

	SelectableGroup *sg = selectedItems;
	emptySelectedItems();
	automata->delGroup(sg->getStateGroup());
	sg->removeAll();
	selectableGroups.removeAll(sg);
	delete (sg);
	update();
}

void AutomataDrawable::loadGroup(int groupNumber) {
	SelectableGroup *sg = selectableGroups[groupNumber];

	for (int i = 0; i < selectedItems->getNumItems(); i++) {
		selectedItems->getItem(i).setSelected(false);
	}

	auto items = sg->getItems();
	for (Groupable *item : items) {
		item->setSelected(true);
	}
	selectedItems = sg;
	update();
}

Groupable *AutomataDrawable::getSelectedItem() {
	if (selectedItems->getNumItems() > 0) {
		return &selectedItems->getItem(0);
	}
	return nullptr;
}

SelectableGroup *AutomataDrawable::getSelectedItems() {
	return selectedItems;
}

int AutomataDrawable::getNumSelectedItems() {
	return selectedItems->getNumItems();
}

int AutomataDrawable::getNumSelectableGroups() const {
	return selectableGroups.size();
}

SelectableGroup *AutomataDrawable::getCurrentSelectableGroup() {
	if (selectedItems == &emptySelection) {
		return nullptr;
	}
	return selectedItems;
}

SelectableGroup *AutomataDrawable::getSelectableGroup(int index) {
	return selectableGroups[index];
}

void AutomataDrawable::buttonMoved(DragEvent &event) {
	for (int i = 0; i < selectedItems->getNumItems(); i++) {
		selectedItems->getItem(i).move(event);
	}
}

QPen *AutomataDrawable::getChosenPen() const {
	return nullptr;
}

QBrush *AutomataDrawable::getChosenBrush() const {
	return nullptr;
}

void AutomataDrawable::updateLocalStyle() {
}

void AutomataDrawable::loadLocalStyle() {
}
}  // namespace VisualAutomata
