#ifndef VISUAL_AUTOMATA_DRAGGABLE_HPP
#define VISUAL_AUTOMATA_DRAGGABLE_HPP

#include <QVector>
#include <QGraphicsView>
#include <QGraphicsProxyWidget>

#include "groupable.hpp"
#include "drag_event.hpp"
#include "drag_button.hpp"

namespace VisualAutomata {
class DragEvent;

class DragButton;

/**
 * Describes an object that can be moved with a DragButton
 * @see DragButton
 */
class Draggable: public Groupable {

	/**< The drawing priority in the scene */
	static const int drawingPriority = 8;

protected:
	/**< List of all DragButtons linked to the object */
	QVector<DragButton *> dragButtons;
	/**< List of all graphics items which represent DragButton */
	QVector<QGraphicsProxyWidget *> graphicsItems;

private:
	/**< If DragButtons are visible in the scene */
	bool areDragButtonsVisibleInScene;

public:
	/**
	 * Explicit Constructor.
	 * @param type is the SelectableType of the object
	 * @see SelectableType
	 */
	explicit Draggable(enum SelectableType type);

	/**
	 * Virtual Destructor.
	 */
	~Draggable() override;

	/**
	 * Method.
	 * Adds the DragButton to the list of dragButton linked with the object.
	 * @param dragButton is the DragButton to add to the list.
	 * @see DragButton
	 */
	void addDragButton(DragButton &dragButton);

	/**
     * Method.
     * Deletes all DragButtons from the list of DragButtons linked with the object.
     * @param scene is the scene from which DragButton are deleted.
	 * @see DragButton
     */
	void removeDragButtonsFromAutomata(QGraphicsScene &scene);

	/**
	 * Method.
	 * Adds DragButtons to the scene if they are set visible.
	 * @param scene is the scene is which the DragButtons are added
	 * @see DragButton
	 */
	void addDragButtonsToScene(QGraphicsScene &scene);

	/**
	 * Method.
	 * Removes DragButtons from the scene if they are set invisible.
	 * @param scene is the scene is which the DragButtons are added
	 * @see DragButton
	 */
	void removeDragButtonsFromScene(QGraphicsScene &scene);

	/**
	 * Method.
	 * Returns if the DragButtons are visible in the scene
	 * @return if the DragButtons are visible
	 * @see DragButton
	 */
	bool getAreDragButtonsVisibleInScene() const;

	/**
	 * Method.
	 * Returns the DragButton with the index i in the vector of DragButtons.
	 * @param i is the index of the dragButton in the vector.
	 * @see DragButton
	 */
	DragButton *getDragButton(int i) const;

	/** 
	 * Method.
	 * Returns the hitbox of the DragButton with the index i in the list
	 * @param i is the index of the DragButton
	 * @return the hitbox of the DragButton
	 * @see DragButton
	 */
	QRectF getDragButtonHitBox(int i) const;

	/**
	 * Method.
	 * Returns the number of DragButton in the vector.
	 * @return the number of DragButton
	 */
	int getNbDragButton() const;

	/**
	 * Method.
	 * Increments the position of all DragButtons linked with the object by x and y
	 * @param x is the abscissa coordinate
	 * @param y is the orderly coordinate
	 * @see DragButton
	 */
	void moveDragButtons(int dx, int dy);

	/**
	 * Virtual Method.
	 * When a DragButton moved, this function is called
	 */
	virtual void buttonMoved(DragEvent &event) = 0;
};
}  // namespace VisualAutomata

#endif
