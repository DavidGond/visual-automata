#include <QColor>
#include <QDebug>
#include "selectable_group.hpp"
#include "state_drawable.hpp"

namespace VisualAutomata {

SelectableGroup::SelectableGroup(int priority, const std::string &name) :
		name(name),
		priority(priority),
		editable(false),
		apply(false),
		pen(new QPen(Qt::black, 5)),
		brush(new QBrush(Qt::white, Qt::SolidPattern)) {
	stateGroup = new StateGroup();
	stateGroup->setLabel(name);
}

SelectableGroup::SelectableGroup(StateGroup &stateGroup) {
	this->name = stateGroup.getLabel();

	// Sets default values
	this->priority = 0;
	this->editable = false;
	this->apply = false;

	this->pen = new QPen(Qt::black, 5);
	this->brush = new QBrush(Qt::white, Qt::SolidPattern);

	// Load the group style if it exists
	Style &style = stateGroup.getStyle();

	this->priority = style.loadInt(Style::groupPriorityAttribute, this->priority);
	this->apply = style.loadBool(Style::groupApplyStyleAttribute, this->apply);

	style.loadPen(*this->pen);
	style.loadBrush(*this->brush);

	this->stateGroup = &stateGroup;
}

void SelectableGroup::addItem(Groupable &item) {
	if (!groupItems.contains(&item)) {
		item.addAttachedGroup(*this);
		groupItems.push_back(&item);
		if (item.getType() == STATE) {
			stateGroup->addState(((StateDrawable &) item).getData());
		}
	}
}

void SelectableGroup::addItems(const QVector<Groupable *> &items) {
	for (Groupable *item : items) {
		addItem(*item);
	}
}

void SelectableGroup::removeItem(Groupable &item) {
	if (groupItems.contains(&item)) {
		item.removeAttachedGroup(*this);
		groupItems.removeAll(&item);
		if (item.getType() == STATE) {
			stateGroup->delState(((StateDrawable &) item).getData());
		}
	}
}

void SelectableGroup::removeItems(const QVector<Groupable *> &items) {
	for (Groupable *item : items) {
		removeItem(*item);
	}
}

void SelectableGroup::removeAll() {
	int n = getNumItems();
	for (int i = n - 1; i >= 0; i--) {
		removeItem(getItem(i));
	}
}

bool SelectableGroup::contains(Groupable &item) {
	for (Groupable *g : groupItems) {
		if (g == &item) {
			return true;
		}
	}
	return false;
}

Groupable &SelectableGroup::getItem(int i) {
	return *groupItems[i];
}

QVector<Groupable *> SelectableGroup::getItems() {
	return groupItems;
}

int SelectableGroup::getNumItems() const {
	return groupItems.size();
}

void SelectableGroup::setName(const std::string &newName) {
	this->name = newName;
	stateGroup->setLabel(newName);
}

std::string SelectableGroup::getName() const {
	return name;
}

void SelectableGroup::changePriority(int newPriority) {
	this->priority = newPriority;
	stateGroup->getStyle().saveInt(Style::groupPriorityAttribute, this->priority);
}

int SelectableGroup::getPriority() const {
	return priority;
}

void SelectableGroup::setEditable(bool state) {
	this->editable = state;
}

bool SelectableGroup::isEditable() const {
	return editable;
}

void SelectableGroup::setPenColor(const QColor &color) {
	pen->setColor(color);
	stateGroup->getStyle().savePen(*pen);
}

void SelectableGroup::setPenSize(int size) {
	pen->setWidth(size);
	stateGroup->getStyle().savePen(*pen);
}

void SelectableGroup::setPenStyle(Qt::PenStyle &style) {
	pen->setStyle(style);
	stateGroup->getStyle().savePen(*pen);
}

void SelectableGroup::setBrushColor(const QColor &color) {
	brush->setColor(color);
	stateGroup->getStyle().saveBrush(*brush);
}

QPen *SelectableGroup::getPen() const {
	return pen;
}

QBrush *SelectableGroup::getBrush() const {
	return brush;
}

bool SelectableGroup::getApplyStyle() const {
	return apply;
}

void SelectableGroup::setApplyStyle(bool state) {
	this->apply = state;
	stateGroup->getStyle().saveBool(Style::groupApplyStyleAttribute, this->apply);
}

StateGroup &SelectableGroup::getStateGroup() {
	return *stateGroup;
}

void SelectableGroup::toString() const {
	printf("nombre items : %d", getNumItems());
}
}  // namespace VisualAutomata
