#include <QGraphicsTextItem>

#include <cmath>
#include <QVector2D>
#include <QDebug>
#include <complex>
#include <iostream>

#include "transition.hpp"
#include "automata_scene.hpp"

namespace VisualAutomata {
int TransitionDrawable::preferenceAccuracy = 4;

TransitionDrawable::TransitionDrawable(QVector<QPointF *> &points,
                                       Transition *transition,
                                       AutomataDrawable &automataDrawable) :
		Draggable(TRANSITION), transition(transition), points(points), isInitial(false) {
	for (int i = 1; i < this->points.size() - 1; i++) {
		addDragButton(*(new DragButton(this->points[i]->x(),
		                               this->points[i]->y(),
		                               buttonSize,
		                               *this)));
	}
	localAccuracy = 0;
	arrowAngle = 3.14 / 3.0;
	arrowLength = 20.0;
	std::string weight;
	if (transition != nullptr) {
		weight = getText();
	} else {
		weight = "";
	}

	char weightStr[weight.length() + 1];
	strcpy(weightStr, weight.c_str());

	weightText = new QGraphicsTextItem(weightStr);

	weightText->setDefaultTextColor(Qt::GlobalColor::red);
	weightText->setPos(getWeightTextCoordinate());

	this->automataDrawable = &automataDrawable;
	isInitial = false;
	setIsInitial(false);

	if (transition != nullptr) // transition can be nullptr if the transition is initial
	{
		localStyle = &transition->getStyle();
		loadLocalStyle();
	}
}

TransitionDrawable::~TransitionDrawable() {
	while (!points.empty()) {
		QPointF *p = points.last();
		points.pop_back();
		if (p != nullptr) {
			delete (p);
		}

		if (automataDrawable != nullptr) {
			automataDrawable = nullptr;
		}
	}

	while (!states.empty()) {
		StateDrawable *s = states.last();
		states.pop_back();
		if (s != nullptr) {
			delete (s);
		}
	}

	if (scene != nullptr) {
		delete (scene);
		scene = nullptr;
	}

	while (!lines.empty()) {
		QGraphicsLineItem *l = lines.last();
		lines.pop_back();
		if (l != nullptr) {
			delete (l);
		}
	}

	while (!arrowLines.empty()) {
		QGraphicsLineItem *l = arrowLines.last();
		arrowLines.pop_back();
		if (l != nullptr) {
			delete (l);
		}
	}

	if (weightText != nullptr) {
		delete (weightText);
		weightText = nullptr;
	}

	if (TransitionDrawable::preferencePen != nullptr) {
		delete (TransitionDrawable::preferencePen);
		TransitionDrawable::preferencePen = nullptr;
	}
}

// Returns the number of control points
int TransitionDrawable::getNumPoints() const {
	return points.size();
}

void TransitionDrawable::addLinkedState(StateDrawable &state, bool isDestination) {
	if (isDestination) {
		destinationIndex = states.size();
	}
	states.push_back(&state);
	if (isInitial) {
		if (getNumPoints() != 2) {
			qFatal("An initial transition must only have 2 control points");
		}
		if (&getPoint(0) == &state.getCenter()) {
			initialPoint = &getPoint(1);
		} else {
			initialPoint = &getPoint(0);
		}
	}
}

QPointF &TransitionDrawable::getInitialPoint() const {
	return *initialPoint;
}

void TransitionDrawable::unlink() {
	while (!states.empty()) {
		StateDrawable *s = states.last();
		states.pop_back();
		s->unlink(*this);
	}
}

// Returns the control point i
QPointF &TransitionDrawable::getPoint(int i) const {
	return *points[i];
}

// Returns an approximation of the length of the transition
int TransitionDrawable::getLength() {

	/*
	double length = 0;
	int pointsNumber = 10; // The number of points using for the computation of the length
	QPointF previous = getCoordinate(0.0);
	QPointF next;

	double t;
	for (t =  1.0 / pointsNumber; t < 1.0; t += 1.0 / pointsNumber) {
	  next = getCoordinate(t);
	  //qDebug() << next;
	  length += std::sqrt(pow(previous.x() - next.x(), 2) + pow(previous.y() - next.x(), 2));
	  previous = next;
	  //qDebug() << length;
	}
	next = getCoordinate(1.0);
	length += std::sqrt(pow(previous.x() - next.x(), 2) + pow(previous.y() - next.x(), 2));
	qDebug() << length;

	return qRound(length);*/
	double length = 0;
	for (int k = 0; k < points.size() - 1; k++) {
		length += std::sqrt(pow(points[k]->x() - points[k + 1]->x(), 2)
				                    + pow(points[k]->y() - points[k + 1]->y(), 2));
	}
	return qRound(length);
}

void TransitionDrawable::move(DragEvent &dragEvent) {
	if (isInitial) {
		moveInitialTransition(dragEvent.getDeltaX(), dragEvent.getDeltaY(), *dragEvent.getSource());
	}
	if (!AutomataScene::controlKey || AutomataDrawable::getNumSelectedItems() > 1) {
		directedMove(dragEvent.getDeltaX(), dragEvent.getDeltaY(), dragEvent.getSource());
	} else {
		moveRelativelyToStates(dragEvent.getDeltaX(),
		                       dragEvent.getDeltaY(),
		                       *dragEvent.getSource());
	}

	emit localStyleChanged();
}

void TransitionDrawable::moveInitialTransition(int dx, int dy, const DragButton &source) {
	initialPoint->setX(initialPoint->x() + dx);
	initialPoint->setY(initialPoint->y() + dy);
	if (&source != dragButtons[0]) {
		dragButtons[0]->relativeMove(dx, dy);
	}
}

void TransitionDrawable::moveAllTransition(int dx, int dy) {
	points.last()->setX(points.last()->x() + dx);
	points.last()->setY(points.last()->y() + dy);
	points.first()->setX(points.first()->x() + dx);
	points.first()->setY(points.first()->y() + dy);
	directedMove(dx, dy);
}

void TransitionDrawable::directedMove(int dx, int dy) {
	directedMove(dx, dy, nullptr);
}

void TransitionDrawable::directedMove(int dx, int dy, const DragButton *buttonSource) {

	if (buttonSource == nullptr || AutomataDrawable::getNumSelectedItems() > 1) {

		for (int i = 1; i < points.size() - 1; i++) {
			points[i]->setX(points[i]->x() + dx);
			points[i]->setY(points[i]->y() + dy);
		}

		for (auto &dragButton : dragButtons) {
			dragButton->relativeMove(dx, dy);
		}
	} else {
		for (int i = 1; i < points.size() - 1; i++) {
			if (dragButtons[i - 1] == buttonSource) {
				points[i]->setX(points[i]->x() + dx);
				points[i]->setY(points[i]->y() + dy);
			}
		}

		for (auto &dragButton : dragButtons) {
			if (dragButton == buttonSource) {
				dragButton->relativeMove(dx, dy);
			}
		}
	}
}

double enlargement(std::complex<double> center, std::complex<double> p1, std::complex<double> p2) {
	return std::abs((p2 - center) / (p1 - center));
}

std::complex<double> homothetie(std::complex<double> z, std::complex<double> center, double k) {
	return k * z + (1 - k) * center;
}

double angle(std::complex<double> center, std::complex<double> p1, std::complex<double> p2) {
	return std::arg((p2 - center) / (p1 - center));
}

std::complex<double> rotation(std::complex<double> z, std::complex<double> center, double theta) {
	const std::complex<double> i(0, 1);
	return std::exp(i * theta) * (z - center) + center;
}

void TransitionDrawable::moveRelativelyToStates(int dx, int dy, const DragButton &source) {
	QPointF p1 = states.first()->getCenter();
	QPointF p2 = source.pos();

	std::complex<double> o(p1.x(), p1.y());
	std::complex<double> a(p2.x() - dx, p2.y() - dy);
	std::complex<double> b(p2.x(), p2.y());
	for (int i = 1; i < points.size() - 1; i++) {
		std::complex<double> z(points[i]->x(), points[i]->y());

		double k = enlargement(o, a, b);
		double theta = angle(o, a, b);

		std::complex<double> zp = homothetie(z, o, k);
		zp = rotation(zp, o, theta);

		points[i]->setX(std::real(zp));
		points[i]->setY(std::imag(zp));
	}

	for (auto &dragButton : dragButtons) {
		std::complex<double> z(dragButton->locationX(), dragButton->locationY());

		double k = enlargement(o, a, b);
		double theta = angle(o, a, b);

		std::complex<double> zp = homothetie(z, o, k);
		zp = rotation(zp, o, theta);

		dragButton->moveTo(std::real(zp), std::imag(zp));
	}
}

void TransitionDrawable::moveRelativelyToStates(int dx, int dy, StateDrawable &source) {

	if (states.size() >= 2) {
		QPointF p1 = states.first()->getCenter();
		QPointF p2 = states.last()->getCenter();

		if (&source == states.first()) {
			p2 = states.first()->getCenter();
			p1 = states.last()->getCenter();
		}

		std::complex<double> o(p1.x(), p1.y());
		std::complex<double> a(p2.x() - dx, p2.y() - dy);
		std::complex<double> b(p2.x(), p2.y());
		for (int i = 1; i < points.size() - 1; i++) {
			std::complex<double> z(points[i]->x(), points[i]->y());

			double k = enlargement(o, a, b);
			double theta = angle(o, a, b);

			std::complex<double> zp = homothetie(z, o, k);
			zp = rotation(zp, o, theta);

			points[i]->setX(std::real(zp));
			points[i]->setY(std::imag(zp));
		}

		for (auto &dragButton : dragButtons) {
			std::complex<double> z(dragButton->locationX(), dragButton->locationY());

			double k = enlargement(o, a, b);
			double theta = angle(o, a, b);

			std::complex<double> zp = homothetie(z, o, k);
			zp = rotation(zp, o, theta);

			dragButton->moveTo(std::real(zp), std::imag(zp));
		}
	}
}

QPointF TransitionDrawable::getCoordinate(double t) const {
	QPointF pNew(0, 0);
	int numPoints = getNumPoints();
	int n = numPoints - 1;

	for (int k = 0; k < numPoints; k++) {
		pNew += QPointF(binomialCoefficient(n, k) * pow(1 - t, n - k) * pow(t, k) * getPoint(k));
	}
	return pNew;
}

/**
 * Returns C(n,k)
 * @param n
 * @param k
 * @return C(n,k)
 */
int TransitionDrawable::binomialCoefficient(int n, int k) {
	if (k > n) {
		return 0;
	}
	if (k * 2 > n) {
		k = n - k;
	}
	if (k == 0) {
		return 1;
	}

	int result = n;
	for (int i = 2; i <= k; ++i) {
		result *= (n - i + 1);
		result /= i;
	}
	return result;
}

Transition *TransitionDrawable::getData() const {
	return transition;
}

void TransitionDrawable::updateArrow() {
	StateDrawable *destination = states[destinationIndex];
	// If the destination is linked to the lines in the beginning of the vector lines (else it is linked to the lines in the end)
	bool isDestinationAtBeginning;
	QPointF destCenter = destination->getCenter();
	float radius = destination->getRadius();
	int k;
	if (distance(destCenter, lines[0]->line().p1()) < radius
			|| distance(destCenter, lines[0]->line().p2()) < radius) {
		isDestinationAtBeginning = true;
		k = 0;
	} else {
		isDestinationAtBeginning = false;
		k = lines.size() - 1;
	}

	// Finding the line just after the end of the state circle
	double dist = radius - 1.0;
	double dist2;
	while (dist < radius && k >= 0 && k < lines.size()) {
		QPointF point1 = lines[k]->line().p1();
		QPointF point2 = lines[k]->line().p2();
		dist = distance(destCenter, point1);
		dist2 = distance(destCenter, point2);
		dist = max(dist, dist2);
		if (dist < radius) // We will continue the loop
		{
			if (isDestinationAtBeginning) {
				k++;
			} else {
				k--;
			}
		}
	}

	QLineF correctLine = lines[k]->line();

	// A is the point outside the circle and B is the point inside the circle
	QPointF pointA, pointB;
	if (distance(correctLine.p2(), destCenter) < distance(correctLine.p1(), destCenter)) {
		pointB = correctLine.p2();
		pointA = correctLine.p1();
	} else {
		pointB = correctLine.p1();
		pointA = correctLine.p2();
	}
	// The equation of the segment is y(t) = t * AB with t in [0,1]
	// The equation to solve is t^2 * dist²(AB) - 2 * t * (C - A) * (B - A) - (C - A)^2 - 1 = 0 where t in [0, 1] and C is the center of the state, y(t_sol) is the intersection between the line and the circle
	double a = pow(distance(pointA, pointB), 2);
	double b = -2.0 * ((destCenter.x() - pointA.x()) * (pointB.x() - pointA.x())
			+ (destCenter.y() - pointA.y()) * (pointB.y() - pointA.y()));
	double c = pow(pointA.x() - destCenter.x(), 2) + pow(pointA.y() - destCenter.y(), 2)
			- pow(radius, 2);
	double delta = pow(b, 2) - 4.0 * a * c;
	double tSol;
	double t1 = (-b - sqrt(delta)) / (2.0 * a);
	double t2 = (-b + sqrt(delta)) / (2.0 * a);
	if (t1 > -0.01f && t2 > -0.01f) {
		tSol = min(t1, t2);
	} else {
		tSol = max(t1, t2);
	}
	QPointF intersectionPoint =
			pointA + (pointB - pointA) * tSol; // Intersection between the line and the circle

	QPointF vectorDirectorNormalized = (pointA - pointB) / distance(pointB,
	                                                                pointA); // The normalized vector director of the corrected line oriented from A to B

	if (arrowLines.empty()) // The first time the arrow is draw
	{
		auto *arrowLine1 =
				new QGraphicsLineItem(0, 0, 1, 1); // Arbitrary values, we move them just after
		auto *arrowLine2 = new QGraphicsLineItem(0, 0, 1, 1);
		arrowLines.append(arrowLine1);
		arrowLines.append(arrowLine2);
		if (Drawable::getIsVisibleInScene()) {
			scene->addItem(arrowLine1);
			scene->addItem(arrowLine2);
		}
	}

	double alpha = arrowAngle / 2.0;
	// Rotates the vector director
	QPointF rotate = intersectionPoint + (QPointF(
			vectorDirectorNormalized.x() * cos(alpha) - vectorDirectorNormalized.y() * sin(alpha),
			sin(alpha) * vectorDirectorNormalized.x() + cos(alpha) * vectorDirectorNormalized.y())
			* arrowLength);
	QPointF rotateMinus = intersectionPoint + (QPointF(
			vectorDirectorNormalized.x() * cos(-alpha) - vectorDirectorNormalized.y() * sin(-alpha),
			sin(-alpha) * vectorDirectorNormalized.x() + cos(-alpha) * vectorDirectorNormalized.y())
			* arrowLength);

	arrowLines[0]->setLine(QLineF(intersectionPoint, rotate));
	arrowLines[1]->setLine(QLineF(intersectionPoint, rotateMinus));
	QPen chosenPen = *getChosenPen();
	if (isSelected()) {
		chosenPen.setColor(Qt::green);
	}
	arrowLines[0]->setPen(chosenPen);
	arrowLines[1]->setPen(chosenPen);
}

void TransitionDrawable::updateDotLines() {
	for (int k = 0; k < getNbDragButton(); k++) {
		if (k == dotLines.size()) {
			auto *line =
					new QGraphicsLineItem(0, 0, 1, 1); // Arbitrary values, we move them just after
			dotLines.append(line);
			if (Drawable::getIsVisibleInScene()) {
				scene->addItem(line);
			}
		}
		int lineIndex = (computesPointsNumber() * (k + 1)) / (getNbDragButton() + 1);
		QPointF curvePoint = lines[lineIndex]->line().p1();
		QPen chosenPenCopy = *getChosenPen();
		chosenPenCopy.setWidth(1);
		chosenPenCopy.setStyle(Qt::DashLine);
		if (isSelected()) {
			chosenPenCopy.setColor(Qt::green);
		}
		dotLines[k]->setLine(getDragButton(k)->pos().x(),
		                     getDragButton(k)->pos().y(),
		                     curvePoint.x(),
		                     curvePoint.y());
		dotLines[k]->setPen(chosenPenCopy);
	}
}

double TransitionDrawable::distance(QPointF p1, QPointF p2) {
	return sqrt(pow(p1.x() - p2.x(), 2) + pow(p1.y() - p2.y(), 2));
}

void TransitionDrawable::update() {
	QPointF pNew;

	QPointF pOld(getPoint(0));
	int pointNumber = computesPointsNumber();
	double t = 1.0 / pointNumber;

	int count = 0;  // To count the number of lines needed

	while (t < 1) {
		pNew = getCoordinate(t);

		// If there are still lines to update
		if (count < lines.size()) {
			lines[count]->setLine(pOld.x(), pOld.y(), pNew.x(), pNew.y());
		}
			// Else other are created
		else {
			auto *newLine = new QGraphicsLineItem(pOld.x(), pOld.y(), pNew.x(), pNew.y());
			lines.append(newLine);
			if (Drawable::getIsVisibleInScene()) {
				scene->addItem(newLine);
			}
		}
		count++;

		pOld = pNew;
		t += 1.0 / pointNumber;
	}

	pNew = getCoordinate(1);

	if (count < lines.size()) {
		lines[count]->setLine(pOld.x(), pOld.y(), pNew.x(), pNew.y());
	} else {
		auto *newLine = new QGraphicsLineItem(pOld.x(), pOld.y(), pNew.x(), pNew.y());
		lines.append(newLine);
		if (Drawable::getIsVisibleInScene()) {
			scene->addItem(newLine);
		}
	}
	count++;

	// Deletes line if there are too much
	while (count < lines.size()) {
		if (Drawable::getIsVisibleInScene()) {
			scene->removeItem(lines.last());
		}
		lines.pop_back();
	}
	// Change the drawing priority of lines in the scene
	for (auto &line : lines) {
		line->setZValue(drawingPriority);
	}

	QPen chosenPen = *getChosenPen();
	if (isSelected()) {
		chosenPen.setColor(Qt::green);
		for (auto &line : lines) {
			line->setPen(chosenPen);
		}
	} else {
		for (auto &line : lines) {
			line->setPen(chosenPen);
		}
	}
	weightText->setPos(getWeightTextCoordinate());

	updateArrow();
	updateText();
	if (!isInitial) {
		updateDotLines();
	}
}

QPointF TransitionDrawable::getWeightTextCoordinate() const {
	if (isInitial) {
		return *initialPoint;
	} else {
		return getCoordinate(0.5);
	}
}

void TransitionDrawable::addAttachedGroup(SelectableGroup &group) {
	if (attachedGroups.contains(&group)) {
		return;
	}

	attachedGroups.push_back(&group);
}

void TransitionDrawable::removeAttachedGroup(SelectableGroup &group) {
	if (attachedGroups.contains(&group)) {
		attachedGroups.removeAll(&group);
	}
}

QPen *TransitionDrawable::preferencePen = new QPen();

void TransitionDrawable::setPreferencePenColor(const QColor &color) {
	TransitionDrawable::preferencePen->setColor(color);
}

void TransitionDrawable::setPreferencePenSize(int size) {
	TransitionDrawable::preferencePen->setWidth(size);
}

void TransitionDrawable::setPreferencePenStyle(Qt::PenStyle &style) {
	TransitionDrawable::preferencePen->setStyle(style);
}

QPen *TransitionDrawable::getChosenPen() const {
	if (getHasLocalStyle()) {
		return getLocalPen();
	} else {
		int priority = -1000000;
		QPen *newPen = nullptr;

		for (SelectableGroup *group : attachedGroups) {
			if (group->getApplyStyle() && group->getPriority() > priority) {
				newPen = group->getPen();
				priority = group->getPriority();
			}
		}

		if (newPen != nullptr) {
			return newPen;
		}
	}

	return preferencePen;
}

int TransitionDrawable::getChosenAccuracy() const {
	if (getHasLocalStyle()) {
		return localAccuracy;
	} else {
		return preferenceAccuracy;
	}
}

void TransitionDrawable::setLocalAccuracy(int accuracy) {
	localAccuracy = accuracy;
}

QBrush *TransitionDrawable::getChosenBrush() const {
	return getLocalBrush();
}

bool TransitionDrawable::getHasLocalStyle() const {
	return localAccuracy != 0 || Drawable::getHasLocalStyle();
}

int TransitionDrawable::computesPointsNumber() {
	int curveLength = getLength();
	int pointsNumber = (curveLength * getChosenAccuracy()) / 300;
	return std::max(pointsNumber, 2);
}

void TransitionDrawable::buttonMoved(DragEvent &event) {
	automataDrawable->buttonMoved(event);
	automataDrawable->update();
}

void TransitionDrawable::buttonSelected() {
	automataDrawable->setSelectedItem(*this);
}

void TransitionDrawable::addToScene() {
	if (!getIsVisibleInScene()) {
		for (auto &line : lines) {
			scene->addItem(line);
		}
		for (auto &arrowLine : arrowLines) {
			scene->addItem(arrowLine);
		}
		for (auto &dotLine : dotLines) {
			scene->addItem(dotLine);
		}
		scene->addItem(weightText);
		Drawable::addToScene();
		update(); // TODO : addToScene should not use update
	}
}

void TransitionDrawable::setIsInitial(bool initial) {
	this->isInitial = initial;
}

void TransitionDrawable::setScene(QGraphicsScene &newScene) {
	this->scene = &newScene;
	if (!isInitial) { // Initial transitions are drawn by the state
		addToScene();
		addDragButtonsToScene(newScene);
	}
}

void TransitionDrawable::removeFromScene() {
	if (getIsVisibleInScene()) {
		Drawable::removeFromScene();
		for (auto &line : lines) {
			scene->removeItem(line);
		}
		for (auto &arrowLine : arrowLines) {
			scene->removeItem(arrowLine);
		}
		for (auto &dotLine : dotLines) {
			scene->removeItem(dotLine);
		}
		scene->removeItem(weightText);
	}
}

void TransitionDrawable::removeFromAutomata() {
	removeFromScene();
	while (!lines.isEmpty()) {
		lines.pop_front();
	}
	while (!arrowLines.isEmpty()) {
		arrowLines.pop_front();
	}
	this->unlink();
	this->removeDragButtonsFromAutomata(*scene);
	scene->removeItem(weightText);
}

void TransitionDrawable::updateText() {
	weightText->setPos(getCoordinate(0.5));
	weightText->setPlainText(getText().c_str());
}

void TransitionDrawable::updateLocalStyle() {
	if (!isInitial) {
		localStyle->saveBool(Style::hasLocalStyleAttribute, getHasLocalStyle());
		if (getHasLocalStyle()) {
			localStyle->savePen(*localPen);
		}

		QVector<QString> xCoord;
		QVector<QString> yCoord;
		for (int k = 1; k < getNumPoints() - 1; k++) {
			xCoord.append(QString(QString::number(getPoint(k).x())));
			yCoord.append(QString(QString::number(getPoint(k).y())));
		}

		localStyle->saveQVectorQString(Style::transitionXAttribute, xCoord);
		localStyle->saveQVectorQString(Style::transitionYAttribute, yCoord);
	}
}

void TransitionDrawable::loadLocalStyle() {
	if (!isInitial) {
		setHasLocalStyle(localStyle->loadBool(Style::hasLocalStyleAttribute, getHasLocalStyle()));
		if (getHasLocalStyle()) {
			Drawable::initializePens();
			localStyle->loadPen(*localPen);
		}
		if (Style::useCoordinates) {
			QVector<QString> xCoord;
			QVector<QString> yCoord;
			localStyle->loadQVectorQString(Style::transitionXAttribute, xCoord);
			localStyle->loadQVectorQString(Style::transitionYAttribute, yCoord);
			for (int k = 0; k < xCoord.size(); k++) {
				points[k
						+ 1]->setX(xCoord[k].toFloat()); // k + 1 because the point 0 is a point of a state
				points[k + 1]->setY(yCoord[k].toFloat());
				getDragButton(k)->moveTo(points[k + 1]->x(), points[k + 1]->y());
			}
		}
	}
}

bool TransitionDrawable::getIsInitial() const {
	return isInitial;
}

QVector<StateDrawable *> &TransitionDrawable::getStatesDrawable() {
	return states;
}

std::string TransitionDrawable::getText() {
	if (transition != nullptr) {
		if (!transition->getWeight().empty()) {
			return transition->getLabel() + " | " + transition->getWeight();
		} else {
			return transition->getLabel();
		}
	} else {
		return "";
	}
}
}  // namespace VisualAutomata
