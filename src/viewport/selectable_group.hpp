#ifndef VISUAL_AUTOMATA_SELECTABLE_GROUP_HPP
#define VISUAL_AUTOMATA_SELECTABLE_GROUP_HPP

#include <QVector>
#include <QPen>
#include <QBrush>

#include "selectable.hpp"
#include "groupable.hpp"
#include "state_group.hpp"

namespace VisualAutomata {

class Groupable;

/**
 * Creates groups of object
 */
class SelectableGroup {
	/**< The name of the SelectableGroup */
	std::string name;
	/**< The priority of the SelectableGroup */
	int priority;
	/**< If the SelectableGroup is editable */
	bool editable;
	/**< If the Style of the SelectableGroup is apply */
	bool apply;

	/**< The Pen of the SelectableGroup */
	QPen *pen;
	/**< The Brush of the SelectableGroup */
	QBrush *brush;

	/**< All objects in the SelectableGroup */
	QVector<Groupable *> groupItems;

	/**< StateGroup linked with the SelectableGroup */
	StateGroup *stateGroup;

public:

	/**
	 * Constructor.
	 * @param priority is the priority of the SelectableGroup
	 * @param name is the name of the SelectableGroup
	 * */
	SelectableGroup(int priority, const std::string &name);

	/**
	 * Constructor.
	 * @param stateGroup is the StateGroup linked with the SelectableGroup
	 * @see StateGroup
	 */
	explicit SelectableGroup(StateGroup &stateGroup);

	/**
	 * Method.
	 * Adds an object to the group.
	 * @param item is the item to add
	 * @see Groupable
	 */
	void addItem(Groupable &item);

	/**
	 * Method.
	 * Adds objects to the group.
	 * @param items are the items to add
	 * @see Groupable
	 */
	void addItems(const QVector<Groupable *> &items);

	/**
	 * Method.
	 * Removes an object from the group.
	 * @param item is the item to remove
	 * @see Groupable
	 */
	void removeItem(Groupable &item);

	/**
	 * Method.
	 * Removes objects from the group.
	 * @param item are the items to remove
	 * @see Groupable
	 */
	void removeItems(const QVector<Groupable *> &items);

	/**
	 * Method.
	 * Removes all objects from the SelectableGroup.
	 */
	void removeAll();

	/**
	 * Method.
	 * Returns if the group contains the object or not.
	 * @param item is the object tested
	 * @return true is the object is in the SelectableGroup, false otherwise
	 * @see Groupable
	 */
	bool contains(Groupable &item);

	/**
	 * Method.
	 * Returns the object at the index i in the SelectableGroup.
	 * @param i is the index of the object
	 * @return the object at the index i
	 * @see Groupable
	 */
	Groupable &getItem(int i);

	/**
	 * Method.
	 * Returns all the items of the SelectableGroup. A copy of the vector is made to allow manipulation on the vector.
	 * @return all the items of the SelectableGroup
	 * @see Groupable
	 */
	QVector<Groupable *> getItems();

	/**
	 * Method.
	 * Returns the number of objects in the SelectableGroup.
	 * @return the number of objects in the SelectableGroup.
	 */
	int getNumItems() const;

	/**
	 * Method.
	 * Changes the name of the SelectableGroup.
	 * @param newName is the new name of the SelectableGroup
	 */
	void setName(const std::string &newName);

	/**
	 * Method.
	 * Returns the name of the SelectableGroup.
	 * @return the name of the SelectableGroup
	 */
	std::string getName() const;

	/**
	 * Method.
	 * Changes the priority of the SelectableGroup.
	 * @param newPriority is the new priority of the SelectableGroup
	 */
	void changePriority(int newPriority);

	/**
	 * Method.
	 * Returns the priority of the SelectableGroup.
	 * @return Returns the name of the SelectableGroup
	 */
	int getPriority() const;

	/**
	 * Method.
	 * Sets if the SelectableGroup is editable or not.
	 * @param state is the new state for the edition of the SelectableGroup
	 */
	void setEditable(bool state);

	/**
	 * Method.
	 * Returns if the SelectableGroup is editable or not.
	 * @return true if it is editable, false otherwise
	 */
	bool isEditable() const;

	/**
	 * Method.
	 * Changes the Pen color of the SelectableGroup
	 * @param color is the new color of the Pen
	 */
	void setPenColor(const QColor &color);

	/**
	 * Method.
	 * Changes the Pen size of the SelectableGroup
	 * @param size is the new size of the Pen
	 */
	void setPenSize(int size);

	/**
	 * Method.
	 * Changes the PenStyle of the SelectableGroup
	 * @param style is the new style of the Pen
	 */
	void setPenStyle(Qt::PenStyle &style);

	/**
	 * Method.
	 * Changes the Brush color of the SelectableGroup
	 * @param color is the new color of the Brush
	 */
	void setBrushColor(const QColor &color);

	/**
	 * Method.
	 * Returns the Pen of the SelectableGroup.
	 * @return the Pen of the SelectableGroup
	 */
	QPen *getPen() const;

	/**
	 * Method.
	 * Returns the Brush of the SelectableGroup.
	 * @return the Brush of the SelectableGroup
	 */
	QBrush *getBrush() const;

	/**
	 * Method.
	 * Returns if the style of the SelectableGroup is applied to the objects or not.
	 * @return true if it is applied, false otherwise
	 */
	bool getApplyStyle() const;

	/**
	 * Method.
	 * Sets if the style of the SelectableGroup is applied to the objects.
	 * @param b defines if the style will be applied or not
	 */
	void setApplyStyle(bool b);

	/**
	 * Method.
	 * Returns the StateGroup associated to the SelectableGroup
	 * @return the StateGroup associated to the SelectableGroup
	 * @see StateGroup
	 */
	StateGroup &getStateGroup();

	/**
	 * Method.
	 * Prints information about the SelectableGroup
	 */
	void toString() const;
};
}  // namespace VisualAutomata

#endif
