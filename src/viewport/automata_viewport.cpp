#include "automata_viewport.hpp"
#include <QDebug>

namespace VisualAutomata {
qreal AutomataViewport::zoomFactor = 1;

AutomataViewport::AutomataViewport(QWidget *parent) : QWidget(parent) {
	view = new QGraphicsView(this);
	auto scene = new AutomataScene(this, 0, 0, 1370, 1000);
	view->setScene(scene->getScene());
	view->setTransformationAnchor(QGraphicsView::AnchorViewCenter); // enable zoom to be centered
	currentScene = scene;

	setSquareBackground();
}

AutomataViewport::~AutomataViewport() {
	if (currentScene != nullptr) {
		delete (currentScene);
		currentScene = nullptr;
	}
}

// Updates the drawing of the automata
void AutomataViewport::redraw() const {
	if (currentScene != nullptr) {
		currentScene->redraw();
	}
}

void AutomataViewport::zoomIn() {
	zoomFactor *= 1.2;
	zoomByFactor();
}

void AutomataViewport::zoomOut() {
	zoomFactor /= 1.2;
	zoomByFactor();
}

void AutomataViewport::zoomActualSize() {
	zoomFactor = 1;
	zoomByFactor();
}

void AutomataViewport::zoomByFactor() {
	view->setTransform(QTransform(zoomFactor, 0, 0, zoomFactor, 0, 0));
}

void AutomataViewport::removeStateFromAutomata(StateDrawable &s) {
	currentScene->getAutomataDrawable()->removeStateFromAutomata(s);
}

void AutomataViewport::removeTransitionFromAutomata(TransitionDrawable &t) {
	currentScene->getAutomataDrawable()->removeTransitionFromAutomata(t);
}

void AutomataViewport::wheelEvent(QWheelEvent *event) {
	if (QApplication::keyboardModifiers() == Qt::ControlModifier
			|| QApplication::keyboardModifiers() == Qt::ShiftModifier) {

		// mouse's wheel used
		if (event->type() == QEvent::Wheel) {
			const qreal delta = event->angleDelta().y() / 120.0;
			if (delta > 0) {
				zoomFactor *= 1.05;
			} else if (delta < 0) {
				zoomFactor /= 1.05;
			}
			view->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
			zoomByFactor();
		}
	}
}

void AutomataViewport::keyPressEvent(QKeyEvent *event) {

	if (event->modifiers() == Qt::ControlModifier) {
		if (event->key() == Qt::Key_Plus) {
			zoomFactor *= 1.2;
			zoomByFactor();
		} else if (event->key() == Qt::Key_Minus) {
			zoomFactor /= 1.2;
			zoomByFactor();
		} else if (event->key() == Qt::Key_0) {
			zoomFactor = 1;
			zoomByFactor();
		}
	}
}

void AutomataViewport::enterEvent(QEvent *mouseEvent) {
	ViewportAction action = currentScene->getCurrentAction();
	if (action == action_add_state || action == action_add_transition) {
		QApplication::setOverrideCursor(Qt::CrossCursor);
	}

	QWidget::enterEvent(mouseEvent);
}

void AutomataViewport::leaveEvent(QEvent *mouseEvent) {
	QApplication::restoreOverrideCursor();
	currentScene->getAutomataDrawable()->endTransitionPreview();
	QWidget::leaveEvent(mouseEvent);
}

void AutomataViewport::setSquareBackground() {
	view->setBackgroundBrush(QPixmap(":/pictures/grid-square.png"));
}

void AutomataViewport::setTriangularBackground() {
	view->setBackgroundBrush(QPixmap(":/pictures/grid-triangle.jpeg"));
}

void AutomataViewport::setCrossBackground() {
	view->setBackgroundBrush(QPixmap(":/pictures/grid-cross.png"));
}

void AutomataViewport::setBlankBackground() {
	view->setBackgroundBrush(QPixmap(":/pictures/grid-blank.png"));
}

AutomataScene *AutomataViewport::getScene() {
	return currentScene;
}
}  // namespace VisualAutomata
