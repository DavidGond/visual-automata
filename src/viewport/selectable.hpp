#ifndef VISUAL_AUTOMATA_SELECTABLE_HPP
#define VISUAL_AUTOMATA_SELECTABLE_HPP

#include "movable.hpp"

namespace VisualAutomata {

/** 
 * Enum.
 * Types to define if an object is a TransitionDrawable or a StateDrawable
 * @see StateDrawable
 * @see TransitionDrawable
 */
enum SelectableType {
	TRANSITION, STATE
};

/** 
 * Objects from this class can be selected
 */
class Selectable: public Movable {
	/**< Informs if the object is selected */
	bool selected;
	/**< Informs about the SelectableType of the object */
	enum SelectableType type;

public:
	/**
	 * Constructor.
	 * @param type is the SelectableType of the object
	 * @see SelectableType
	 */
	explicit Selectable(enum SelectableType type) : selected(false), type(type) {
	}

	/**
	 * virtual Destructor.
	 */
	virtual ~Selectable();

	/**
	 * Method.
	 * Returns if the object is selected.
	 * @return if the object is selected
	 */
	bool isSelected() const;

	/**
	 * Method.
	 * Defines if the object is selected
	 * @param b defines if the object is selected (true) or not (false)
	 */
	void setSelected(bool b);

	/**
	 * Returns the SelectableType of the object.
	 * @return the SelectableType of the object
	 * @see SelectableGroup
	 */
	enum SelectableType getType() const;

	/**
	 * Virtual Method.
	 * When a DragButton is selected, this function is called
	 */
	virtual void buttonSelected() = 0;
};
}  // namespace VisualAutomata

#endif
