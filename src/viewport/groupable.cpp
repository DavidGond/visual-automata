#include "groupable.hpp"

namespace VisualAutomata {

QVector<SelectableGroup *> Groupable::getAttachedGroups() {
	return attachedGroups;
}

void Groupable::addAttachedGroup(SelectableGroup &group) {
	if (attachedGroups.contains(&group)) {
		return;
	}

	attachedGroups.push_back(&group);
}

void Groupable::removeAttachedGroup(SelectableGroup &group) {
	if (attachedGroups.contains(&group)) {
		attachedGroups.removeAll(&group);
	}
}
}  // namespace VisualAutomata
