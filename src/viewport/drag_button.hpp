#ifndef VISUAL_AUTOMATA_DRAG_BUTTON_HPP
#define VISUAL_AUTOMATA_DRAG_BUTTON_HPP

#include <QVector>
#include <QPushButton>
#include <QWidget>
#include <QMouseEvent>
#include <QPoint>
#include <memory>
#include <QColor>

#include "drag_event.hpp"
#include "draggable.hpp"

namespace VisualAutomata {
class Draggable;

class DragEvent;

/**
 * DragButtons are buttons which can be bind to a Draggable object, each time the button is moved, the object is update.
 */
class DragButton: public QPushButton {
	/**< Location of the DragButton on the x coordinate with better precision */
	double locX{};
	/**< Location of the DragButton on the x coordinate with better precision */
	double locY{};

	/**< Previous abscissa of the drag button. */
	int prevX{};
	/**< Previous ordinate of the drag button. */
	int prevY{};

	/**< Objects linked with the drag button, (when the drag button moves, this objects are updated). */
	QVector<Draggable *> boundObjects;
	/**< Event that the DragButton will send to boundObjects when it moves */
	DragEvent *dragEvent;
	bool drag;

public:
	/**
	 * Constructor.
	 * @param x is the x coordinate of the DragButton in the scene
	 * @param y is the y coordinate of the DragButton in the scene
	 * @param size is the size of the drag button.
	 * @param object is the object linked with the drag button.
	 * @see Draggable
	 */
	DragButton(int x, int y, int size, Draggable &object);

	/**
	 * Method.
	 * Sets the color of the drag button.
	 * @param color is the new color for the drag button.
	 */
	void setColor(const QColor &c);

	/**
	 * Method.
	 * Adds another Draggable object to the bound objects.
	 * @param boundObject is the new object to add.
	 * @see Draggable
	 */
	void addBoundObject(Draggable &boundObject);

	/**
	 * Method.
	 * Change the position of the QPushButton.
	 * @param x is the new x coordinate.
	 * @param y is the new y coordinate.
	 */
	void moveTo(int x, int y);

	/**
	 * Method.
	 * Change the position of the QPushButton (like moveTo(int, int) but with float to have a better precision).
	 * @param x is the new x coordinate.
	 * @param y is the new y coordinate.
	 */
	void moveTo(double x, double y);

	/**
	 * Method.
	 * Increases the coordinate with dx and dy.
	 * @param dx is the movement on the abscissa coordinate.
	 * @param dy is the movement on the orderly coordinate.
	 */
	void relativeMove(int dx, int dy);

	/**
	 * Method (Overriding).
	 * Initialises the prevX and prevY position when the mouse selects the button and defines boundObjects as selected. Calls the buttonSelected function of all boundObjects
	 * @param event stores some information relative to the mouse.
	 */
	void mousePressEvent(QMouseEvent *event) override;

	/**
	 * Method (Overriding).
	 * Unselects boundObjects.
	 * @param event stores some information relative to the mouse.
	 */
	void mouseReleaseEvent(QMouseEvent *event) override;

	/**
	 * Method (Overriding).
	 * Moves the button according to the mouse position when the button is selected and the mouse is moving. Calls the buttonMoved function of all boundObjects.
	 * @param event stores some information relative to the mouse.
	 */
	void mouseMoveEvent(QMouseEvent *event) override;

	/**
	 * Method.
	 * Returns the floating x coordinate of the DragButton.
	 * @return the floating x coordinate
	 */
	double locationX() const;

	/**
	 * Method.
	 * Returns the floating y coordinate of the DragButton.
	 * @return the floating y coordinate
	 */
	double locationY() const;
};
}  // namespace VisualAutomata

#endif
