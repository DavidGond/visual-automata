#ifndef VISUAL_AUTOMATA_DRAG_EVENT_HPP
#define VISUAL_AUTOMATA_DRAG_EVENT_HPP

#include "drag_button.hpp"

namespace VisualAutomata {
class DragButton;

/** Gathers some information about the moveTo of a DragButton */
class DragEvent {
	/**< position differential on abscissa coordinate */
	int dx;
	/**< position differential on orderly coordinate */
	int dy;

	/**< DragButton associated with the event */
	const DragButton *dragButton;

public:
	/**
	 * Destructor.
	 */
	~DragEvent();

	/**
	 * Method.
	 * Sets the values associated with the event.
	 * @param addX is the added amount on the abscissa
	 * @param addY is the added amount of the ordinate
	 * @param button is the DragButton source of the event
	 * @see DragButton
	 */
	void setValues(int addX, int addY, const DragButton &button);

	/**
	 * Method.
	 * Returns the position differential on the x coordinate of the event.
	 * @return the position differential on the x coordinate
	 */
	int getDeltaX() const;

	/**
	 * Method.
	 * Returns the position differential on the y coordinate of the event.
	 * @return the position differential on the y coordinate
	 */
	int getDeltaY() const;

	/**
	 * Method.
	 * Returns the source of the event.
	 * @return the source of the event.
	 */
	const DragButton *getSource() const;
};
}  // namespace VisualAutomata

#endif
