#ifndef VISUAL_AUTOMATA_MOVABLE_HPP
#define VISUAL_AUTOMATA_MOVABLE_HPP

namespace VisualAutomata {

class DragEvent;

/**
 * Objects from this interface can be moved
 */
class Movable {
public:
	/**
	 * Virtual Method.
	 * Moves the object according to the DragEvent
	 * @param event stores information about the event
	 * @see DragEvent
	 */
	virtual void move(DragEvent &event) = 0;
};
}  // namespace VisualAutomata

#endif
