#ifndef VISUAL_AUTOMATA_DRAWABLE_HPP
#define VISUAL_AUTOMATA_DRAWABLE_HPP

#include "style.hpp"

#include <QGraphicsScene>
#include <QPen>
#include <QBrush>

namespace VisualAutomata {
/**
 * Virtual class which provides methods for Drawable objects
 */
class Drawable: public QObject {
Q_OBJECT
private:
	/**< If the drawable was added in the scene */
	bool isVisibleInScene;

protected:
	/**< The local Style of the Drawable */
	Style *localStyle;
	/**< The local Pen of the Drawable */
	QPen *localPen;
	/**< The local Brush of the QBrush */
	QBrush *localBrush;
	/**< If the Drawable has a local Style */
	bool hasLocalStyle;

public:
	/**
	 * Constructor.
	 */
	Drawable();

	/**
	 * Virtual Destructor
	 */
	~Drawable() override;

	/**
	   * Virtual Method.
	   * Updates Drawable objects.
	   */
	virtual void update() = 0;

	/**
	 * Virtual Method.
	 * Removes a Drawable object from the scene
	 * Do not forget to redefine this function in daughter classes
	 * Do not forget to call Drawable::removeFromScene() in the redefined function
	 */
	virtual void removeFromScene();

	/**
	 * Virtual Method.
	 * Adds a Drawable object in the scene
	 * Do not forget to redefine this function in daughter classes
	 * Do not forget to call Drawable::removeFromScene() in the redefined function
	 */
	virtual void addToScene();

	/**
	 * Method.
	 * Returns if the Drawable object is visible in the scene.
	 * @return if the object is visible
	 */
	bool getIsVisibleInScene() const;

	/**
	 * Virtual Method.
	 * Changes the Pen color of the Drawable object
	 * @param color is the new QColor for the Pen of the Drawable object
	 */
	virtual void setLocalPenColor(const QColor &color);

	/**
	 * Virtual Method.
	 * Changes the Pen size of the Drawable object
	 * @param size is the new size for the Pen of the Drawable object
	 */
	virtual void setLocalPenSize(int size);

	/**
	 * Virtual Method.
	 * Changes the Pen Style of the Drawable object
	 * @param style is the new PenStyle for the Pen of the Drawable object
	 */
	virtual void setLocalPenStyle(Qt::PenStyle &style);

	/**
	 * Virtual Method.
	 * Changes the Brush color of the Drawable object
	 * @param color is the new QColor for the Brush of the Drawable object
	 */
	virtual void setLocalBrushColor(const QColor &color);

	/**
	 * Virtual Method.
	 * Returns the Pen chosen to draw the Drawable object. The priority are : preference Pen > SelectableGroup Pen > local Pen
	 * @return the Pen chosen to draw the Drawable object
	 */
	virtual QPen *getChosenPen() const = 0;

	/**
	 * Virtual Method.
	 * Returns the Pen chosen to draw the Drawable object. The priority are : preference Pen > SelectableGroup Pen > local Pen
	 * @return the Pen chosen to draw the Drawable object
	 */
	virtual QBrush *getChosenBrush() const = 0;

	/**
	 * Virtual Method.
	 * Load the local style of the state or transition. Must be done at the opening of the automaton
	 */
	virtual void loadLocalStyle() = 0;

public slots:
	/**
	 * Virtual Method.
	 * Updates the local style of the drawable
	 */
	virtual void updateLocalStyle() = 0;

signals:
	/**
	 * Signal.
	 * This signal is emitted when the local style is changed by the user. It will call the function updateLocalStyle()
	 */
	void localStyleChanged();

protected:
	/**
	 * Virtual Method.
	 * Returns the local Pen of the Drawable object.
	 * @return the local Pen of the Drawable object
	 */
	virtual QPen *getLocalPen() const;

	/**
	 * Virtual Method.
	 * Returns the local Brush of the Drawable object.
	 * @return the local Brush of the Drawable object
	 */
	virtual QBrush *getLocalBrush() const;

	/**
	 * Virtual Method.
	 * Returns if the Drawable object has a local Style.
	 * @return if the Drawable object has a local Style
	 */
	virtual bool getHasLocalStyle() const;

	/**
	 * Virtual Method.
	 * Sets if the Drawable object has a local Style or not.
	 * @param value is the new value of the boolean
	 */
	virtual void setHasLocalStyle(bool value);

	/**
	 * Method.
	 * Initialises Pens of the Drawable object.
	 */
	void initializePens();
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_DRAWABLE_HPP
