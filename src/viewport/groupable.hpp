#ifndef VISUAL_AUTOMATA_GROUPABLE_HPP
#define VISUAL_AUTOMATA_GROUPABLE_HPP

#include "selectable.hpp"
#include "selectable_group.hpp"

namespace VisualAutomata {
class SelectableGroup;

/**
 * Objects from this class can be attached to a SelectableGroup
 * @see SelectableGroup
 */
class Groupable: public Selectable {
protected:
	/**< All groups in which the object is */
	QVector<SelectableGroup *> attachedGroups;

public :
	/**
	 * Constructor.
	 * @param type is the type of the object
	 * @see SelectableType
	 */
	explicit Groupable(enum SelectableType type) : Selectable(type) {
	}

	/**
	 * Method.
	 * Returns all the SelectableGroups to which this object is attached to
	 * @return all the SelectableGroups to which this object is attached to
	 * @see SelectableGroup
	 */
	QVector<SelectableGroup *> getAttachedGroups();

	/**
	 * Method.
	 * Attaches this object to a group
	 * @param group is the SelectableGroup to which this object will be attached to
	 * @see SelectableGroup
	 */
	virtual void addAttachedGroup(SelectableGroup &group);

	/**
	* Method.
	* Removes this object from a group
	* @param group is the SelectableGroup to which this object will be removed
	* @see SelectableGroup
	*/
	virtual void removeAttachedGroup(SelectableGroup &group);
};
}  // namespace VisualAutomata

#endif
