#ifndef VISUAL_AUTOMATA_AUTOMATA_DRAWABLE_HPP
#define VISUAL_AUTOMATA_AUTOMATA_DRAWABLE_HPP

#include <QVector>
#include <QGraphicsView>

#include "automata.hpp"
#include "transition_drawable.hpp"
#include "state_drawable.hpp"
#include "selectable_group.hpp"

namespace VisualAutomata {

class StateDrawable;

class TransitionDrawable;

/**
 * An AutomataDrawable representing an automata in the scene.
 */
class AutomataDrawable: public Drawable {
Q_OBJECT

	/**< StateDrawables in the automata */
	QVector<StateDrawable *> states;
	/**< TransitionDrawables in the automata */
	QVector<TransitionDrawable *> transitions;

	std::string previewName;
	/**< StateDrawable which is going to be the source of the next transition */
	StateDrawable *previewSource;
	/**< StateDrawable which is going to be the destination of the next transition */
	StateDrawable *previewDestination;
	QVector<QPointF *> previewControlPoints;

	/**< Returns the StateDrawable associated with a State */
	std::map<const State *, StateDrawable *> stateDictionary;
	/**< Returns the TransitionDrawable associated with a Transition */
	std::map<const Transition *, TransitionDrawable *> transitionDictionary;

	/**< Current items selected in the scene */
	static SelectableGroup *selectedItems;
	/**< Default SelectableGroup when no groups are selected */
	SelectableGroup emptySelection;
	/**< All SelectableGroups defined by the user in the AutomataDrawable */
	QVector<SelectableGroup *> selectableGroups;

	/**< Scene in which components are drawn */
	QGraphicsScene *scene;

	/**< Automata linked with the AutomataDrawable */
	Automata *automata;

public:

	/**
	 * Constructor.
	 * @param scene is the scene in which the AutomataDrawable will be drawn
	 */
	explicit AutomataDrawable(QGraphicsScene &scene);

	/**
	 * Virtual Destructor.
	 */
	~AutomataDrawable() override;

	/**
	 * Method.
	 * Updates all TransitionDrawables and StateDrawables of the AutomataDrawable.
	 * @see TransitionDrawable
	 * @see StateDrawable
	 */
	void update() override;

	/**
	 * Method.
	 * Initialises all components of the AutomataDrawable and draw them in the scene.
	 * @param newAutomata is Automata which will be linked with this AutomataDrawable
	 * @see Automata
	 */
	void setAutomata(Automata *newAutomata);

	/**
	 * Method
	 * Sets the Automata to draw with a linear layout
	 */
	void setAutomataLinear(VisualAutomata::Automata *oldAutomata);

	/**
	 * Method
	 * Sets the Automata to draw with a Circular layout
	 */
	void setAutomataCircular(VisualAutomata::Automata *oldAutomata);

	Automata *getAutomata() const;

	/**
	 * Method.
	 * Changes the scene in which components of the AutomataDrawable are drawn. A call to removeFromScene and addToScene functions are necessary to redraw all components previously drawn in the new scene
	 * @param newScene is the new scene in which components will be drawn
	 * @see removeFromScene()
	 * @see addToScene()
	 */
	void setScene(QGraphicsScene &newScene);

	/**
	 * Virtual Method.
	 * Removes the AutomataDrawable and all it's components from the scene
	 */
	void removeFromScene() override;

	/**
	 * Virtual Method.
	 * Adds the AutomataDrawable and all it's components to the scene
	 */
	void addToScene() override;

	/**
	 * Method.
	 * Adds a Transition in the Automata linked with the AutomataDrawable
	 * @param transition is the Transition to add
	 * @see Automata
	 * @see Transition
	 */
	void addTransitionToAutomata(Transition &transition);

	/**
	 * Method.
	 * Adds a State in the Automata linked with the AutomataDrawable
	 * @param state is the State to add
	 * @see Automata
	 * @see State
	 */
	void addStateToAutomata(State &state);

	/**
	 * Method.
	 * Adds a StateDrawable to the AutomataDrawable.
	 * @param center is the coordinates of the center of the StateDrawable in the scene
	 * @param radius is the radius of the StateDrawable
	 * @param state is the State linked with the StateDrawable
	 * @return the StateDrawable adds to the AutomataDrawable
	 * @see State
	 * @see StateDrawable
	 */
	StateDrawable *addStateToScene(QPointF &center, float radius, State &state);

	/**
	 * Method.
	 * Removes a StateDrawable from the AutomataDrawable. And it's State linked with from the Automata.
	 * @param state is the StateDrawable to remove
	 * @see Automata
	 * @see State
	 * @see StateDrawable
	 */
	void removeStateFromAutomata(StateDrawable &state);

	/**
	 * Method.
	 * Adds a TransitionDrawable to the AutomataDrawable.
	 * @param controlPoints are the control points of the TransitionDrawable. If the vector is empty, a default control point is added
	 * @param up indicates if the arc of the TransitionDrawable must drawn up or down
	 * @param transition is the Transition linked with the TransitionDrawable
	 * @return the TransitionDrawable adds to the AutomataDrawable
	 * @see Transition
	 * @see TransitionDrawable
	 */
	TransitionDrawable *addTransitionToScene(QVector<QPointF *> &controlPoints,
	                                         bool up,
	                                         Transition &transition);

	/**
	 * Method.
	 * Removes a TransitionDrawable from the AutomataDrawable. And it's Transition linked with from the Automata.
	 * @param transition is the TransitionDrawable to remove
	 * @see Automata
	 * @see Transition
	 * @see TransitionDrawable
	 */
	void removeTransitionFromAutomata(TransitionDrawable &transition);

	/**
	 * Method.
	 * Adds TransitionDrawable and Transition it's Transition linked to respectively the AutomataDrawable and it's Automata. An arrow will be draw on the left of the TransitionDrawable.
	 * @see Automata	 
	 * @see Transition
	 * @see TransitionDrawable
	 */
	void addLeftTransition();

	/**
	 * Methods
	 * Edits parameters for the new Transition.
	 */
	void setPreviewName(std::string name);

	void setPreviewSource(StateDrawable &s);

	void setPreviewDestination(StateDrawable &s);

	bool isDefinePreviewSource();

	bool isDefinePreviewDestination();

	void endTransitionPreview();

	/**
	 * Method.
	 * Adds a TransitionDrawable which will be linked with an initial StateDrawable to the AutomataDrawable. And adds it's Transition linked to the Automata.
	 * @param control_points are the control points of the TransitionDrawable. If the vector is empty, a default control point is added
	 * @param state is the initial StateDrawable to which the TransitionDrawable is linked with
	 * @return the TransitionDrawable adds to the AutomataDrawable
	 * @see Automata
	 * @see Transition
	 * @see TransitionDrawable
	 * @see StateDrawable
	 */
	TransitionDrawable *addInitialTransitionToScene(QPointF *controlPoint, StateDrawable &state);

	/**
	 * Method.
	 * Deselects all current items selected and places the default SelectableGroup as current SelectableGroup selected in the AutomataDrawable.
	 * @see SelectableGroup
	 */
	void emptySelectedItems();

	/**
	 * Method.
	 * Selects all the items present in an area defined by a rectangle and places them in the current SelectableGroup instead of the previous elements. If MAJ is pressed, the items selected are added to the SelectableGroup if they are not already present, otherwise they are removed from it. 
	 * @param selectionRect is the area in which items are selected.
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectedItem(const QRectF &selectionRect);

	/**
	 * Method.
	 * Checks if a StateDrawable is in an area defined by a QRectF.
	 * @param selectionRect is the area in which the StateDrawable is searched
	 * @param state is the StateDrawable which is tested to be in the area
	 * @return true if at least one of the control points of the StateDrawable is in the QRectF area, false otherwise
	 * @see StateDrawable
	 */
	static bool contains(const QRectF &selectionRect, const StateDrawable &state);

	/**
	 * Method.
	 * Selects the items and places them in the current SelectableGroup instead of the previous elements. If MAJ is pressed, the items selected is added to the SelectableGroup if it is not already present, otherwise it is removed from it. 
	 * @param item is the Groupable studied
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectedItem(Groupable &item);

	/**
	 * Method.
	 * Saves a new SelectableGroup in the AutomataDrawable with the items of the current SelectableGroup. The StateGroup linked with it is added to the Automata linked with the AutomataDrawable. The new SelectableGroup created is placed as current SelectableGroup.
	 * @see Automata
	 * @see StateGroup
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void createGroupWithSelectedItems();

	/**
	 * Method.
	 * Deletes the current SelectableGroup from the AutomataDrawable (if it's not the default SelectableGroup of the AutomataDrawable). The StateGroup linked with it is deletes from the Automata link with the AutomataDrawable. The default SelectableGroup is placed as current SelectableGroup.
	 * @see Automata
	 * @see StateGroup
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void delSelectedGroup();

	/**
	 * Method.
	 * Changes the current SelectableGroup of the AutomataDrawable with the SelectableGroup of the index i in the list of SelectableGroups created by user.
	 * @param groupNumber is the index of the SelectableGroup to place as current SelectableGroup
	 * @see SelectableGroup
	 */
	void loadGroup(int groupNumber);

	/**
	 * Static Method.
	 * Returns the first item selected in the current SelectableGroup of the AutomataDrawable.
	 * @return the first item selected in the current SelectableGroup
	 * @see SelectableGroup
	 */
	static Groupable *getSelectedItem();

	/** 
	 * Static Method.
	 * Returns the number of items in the current SelectableGroup of the AutomataDrawable.
	 * @return the number of items in the current SelectableGroup
	 * @see SelectableGroup
	 */
	static int getNumSelectedItems();

	/**
	 * Method.
	 * Returns the current SelectableGroup of the AutomataDrawable.
	 * @return the current SelectableGroup
	 * @see SelectableGroup
	 */
	static SelectableGroup *getSelectedItems();

	/**
	 * Method.
	 * Returns the number of SelectableGroups created in the AutomataDrawable.
	 * @return the number of SelectableGroups created
	 * @see SelectableGroup
	 */
	int getNumSelectableGroups() const;

	/**
	 * Method.
	 * Returns the SelectableGroup at the index id from the list of SelectedGroups created in the AutomataDrawable.
	 * @param id is the index of the SelectableGroup in the list
	 * @return the SelectableGroup at the index id
	 * @see SelectableGroup
	 */
	SelectableGroup *getSelectableGroup(int id);

	/**
	 * Method.
	 * Returns the current SelectableGroup of the AutomataDrawable
	 * @return the current SelectableGroup
	 * @see SelectableGroup
	 */
	SelectableGroup *getCurrentSelectableGroup();

	/**
	 * Method.
	 * Calls the move function of all items selected in the AutomataDrawable.
	 * @param event is the DragEvent linked with the movement
	 * @see DragEvent
	 */
	void buttonMoved(DragEvent &event);

	/**
	 * Method.
	 * Useless function but must be implements because of the inheritance with Drawable
	 * @see Drawable
	 */
	QPen *getChosenPen() const override;

	/**
	 * Method.
	 * Useless function but must be implements because of the inheritance with Drawable
	 * @see Drawable
	 */
	QBrush *getChosenBrush() const override;

	/**
	 * Method.
	 * Useless function but must be implements because of the inheritance with Drawable
	 * @see Drawable
	 */
	void updateLocalStyle() override;

	/**
	 * Method.
	 * Useless function but must be implements because of the inheritance with Drawable
	 * @see Drawable
	 */
	void loadLocalStyle() override;

signals:
	/** 
	 * Method.
	 * This signals is emitted when the selected item changes. The connexion is made in the AutomataController
	 * @see AutomataController
	 */
	void selectedItemChanged(Selectable &item);

	/** 
	 * Method.
	 * This signals is emitted when the selected item is emptied. The connexion is made in the AutomataController
	 * @see AutomataController
	 */
	void selectedItemEmpty();

};
}  // namespace VisualAutomata

#endif
