#include <QGraphicsTextItem>
#include <QGraphicsSceneDragDropEvent>

#include "automata_scene.hpp"

#include <QApplication>
#include <QDebug>

namespace VisualAutomata {

bool AutomataScene::controlKey = false;

bool AutomataScene::mousePressedSelectRect = false;

AutomataScene::AutomataScene(QWidget *parent, int x, int y, int width, int height) :
		QGraphicsScene(parent),
		automataDrawable(new AutomataDrawable(*this)),
		currentAction(no_action) {

	setSceneRect(x, y, width, height);

	initialPoint = std::make_unique<QPointF>(0, 0);
	finalPoint = std::make_unique<QPointF>(0, 0);
	selectionRect = std::make_unique<QGraphicsRectItem>(0, 0, 0, 0);
	selectionRect->setBrush(QColor(0, 0, 0, 35));
	selectionRect->setZValue(20);
	addItem(selectionRect.get());
}

AutomataScene::~AutomataScene() = default;

AutomataDrawable *AutomataScene::getAutomataDrawable() const {
	return automataDrawable;
}

void AutomataScene::redraw() {
	automataDrawable->update();
}

void AutomataScene::setCurrentAction(enum ViewportAction action) {
	currentAction = action;
}

void AutomataScene::addState(QPointF &center, float radius) {
	auto *s = new State();
	automataDrawable->addStateToAutomata(*s);
	StateDrawable *sd = automataDrawable->addStateToScene(center, radius, *s);
	sd->buttonSelected();
}

void AutomataScene::addInitialState(QPointF &center, float radius) {
	auto *s = new State();
	s->setInitial(true);
	automataDrawable->addStateToAutomata(*s);
	StateDrawable *sd = automataDrawable->addStateToScene(center, radius, *s);
	sd->buttonSelected();
}

void AutomataScene::addFinalState(QPointF &center, float radius) {
	auto *s = new State();
	s->setFinal(true);
	automataDrawable->addStateToAutomata(*s);
	StateDrawable *sd = automataDrawable->addStateToScene(center, radius, *s);
	sd->buttonSelected();
}

ViewportAction AutomataScene::getCurrentAction() {
	return currentAction;
}

void AutomataScene::keyPressEvent(QKeyEvent *keyEvent) {
	if (keyEvent->key() == Qt::Key_Shift) {
		AutomataScene::controlKey = true;
	}
}

void AutomataScene::keyReleaseEvent(QKeyEvent *keyEvent) {
	if (keyEvent->key() == Qt::Key_Shift) {
		AutomataScene::controlKey = false;
	}
}

void AutomataScene::mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) {
	auto *p = new QPointF(mouseEvent->scenePos().x(), mouseEvent->scenePos().y());

	switch (currentAction) {
	case no_action : initialPoint->setX(mouseEvent->scenePos().x());
		initialPoint->setY(mouseEvent->scenePos().y());

		mousePressedSelectRect = true;
		break;

	case action_add_state : this->addState(*p);
		currentAction = no_action;
		break;

	case action_add_initial_state : this->addInitialState(*p);
		currentAction = no_action;
		break;

	case action_add_final_state : this->addFinalState(*p);
		currentAction = no_action;
		break;

	case action_add_transition : break;
	}

	QApplication::restoreOverrideCursor();

	QGraphicsScene::mousePressEvent(mouseEvent);
}

void AutomataScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) {
	if (mousePressedSelectRect) {
		mousePressedSelectRect = false;

		if (automataDrawable != nullptr) {
			automataDrawable->setSelectedItem(selectionRect->rect());
		}
		selectionRect->setRect(0, 0, 0, 0);
	}

	QGraphicsScene::mouseReleaseEvent(mouseEvent);
}

void AutomataScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) {

	if (mousePressedSelectRect) {
		finalPoint->setX(mouseEvent->scenePos().x());
		finalPoint->setY(mouseEvent->scenePos().y());
		selectionRect->setRect(std::min(initialPoint->x(), finalPoint->x()),
		                       std::min(initialPoint->y(), finalPoint->y()),
		                       std::max(initialPoint->x(), finalPoint->x())
				                       - std::min(initialPoint->x(), finalPoint->x()),
		                       std::max(initialPoint->y(), finalPoint->y())
				                       - std::min(initialPoint->y(), finalPoint->y()));
	}

	QGraphicsScene::mouseMoveEvent(mouseEvent);
}

QGraphicsScene *AutomataScene::getScene() {
	return this;
}
}  // namespace VisualAutomata
