#ifndef VISUAL_AUTOMATA_AUTOMATA_SCENE_HPP
#define VISUAL_AUTOMATA_AUTOMATA_SCENE_HPP

#include <QGraphicsView>
#include <QPointer>

#include "automata_drawable.hpp"
#include "state.hpp"
#include "transition_drawable.hpp"
#include "viewport_action.hpp"

namespace VisualAutomata {

/**
 * Defines a scene in which an AutomataDrawable can be drawn.
 */
class AutomataScene: public QGraphicsScene {
	/**< The automata which is drawn in the scene */
	QPointer<AutomataDrawable> automataDrawable;

	/**< A rectangle which defines a selected area. All items in this area will be selected. */
	std::unique_ptr<QGraphicsRectItem> selectionRect;
	/**< Minimum point of the selection rectangle */
	std::unique_ptr<QPointF> initialPoint;
	/**< Maximum point of the selection rectangle */
	std::unique_ptr<QPointF> finalPoint;
	/**< The action made by the Viewport */
	enum ViewportAction currentAction;

public:
	/**< If a selection rectangle is defined */
	static bool mousePressedSelectRect;
	/**< If Shift key is pressed */
	static bool controlKey;

	/**
	 * Constructor.
	 * @param parent is the parent widget of the scene.
	 * @param x is the abscissa of the scene.
	 * @param y is the ordinate of the scene.
	 * @param width is the width of the scene.
	 * @param height is the height of the scene.
	 */
	AutomataScene(QWidget *parent, int x, int y, int width, int height);

	/**
	 * Destructor.
	 */
	~AutomataScene() override;

	/**
	 * Method.
	 * Updates all the components of the AutomataDrawable in the AutomataScene.
	 * @see AutomataDrawable
	 */
	void redraw();

	void setCurrentAction(enum ViewportAction action);

	ViewportAction getCurrentAction();

	/**
	 * Method.
	 * Adds a StateDrawable into the AutomataDrawable linked with the scene.
	 * @param center is the coordinates of the center of the StateDrawable in the AutomataScene
	 * @param radius is the radius of the StateDrawable
	 * @see AutomataDrawable
	 * @see StateDrawable
	 */
	void addState(QPointF &center, float radius = 50);

	/**
	 * Method.
	 * Adds an initial StateDrawable into the AutomataDrawable linked with the scene.
	 * @param center is the coordinates of the center of the StateDrawable in the AutomataScene
	 * @param radius is the radius of the StateDrawable
	 * @see AutomataDrawable
	 * @see StateDrawable
	 */
	void addInitialState(QPointF &center, float radius = 50);

	/**
     * Method.
     * Adds a final StateDrawable into the AutomataDrawable linked with the scene.
	 * @param center is the coordinates of the center of the StateDrawable in the AutomataScene
	 * @param radius is the radius of the StateDrawable
	 * @see AutomataDrawable
	 * @see StateDrawable
     */
	void addFinalState(QPointF &center, float radius = 50);

	/**
	 * Method.
	 * Checks if MAJ is pressed
	 * @param keyEvent is the parameter linked with the event
	 */
	void keyPressEvent(QKeyEvent *keyEvent) override;

	/**
	 * Method.
	 * Checks if MAJ is released
	 * @param keyEvent is the parameter linked with the event
	 */
	void keyReleaseEvent(QKeyEvent *keyEvent) override;

	/**
	 * Method.
	 * Initialises the minimum and maximum points of the selection rectangle. Updates also currentAction.
	 * @param mouseEvent is the parameter linked with the event
	 */
	void mousePressEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

	/**
	 * Method.
	 * Updates the selected items of the AutomataDrawable according to the selection rectangle.
	 * @param mouseEvent is the parameter linked with the event
	 * @see AutomataDrawable
	 */
	void mouseReleaseEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

	/**
	 * Method.
	 * Updates the selection rectangle according to the mouse position.
	 * @param mouseEvent is the parameter linked with the event
	 */
	void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent) override;

	/**
	 * Method.
	 * Returns the AutomataDrawable linked with the AutomataScene
	 * @return the AutomataDrawable
	 */
	AutomataDrawable *getAutomataDrawable() const;

	/**
	 * Method.
	 * Returns the AutomataScene
	 * @return the AutomataScene
	 */
	QGraphicsScene *getScene();
};
}  // namespace VisualAutomata

#endif
