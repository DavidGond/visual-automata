#include "state_drawable.hpp"
#include "viewport_action.hpp"
#include "automata_scene.hpp"
#include <cassert>

#include <QDebug>
#include <cmath>

namespace VisualAutomata {
QPen *StateDrawable::preferencePen = new QPen();

QBrush *StateDrawable::preferenceBrush = new QBrush(Qt::white, Qt::SolidPattern);

StateDrawable::StateDrawable(QPointF &center,
                             float radius,
                             State &state,
                             AutomataDrawable &automataDrawable) :
		Draggable(STATE),
		center(&center),
		radius(radius),
		scene(nullptr),
		isInitial(false),
		isFinal(false) {
	dragButton = new DragButton(center.x(), (int) round(center.y() + radius), buttonSize, *this);
	dragButton->setColor(QColor(Qt::yellow));

	this->state = &state;
	this->automataDrawable = &automataDrawable;
	addDragButton(*dragButton);

	stateText = new QGraphicsTextItem(this->state->getLabel().c_str());
	stateText->setDefaultTextColor(Qt::GlobalColor::red);
	stateText->setPos(getCenter().x() - stateText->boundingRect().width() / 2,
	                  getCenter().y() - stateText->boundingRect().height() / 2);

	static int wdd = 0;
	id = wdd;
	wdd++;
	ellipse = new QGraphicsEllipseItem(getCenter().x() - getRadius(),
	                                   getCenter().y() - getRadius(),
	                                   2 * getRadius(),
	                                   2 * getRadius());
	ellipse->setPen(*getChosenPen());
	ellipse->setBrush(*getChosenBrush());

	ellipse->setZValue(drawingPriority);
	stateText->setZValue(drawingPriority + 1);

	createFinalTransition();
	createInitialTransition();

	setIsInitial(state.isInitial());
	setIsFinal(state.isFinal());

	localStyle = &state.getStyle();
	loadLocalStyle();
}

// Returns the center of the circle
QPointF &StateDrawable::getCenter() const {
	return *center;
}

// Returns the radius of the circle
float StateDrawable::getRadius() const {
	return radius;
}

State &StateDrawable::getData() const {
	return *state;
}

void StateDrawable::removeFromAutomata() {
	removeFromScene();
	removeDragButtonsFromAutomata(*scene);
}

int StateDrawable::getNbrLinked() const {
	return transitionsLinked.size();
}

void StateDrawable::linkWith(TransitionDrawable &object) {
	transitionsLinked.push_back(&object);
}

void StateDrawable::unlink(TransitionDrawable &object) {
	int i = transitionsLinked.indexOf(&object);
	assert(i != -1);
	transitionsLinked.remove(i);
}

void StateDrawable::deleteTransitionsLinked() {
	while (!transitionsLinked.empty()) {
		TransitionDrawable *t = transitionsLinked.last();
		// no need to call 'transitionsLinked.pop_back();' -> delTransitionsDrawable will do it
		automataDrawable->removeTransitionFromAutomata(*t);
	}
}

StateDrawable::~StateDrawable() {
	delete (center);
	delete (state);
	delete (automataDrawable);
	while (!transitionsLinked.empty()) {
		TransitionDrawable *t = transitionsLinked.last();
		transitionsLinked.pop_back();
		delete (t);

	}
	delete (stateText);
	delete (ellipse);
	delete (ellipse2);
	delete (scene);
	delete StateDrawable::preferencePen;
	delete StateDrawable::preferenceBrush;
	delete chosenPen;
	delete chosenBrush;
}

void StateDrawable::move(DragEvent &dragEvent) {
	int dx = dragEvent.getDeltaX();
	int dy = dragEvent.getDeltaY();

	center->setX(center->x() + dx);
	center->setY(center->y() + dy);

	for (int i = 0; i < getNbDragButton(); i++) {
		getDragButton(i)->relativeMove(dx, dy);
	}

	for (auto &i : transitionsLinked) {
		if (!i->isSelected()) {
			if (i->getIsInitial()) {
				i->moveInitialTransition(dx, dy, *dragEvent.getSource());
			} else {
				State &source = i->getData()->getSource();
				State &destination = i->getData()->getDestination();

				if (&source != &destination) {
					i->moveRelativelyToStates(dx, dy, *this);
				} else {
					i->directedMove(dx, dy);
				}
				emit i->localStyleChanged();
			}
		}
	}

	emit localStyleChanged();
}

void StateDrawable::update() {
	ellipse->setBrush(*getChosenBrush());

	// Updates the position of the circle on the screen
	ellipse->setRect(getCenter().x() - getRadius(),
	                 getCenter().y() - getRadius(),
	                 2 * getRadius(),
	                 2 * getRadius());
	ellipse2->setRect(getCenter().x() - 0.75 * getRadius(),
	                  getCenter().y() - 0.75 * getRadius(),
	                  1.5 * getRadius(),
	                  1.5 * getRadius());
	stateText->setPos(getCenter().x() - stateText->boundingRect().width() / 2,
	                  getCenter().y() - stateText->boundingRect().height() / 2);

	if (isSelected()) {
		QPen pen = *getChosenPen();
		pen.setColor(Qt::green);
		ellipse->setPen(pen);
		ellipse2->setPen(pen);
	} else {
		ellipse->setPen(*getChosenPen());
		ellipse2->setPen(*getChosenPen());
	}
	updateText();
}

void StateDrawable::buttonMoved(DragEvent &event) {
	automataDrawable->buttonMoved(event);
	automataDrawable->update();
}

void StateDrawable::buttonSelected() {
	automataDrawable->setSelectedItem(*this);
	auto *automataScene = (AutomataScene *) scene;
	if (automataScene->getCurrentAction() == action_add_transition) {
		if (!automataDrawable->isDefinePreviewSource()) {
			automataDrawable->setPreviewSource(*this);
		} else {
			assert(!automataDrawable->isDefinePreviewDestination());
			automataDrawable->setPreviewDestination(*this);
			automataDrawable->addLeftTransition();
		}
	}
}

void StateDrawable::updateText() {
	stateText->setPlainText(state->getLabel().c_str());

	stateText->setPos(getCenter().x() - stateText->boundingRect().width() / 2,
	                  getCenter().y() - stateText->boundingRect().height() / 2);
}

void StateDrawable::addAttachedGroup(SelectableGroup &group) {
	if (attachedGroups.contains(&group)) {
		return;
	}

	attachedGroups.push_back(&group);
}

void StateDrawable::removeAttachedGroup(SelectableGroup &group) {
	if (attachedGroups.contains(&group)) {
		attachedGroups.removeAll(&group);
	}
}

void StateDrawable::setPreferencePenColor(const QColor &color) {
	StateDrawable::preferencePen->setColor(color);
}

void StateDrawable::setPreferencePenSize(int size) {
	StateDrawable::preferencePen->setWidth(size);
}

void StateDrawable::setPreferencePenStyle(Qt::PenStyle &style) {
	StateDrawable::preferencePen->setStyle(style);
}

void StateDrawable::setPreferenceBrushColor(const QColor &color) {
	StateDrawable::preferenceBrush->setColor(color);
}

void StateDrawable::setIsInitial(bool initial) {
	if (this->isInitial && !initial) {
		removeInitialTransitionFromScene(true);
	} else if (!this->isInitial && initial) {
		if (scene != nullptr) {
			addInitialTransitionToScene();
		}
	}
	this->isInitial = initial;
	state->setInitial(initial);
}

void StateDrawable::createFinalTransition() {
	ellipse2 = new QGraphicsEllipseItem(getCenter().x() - 0.75 * getRadius(),
	                                    getCenter().y() - 0.75 * getRadius(),
	                                    1.5 * getRadius(),
	                                    1.5 * getRadius());
	ellipse2->setZValue(drawingPriority);
	ellipse2->setPen(*getChosenPen());
	ellipse2->setBrush(Qt::NoBrush);
}

void StateDrawable::createInitialTransition() {
	initialTransition = automataDrawable->addInitialTransitionToScene(nullptr, *this);
	auto *d = new DragButton(initialTransition->getInitialPoint().x(),
	                         initialTransition->getInitialPoint().y(),
	                         buttonSize,
	                         *initialTransition);
	initialTransition->addDragButton(*d);
}

void StateDrawable::removeInitialTransitionFromScene(bool removeDragButtons) {
	initialTransition->removeFromScene();
	if (removeDragButtons) {
		initialTransition->removeDragButtonsFromScene(*scene);
	}
}

void StateDrawable::addInitialTransitionToScene() {
	initialTransition->addToScene();
	initialTransition->addDragButtonsToScene(*scene);
}

void StateDrawable::removeFinalTransitionFromScene() {

	scene->removeItem(ellipse2);
}

void StateDrawable::addFinalTransitionToScene() {
	scene->addItem(ellipse2);
}

void StateDrawable::removeFromScene() {
	if (getIsVisibleInScene()) {
		scene->removeItem(ellipse);
		if (isFinal) {
			removeFinalTransitionFromScene();
		}
		if (isInitial) {
			removeInitialTransitionFromScene(false);
		}
		scene->removeItem(stateText);
		Drawable::removeFromScene();
	}
}

void StateDrawable::addToScene() {
	if (!getIsVisibleInScene()) {
		scene->addItem(ellipse);
		if (isFinal) {
			addFinalTransitionToScene();
		}
		if (isInitial) {
			addInitialTransitionToScene();
		}
		scene->addItem(stateText);
		Drawable::addToScene();
	}
}

void StateDrawable::setScene(QGraphicsScene &newScene) {
	this->scene = &newScene;

	addToScene();

	addDragButtonsToScene(newScene);
}

void StateDrawable::setIsFinal(bool final) {
	if (!this->isFinal && final) {
		if (scene != nullptr) {
			addFinalTransitionToScene();
		}
	} else if (this->isFinal && !final) {
		removeFinalTransitionFromScene();
	}

	this->isFinal = final;
	state->setFinal(final);
}

bool StateDrawable::getIsFinal() const {
	return isFinal;
}

bool StateDrawable::getIsInitial() const {
	return isInitial;
}

QPen *StateDrawable::getChosenPen() const {
	if (getHasLocalStyle()) {
		return getLocalPen();
	} else {
		int priority = -1000000;
		QPen *newPen = nullptr;
		for (SelectableGroup *item : attachedGroups) {
			if (item->getApplyStyle() && item->getPriority() > priority) {
				newPen = item->getPen();
				priority = item->getPriority();
			}
		}
		if (newPen != nullptr) {
			return newPen;
		}
	}

	return preferencePen;
}

QBrush *StateDrawable::getChosenBrush() const {
	if (getHasLocalStyle()) {
		return getLocalBrush();
	} else {
		int priority = -1000000;
		QBrush *newBrush = nullptr;

		for (SelectableGroup *group : attachedGroups) {
			if (group->getApplyStyle() && group->getPriority() > priority) {
				newBrush = group->getBrush();
				priority = group->getPriority();
			}
		}
		if (newBrush != nullptr) {
			return newBrush;
		}
	}
	return preferenceBrush;
}

void StateDrawable::updateLocalStyle() {
	localStyle->saveBool(Style::hasLocalStyleAttribute, getHasLocalStyle());
	if (getHasLocalStyle()) {
		localStyle->savePen(*localPen);
		localStyle->saveBrush(*localBrush);
	}

	localStyle->saveInt(Style::stateXAttribute, center->x());
	localStyle->saveInt(Style::stateYAttribute, center->y());
}

void StateDrawable::loadLocalStyle() {

	setHasLocalStyle(localStyle->loadBool(Style::hasLocalStyleAttribute, getHasLocalStyle()));
	if (getHasLocalStyle()) {
		Drawable::initializePens();
		localStyle->loadPen(*localPen);
		localStyle->loadBrush(*localBrush);
	}
	if (Style::useCoordinates) {
		center->setX(localStyle->loadFloat(Style::stateXAttribute, center->x()));
		center->setY(localStyle->loadFloat(Style::stateYAttribute, center->y()));

		dragButton->moveTo(center->x() - dragButton->width() / 2.0,
		                   center->y() + radius - dragButton->height() / 2.0);

		initialTransition->getInitialPoint().setX(getCenter().x() - 2 * getRadius());
		initialTransition->getInitialPoint().setY(getCenter().y());
		initialTransition->getDragButton(0)
		                 ->moveTo(getCenter().x() - 2 * getRadius()
				                          - initialTransition->getDragButton(0)->width() / 2.0,
		                          getCenter().y()
				                          - initialTransition->getDragButton(0)->height() / 2.0);
	}
	update();
}
}  // namespace VisualAutomata
