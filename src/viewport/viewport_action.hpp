#ifndef VISUAL_AUTOMATA_VIEWPORT_ACTION_HPP
#define VISUAL_AUTOMATA_VIEWPORT_ACTION_HPP

namespace VisualAutomata {
/**
 * Defines an action for a viewport
 */
enum ViewportAction {
	no_action,
	action_add_state,
	action_add_initial_state,
	action_add_final_state,
	action_add_transition
};
}  // namespace VisualAutomata
#endif
