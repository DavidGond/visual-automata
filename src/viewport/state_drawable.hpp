#ifndef VISUAL_AUTOMATA_STATE_DRAWABLE_HPP
#define VISUAL_AUTOMATA_STATE_DRAWABLE_HPP

#include <QGraphicsView>
#include <QVector2D>

#include "drag_event.hpp"
#include "drag_button.hpp"
#include "selectable.hpp"
#include "drawable.hpp"
#include "transition_drawable.hpp"
#include "automata_drawable.hpp"
#include "style.hpp"

namespace VisualAutomata {

class TransitionDrawable;

class AutomataDrawable;

/**
 * StateDrawable is the graphical representation of a State.
 * @see State
 */
class StateDrawable: public Drawable, public Draggable {
	/**< The size of the DragButton associated with the StateDrawable */
	static const int buttonSize = 10;
	/**< The drawing priority in the scene */
	static const int drawingPriority = 5;

	/**< The center of the StateDrawable circle */
	QPointF *center;
	/**< The radius of the stateDrawable circle */
	float radius;

	/**< DragButton linked with the StateDrawable */
	DragButton *dragButton;

	/**< State link with the StateDrawable */
	State *state;
	/**< TransitionsDrawables linked with the StateDrawable */
	QVector<TransitionDrawable *> transitionsLinked;
	/**< AutomataDrawable in which the StateDrawable is */
	AutomataDrawable *automataDrawable;

	/**< The StateDrawable name. */
	QGraphicsTextItem *stateText;
	/**< The ellipse object which represents the StateDrawable. */
	QGraphicsEllipseItem *ellipse;
	/**< The second ellipse, only for final states */
	QGraphicsEllipseItem *ellipse2{};
	/**< Scene in which the StateDrawable is drawn */
	QGraphicsScene *scene{};

	/**< chosen Pen used in setScene method */
	QPen *chosenPen{};
	/**< chosen Brush used in setScene method */
	QBrush *chosenBrush{};

	/**< If the StateDrawable is initial */
	bool isInitial;
	/**< If the StateDrawable is final */
	bool isFinal;
	/**< The initial TransitionDrawables linked with this StateDrawable */
	TransitionDrawable *initialTransition{};

public:
	int id;

	/**
	 * Constructor.
	 * @param center is the coordinates of the center of the StateDrawable.
	 * @param radius is the radius of the StateDrawable.
	 * @param state is the State linked with this StateDrawable
	 * @param automataDrawable is the AutomataDrawable in which the StateDrawable is
	 * @see AutomataDrawable
	 */
	StateDrawable(QPointF &center, float radius, State &state, AutomataDrawable &automataDrawable);

	/**
	 * Destructor.
	 */
	~StateDrawable() override;

	/**
	 * Method.
	 * Returns the center of the StateDrawable.
     * @return the center of the StateDrawable
	 */
	QPointF &getCenter() const;

	/**
	 * Method.
	 * Returns the radius of the StateDrawable.
     * @return return the radius of the StateDrawable
	 */
	float getRadius() const;

	/**
	 * Method.
	 * Returns the State associated with this object.
	 * @return the State associated with this object
	 * @see State
	 */
	State &getData() const;

	/**
	 * Method.
	 * Returns the number of TransitionDrawables linked with the StateDrawable.
	 * @return the number of TransitionDrawables linked with the StateDrawable
	 * @see TransitionDrawable
	 */
	int getNbrLinked() const;

	/**
	 * Method.
	 * Adds a new TransitionDrawable linked with the StateDrawable.
	 * @param object is the TransitionDrawable to link with
	 * @see TransitionDrawable
	 */
	void linkWith(TransitionDrawable &object);

	/**
	 * Method.
	 * Unlinks the given TransitionDrawable from this StateDrawable.
	 * @param object is the TransitionDrawable to unlink
	 * @see TransitionDrawable
	 */
	void unlink(TransitionDrawable &object);

	/**
	 * Method.
	 * Deletes and removes from scene all TransitionDrawables linked with the StateDrawable.
	 * @see TransitionDrawable
	 */
	void deleteTransitionsLinked();

	/**
	 * Method.
	 * Moves the StateDrawable and its linked TransitionDrawables according to the DragEvent.
	 * @param dragEvent stores information about the movement
	 * @see TransitionDrawable
	 * @see DragEvent
	 */
	void move(DragEvent &dragEvent) override;

	/**
	 * Method.
	 * Updates the position of the StateDrawable and its characteristics such as its Pen and its Brush.
	 */
	void update() override;

	/**
	 * Method.
	 * Updates StateDrawable name.
	 */
	void updateText();

	/**
	 * Method.
	 * Attaches the StateDrawable to a SelectableGroup.
	 * @param group is the SelectableGroup in which the StateDrawable will be attached
	 * @see SelectableGroup
	 */
	void addAttachedGroup(SelectableGroup &group) override;

	/**
	 * Method.
	 * Removes the StateDrawable from a SelectableGroup.
	 * @param group is the SelectableGroup from which the StateDrawable will be removed
	 * @see SelectableGroup
	 */
	void removeAttachedGroup(SelectableGroup &group) override;

	/**
	 * Method.
	 * Calls the buttonMoved function of the linked AutomataDrawable and its update function.
	 * @param event stores information about the movement
	 */
	void buttonMoved(DragEvent &event) override;

	/**
	 * Method.
	 * Sets if the StateDrawable is initial or not
	 * @param initial defines if the state if initial or not
	 */
	void setIsInitial(bool initial);

	/**
	 * Method.
	 * Returns if the StateDrawable is initial or not.
	 * @return if the StateDrawable is initial or not
	 */
	bool getIsInitial() const;

	/**
	 * Method.
	 * Sets if the StateDrawable is final or not
	 * @param final defines if the state if final or not
	 */
	void setIsFinal(bool final);

	/**
	 * Method.
	 * Returns if the StateDrawable is final or not.
	 * @return if the StateDrawable is final or not
	 */bool getIsFinal() const;

	/**
	 * Method.
	 * Creates the circle for a final state. Doesn't add it to the scene.
	 */
	void createFinalTransition();

	/**
	 * Method.
	 * Creates the initial TransitionDrawable of the state. Adds it to the scene
	 * @see TransitionDrawable
	 */
	void createInitialTransition();

	/**
	 * Method.
	 * Removes the initial TransitionDrawable from the scene but doesn't destruct the object.
	 * @param removeDragButtons if the DragButtons of the initial transition must be removed or not.
	 * @see DragButton
	 * @see TransitionDrawable
	 */
	void removeInitialTransitionFromScene(bool removeDragButtons);

	/** 
	 * Method.
	 * Adds the initial TransitionDrawable to the AutomataScene if it isn't already added.
	 * @see AutomataScene
	 * @see TransitionDrawable
	 */
	void addInitialTransitionToScene();

	/** 
	 * Method.
	 * Removes the circle drawing for a an initial state from the AutomataScene but doesn't destruct it.
	 * @see AutomateScene
	 */
	void removeFinalTransitionFromScene();

	/** 
	 * Method.
	 * Adds the circle of a final to the AutomataScene if it isn't already added.
	 * @see AutomataScene
	 */
	void addFinalTransitionToScene();

	/** 
	 * Method.
	 * Removes the StateDrawable from the AutomataScene but doesn't remove it from the Automata.
	 * Removes the initial TransitionDrawable and the circle for a final state if they are visible.
	 * Doesn't remove the DragButtons from the AutomataScene
	 * @see AutomataScene
	 * @see Automata
	 * @see TransitionDrawable
	 */
	void removeFromScene() override;

	/** 
	 * Method.
	 * Adds the StateDrawable from the AutomataScene but doesn't add it to the Automata.
	 * Adds the initial TransitionDrawable and/or the circle for a final state if the state is final/initial.
	 * Doesn't add the DragButtons to the AutomataScene
	 * @see AutomataScene
	 * @see Automata
	 * @see TransitionDrawable
	 */
	void addToScene() override;

	/**
	 * Method.
	 * Initialises and draw the components of the StateDrawable.
	 * @param newScene is the scene in which components are drawn
	 */
	void setScene(QGraphicsScene &newScene);

	/**
	 * Method.
	 * Notifies the AutomataDrawable linked that this element is selected.
	 * @see AutomataDrawable
	 */
	void buttonSelected() override;

	/**
	 * Method
	 * Remove the StateDrawable from the AutomataDrawable.
	 * @see AutomataDrawable
	 */
	void removeFromAutomata();

	/**
	 * Method.
	 * Changes the color of the preference Pen.
	 * @param color is the new color of the preference Pen
	 */
	static void setPreferencePenColor(const QColor &color);

	/**
	 * Method.
	 * Changes the size of the preference Pen.
	 * @param size is the new size of the preference Pen
	 */
	static void setPreferencePenSize(int size);

	/**
	 * Method.
	 * Changes the PenStyle of the preference Pen.
	 * @param style is the new style of the preference Pen
	 */
	static void setPreferencePenStyle(Qt::PenStyle &style);

	/**
	 * Method.
	 * Changes the color of the preference Brush.
	 * @param color is the new color of the preference Brush
	 */
	static void setPreferenceBrushColor(const QColor &color);

	/**
	 * Method.
	 * Returns the Pen chosen for drawing the StateDrawable circle line.
	 * If there is no local Pen returns the SelectableGroup Pen with the higher priority, and if there is no group, returns the preference Pen.
	 * @return the Pen chosen for drawing the StateDrawable
	 */
	QPen *getChosenPen() const override;

	/**
	 * Method.
	 * Returns the pen chosen for the background of the StateDrawable
	 * If there is no local Brush returns the SelectableGroup Brush with the higher priority, and if there is no group, returns the preference Brush.
	 * @return the Pen chosen for drawing the StateDrawable
	 */
	QBrush *getChosenBrush() const override;

	/** 
	 * Method.
	 * Updates the local style of the state (Line Color, Background Color, Line Style, Line Width and position)
	 * It is called when a property of the state is changed in the AutomataController
	 * @see AutomataController
	 */
	void updateLocalStyle() override;

	/**
	 * Method.
	 * Loads the local style of the state (Line Color, Background Color, Line Style, Line Width and position)
	 * It is called is in the constructor of the StateDrawable
	 */
	void loadLocalStyle() override;

private:
	// The pen and brush defined in the preference window.
	/**< Default Pen shared by all StateDrawables */
	static QPen *preferencePen;
	/**< Default Brush shared by all StateDrawables */
	static QBrush *preferenceBrush;
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_STATE_DRAWABLE_HPP
