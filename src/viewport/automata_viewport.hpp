#ifndef VISUAL_AUTOMATA_AUTOMATA_VIEWPORT_HPP
#define VISUAL_AUTOMATA_AUTOMATA_VIEWPORT_HPP

#include <QGraphicsView>
#include <Qt>
#include <QApplication>
#include <QShortcut>
#include <cmath>
#include <QPointer>
#include "automata_scene.hpp"

namespace VisualAutomata {

/**
 * This class is the container of the scenes in which AutomataDrawable will be displayed.
 */
class AutomataViewport: public QWidget {
Q_OBJECT
	/**< The currently drawn scene. */
	AutomataScene *currentScene;
	/**< The view in which scenes are displayed. */
	QPointer<QGraphicsView> view;
	/**< The factor for the zoom transformation. */
	static qreal zoomFactor;

public:
	/**
	 * Constructor.
	 * @param parent is the parent widget of the QGraphicsView.
	 */
	explicit AutomataViewport(QWidget *parent);

	/**
	 * Destructor.
	 */
	~AutomataViewport() override;

	/**
	 * Method.
	 * Updates the AutomataDrawable of the AutomataScene linked with the AutomataViewport.
	 * @see AutomataScene
	 * @see AutomataViewport
	 */
	void redraw() const;

	/**
	 * Method.
	 * Zooms in the view.
	 */
	void zoomIn();

	/**
	   * Method.
	   * Zooms out from the view.
	 */
	void zoomOut();

	/**
	 * Method.
	 * Returns to the default zoom value.
	 */
	void zoomActualSize();

	/**
	 * Method.
	 * Deletes a StateDrawable from the AutomataDrawable of the current AutomataScene.
	 * @param s is the StateDrawable to delete
	 * @see AutomataScene
	 * @see AutomataDrawable
	 * @see StateDrawable
	 */
	void removeStateFromAutomata(StateDrawable &s);

	/**
	 * Method.
	 * Deletes a TransitionDrawable from the AutomataDrawable of the current AutomataScene.
	 * @param t is the TransitionDrawable to delete
	 * @see AutomataScene
	 * @see AutomataDrawable
	 * @see TransitionDrawable
	 */
	void removeTransitionFromAutomata(TransitionDrawable &t);

	void enterEvent(QEvent *mouseEvent) override;

	void leaveEvent(QEvent *mouseEvent) override;

	/**
	 * Method.
	 * Returns the AutomataScene linked with the AutomataViewport
	 * @return the AutomataScene
	 * @see AutomataScene
	 */
	AutomataScene *getScene();

	/**
	   * Method.
	   * Zooms the AutomataViewport by a zoom factor defined in the class (zoomFactor)
	   */
	void zoomByFactor();

	/**
	 * Method.
	 * Zooms using the mouse's wheel
	 */
	void wheelEvent(QWheelEvent *event) override;

	/**
	 * Method.
	 * Updates the zoom when PLUS, MINUS or ZERO keys are pressed.
	 * @param event is the parameter linked with the Event
	 */

	void keyPressEvent(QKeyEvent *event) override;

	/**
	  * Method.
	  * Sets a squared background brush for the AutomataViewport
	  */
	void setSquareBackground();

	/**
	  * Method.
	  * Sets a triangular background brush for the AutomataViewport
	  */
	void setTriangularBackground();

	/**
	  * Method.
	  * Sets a crossed background brush for the AutomataViewport
	  */
	void setCrossBackground();

	/**
	  * Method
	  * Sets a blank background brush for the AutomataViewport
	  */
	void setBlankBackground();
};
}  // namespace VisualAutomata

#endif //VISUAL_AUTOMATA_AUTOMATA_VIEWPORT_HPP
