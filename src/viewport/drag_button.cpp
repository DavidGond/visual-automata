#include <QPalette>
#include <cmath>
#include "drag_button.hpp"
#include "automata_scene.hpp"

namespace VisualAutomata {

DragButton::DragButton(int x, int y, int size, Draggable &object) : QPushButton() {
	this->setGeometry(x - size / 2, y - size / 2, size, size);
	this->setMinimumSize(size, size);

	this->setAutoFillBackground(true);
	setColor(QColor(Qt::blue));

	this->boundObjects.push_back(&object);
	dragEvent = new DragEvent();
	drag = false;

	locX = QPushButton::x();
	locY = QPushButton::y();
}

void DragButton::setColor(const QColor &c) {
	QPalette pal = this->palette();
	pal.setColor(QPalette::Button, c);
	this->setPalette(pal);
	this->update();
}

void DragButton::addBoundObject(Draggable &boundObject) {
	this->boundObjects.push_back(&boundObject);
}

void DragButton::moveTo(int x, int y) {
	auto dx = (float) (x - QPushButton::x());
	auto dy = (float) (y - QPushButton::y());

	locX += dx;
	locY += dy;
	QPushButton::move(x, y);
}

void DragButton::moveTo(double x, double y) {
	this->locX = x;
	this->locY = y;
	QPushButton::move((int) round(locX), (int) round(locY));
}

void DragButton::relativeMove(int dx, int dy) {
	moveTo(pos().x() + dx, pos().y() + dy);
}

void DragButton::mousePressEvent(QMouseEvent *e) {
	AutomataScene::mousePressedSelectRect = false;

	prevX = e->globalPos().x();
	prevY = e->globalPos().y();
}

void DragButton::mouseReleaseEvent(QMouseEvent *e) {
	(void) e;
	if (!drag) {
		for (auto &boundObject : boundObjects) {
			boundObject->buttonSelected();
		}
	}

	drag = false;
}

void DragButton::mouseMoveEvent(QMouseEvent *e) {
	drag = true;
	int x = e->globalPos().x();
	int y = e->globalPos().y();

	int deltaX = x - prevX;
	int deltaY = y - prevY;

	prevX = x;
	prevY = y;

	dragEvent->setValues(deltaX, deltaY, *this);

	for (auto &bound : boundObjects) {
		if (bound->isSelected()) {
			bound->buttonMoved(*dragEvent);
		} else if (!AutomataScene::controlKey) {
			for (auto &bound2 : boundObjects) {
				bound2->buttonSelected();
				bound2->buttonMoved(*dragEvent);
			}
		}
	}
}

double DragButton::locationX() const {
	return locX;
}

double DragButton::locationY() const {
	return locY;
}
}  // namespace VisualAutomata
