#include "drawable.hpp"
#include <QDebug>

namespace VisualAutomata {
Drawable::Drawable() {
	localBrush = nullptr;
	localPen = nullptr;
	isVisibleInScene = false;
	hasLocalStyle = false;
	localStyle = {};
	QObject::connect(this, SIGNAL(localStyleChanged()), this, SLOT(updateLocalStyle()));
}

Drawable::~Drawable() {
	delete localPen;
	delete localBrush;
}

void Drawable::setLocalPenColor(const QColor &color) {
	initializePens();
	localPen->setColor(color);
}

void Drawable::setLocalPenSize(int size) {
	initializePens();
	localPen->setWidth(size);
}

void Drawable::setLocalPenStyle(Qt::PenStyle &style) {
	initializePens();
	localPen->setStyle(style);
}

void Drawable::setLocalBrushColor(const QColor &color) {
	initializePens();
	localBrush->setColor(color);
}

QPen *Drawable::getLocalPen() const {
	return localPen;
}

QBrush *Drawable::getLocalBrush() const {
	return localBrush;
}

bool Drawable::getHasLocalStyle() const {
	return hasLocalStyle;
}

void Drawable::initializePens() {
	if (localPen == nullptr) {
		localPen = new QPen();
		localPen->setColor(Qt::black);
		localPen->setWidth(1);
	}

	if (localBrush == nullptr) {
		localBrush = new QBrush(Qt::black, Qt::SolidPattern);
	}

	hasLocalStyle = true;
}

void Drawable::addToScene() {
	isVisibleInScene = true;
}

void Drawable::removeFromScene() {
	isVisibleInScene = false;
}

bool Drawable::getIsVisibleInScene() const {
	return isVisibleInScene;
}

void Drawable::setHasLocalStyle(bool value) {
	hasLocalStyle = value;
}
}  // namespace VisualAutomata
