#include "selectable.hpp"

namespace VisualAutomata {

bool Selectable::isSelected() const {
	return selected;
}

void Selectable::setSelected(bool b) {
	selected = b;
}

enum SelectableType Selectable::getType() const {
	return type;
}

Selectable::~Selectable() = default;
}  // namespace VisualAutomata
