#include "draggable.hpp"

namespace VisualAutomata {

Draggable::Draggable(enum SelectableType type) :
		Groupable(type), areDragButtonsVisibleInScene(false) {
}

Draggable::~Draggable() {
	while (!dragButtons.empty()) {
		DragButton *b = dragButtons.last();
		dragButtons.pop_back();
		if (b != nullptr) {
			delete (b);
		}
	}

	while (!graphicsItems.empty()) {
		QGraphicsProxyWidget *b = graphicsItems.last();
		graphicsItems.pop_back();
		if (b != nullptr) {
			delete (b);
		}
	}
}

bool Draggable::getAreDragButtonsVisibleInScene() const {
	return areDragButtonsVisibleInScene;
}

void Draggable::addDragButton(DragButton &dragButton) {
	dragButtons.push_back(&dragButton);
	auto *b = new QGraphicsProxyWidget();
	b->setWidget(&dragButton);
	graphicsItems.push_back(b);
	b->setZValue(drawingPriority);
}

void Draggable::removeDragButtonsFromAutomata(QGraphicsScene &scene) {
	removeDragButtonsFromScene(scene);
	while (!dragButtons.isEmpty()) {
		dragButtons.pop_front();
	}
}

DragButton *Draggable::getDragButton(int i) const {
	return dragButtons[i];
}

QRectF Draggable::getDragButtonHitBox(int i) const {
	return dragButtons[i]->geometry();
}

int Draggable::getNbDragButton() const {
	return dragButtons.size();
}

void Draggable::moveDragButtons(int dx, int dy) {
	for (auto &dragButton : dragButtons) {
		dragButton->moveTo(dx, dy);
	}
}

void Draggable::addDragButtonsToScene(QGraphicsScene &scene) {
	if (!areDragButtonsVisibleInScene) {
		for (auto &graphicsItem : graphicsItems) {
			scene.addItem(graphicsItem);
		}
		areDragButtonsVisibleInScene = true;
	}
}

void Draggable::removeDragButtonsFromScene(QGraphicsScene &scene) {
	if (areDragButtonsVisibleInScene) {
		for (auto &item : graphicsItems) {
			scene.removeItem(item);
		}
		areDragButtonsVisibleInScene = false;
	}
}
}  // namespace VisualAutomata
