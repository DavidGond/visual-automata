#ifndef VISUAL_AUTOMATA_AUTOMATA_CONTROLLER_HPP
#define VISUAL_AUTOMATA_AUTOMATA_CONTROLLER_HPP

#include <QApplication>
#include <QDockWidget>
#include <QSettings>
#include "main_window.hpp"

#include <state_dock_widget.hpp>
#include <transition_dock_widget.hpp>
#include <left_dock_widget.hpp>
#include <group_dock_widget.hpp>
#include <tool_bar.hpp>
#include <menu_bar.hpp>
#include <preference_window.hpp>

#include <iostream>
#include <memory>

#include <state_drawable.hpp>
#include <automata_drawable.hpp>

using namespace VisualAutomata;

/**
 * AutomataController initializes the object of the interface
 * Does the connexion between the signals of the interface and ou slots functions
 */
class AutomataController: public QObject {
Q_OBJECT
private:
	/**< The MainWindow of the application */
	MainWindow *mainWindow;
	/**< The RightDockWidget of the state */
	StateDockWidget *stateDockWidget;
	/**< The RightDockWidget of the transition */
	TransitionDockWidget *transitionDockWidget;
	/**< The RightDockWidget of the group */
	GroupDockWidget *groupeDockWidget;
	/**< The LeftDockWidget containing the buttons to add a state/transition */
	LeftDockWidget *leftDockWidget;
	/**< An empty RightDockWidget */
	RightDockWidget *rightDockWidget;
	/**< The ToolBar of the interface */
	ToolBar *toolBar;
	/**< The MenuBar of the interface */
	MenuBar *menuBar;
	/**< The preference window */
	PreferenceWindow *preferences;
	void selectPositionState(ViewportAction action);
	AutomataViewport *getCurrentViewport();

public:
	/**
	 * Constructor.
	 * Initializes the objects of the interface.
     * Makes all the connexion between the interface signals and the wanted slots.
	 */
	AutomataController();

	/**
	 * Destructor.
	 */
	~AutomataController() override;

	/**
	 * Hides all RightDockWidget (Groupe State Transition and Empty).
	 * @see RightDockWidget
	 */
	void hideAllRightDockWidget();

public slots:
	/**
     * Hides all RightDockWidget and shows the TransitionDockWidget.
	 * @see RightDockWidget
	 * @see TransitionDockWidget
	 */
	void switchToTransitionDockWidget();

	/**
     * Hides all RightDockWidget and shows the StateDockWidget.
	 * @see RightDockWidget
	 * @see StateDockWidget
	 */
	void switchToStateDockWidget();

	/**
     * Hides all RightDockWidget and shows the empty dock widget (a RightDockWidget).
	 * @see RightDockWidget
	 */
	void switchToEmptyDockWidget();

	/**
     * Hides all RightDockWidget and shows the GroupDockWidget.
	 * @see RightDockWidget
	 * @see GroupDockWidget
	 */
	void switchToGroupDockWidget();

	/**
     * Opens the preferences window.
	 * @see PreferenceWindow
	 */
	void displayPreferencesWindow();

	void openFontDialog();

	/** 
	 * Loads all data from the preferences file.
	 */
	void loadPreferenceData();

	/**
     * Updates the drawing of the AutomataDrawable by calling the update() function.
	 * @see AutomataDrawable
	 */
	void updateDrawing() const;

	/**
	 * Zooms in the view.
	 */
	void zoomIn() const;

	/**
	 * Zooms out the view.
	 */
	void zoomOut() const;

	/**
	 * Goes back to actual size zoom.
	 */
	void zoomActualSize() const;

	void newWindow() const;

	void deleteCurrentWindow() const;

	void openFile();

	void saveFileAt() const;

	void saveFile() const;

	void addTransitionMode();

	void setTransitionDefaultName();

	void openLinearLayout();

	void openCircularLayout();

	void enableTransitionMode();

	void disableTransitionMode();

	void selectPositionDefaultState();

	void selectPositionInitialState();

	void selectPositionFinalState();

	/**
	 * This function is called each time a properties of the style is changed in the inspector.
     * Updates the properties of the style of the selected object.
	 */
	void setStyleSelectedItem();

	/**
	 * This function is called each time a properties which isn't linked to the style is changed in the inspector.
     * Updates the properties of the selected object.
	 */
	void setPropertiesSelectedItem();

	/**
	 * This function is called each time the user clicks on a delete button (for transitions or states).
	 * Deletes the selected item.
	 * @see StateDrawable
	 * @see TransitionDrawable
	 */
	void deleteSelectedItem();

	/** 
	 * This function is called each time a new item is selected.
	 * Updates the RightDockWidget corresponding to the item selected.
	 * @see StateDockWidget
	 * @see TransitionDockWidget
	 */
	void updatesDockWidget(Selectable &selectedItem);

	/**
	 * This function is called each time the user activates or not the grid.
	 * Update the MenuBar grid check box according to the state of the grid (activated or not).
	 */
	void updateMenuBarGrid();

	/**
	 * This function is called each time the user activates or not the grid.
	 * Update the ToolBar grid check box according to the state of the grid (activated or not).
	 */
	void updateToolBarGrid();

	/**
	 * This function is called each time the user changes the group priority in the GroupDockWidget.
	 * Updates the priority of the group.
	 * @param p is the new priority of the group
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void changeSelectedGroupPriority(int p);

	/**
	 * This function is called each time the user changes the group name in the GroupDockWidget.
	 * Updates the name of the group.
	 * @param name is the new name of the group.
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectedGroupName(const QString &name);

	/**
	 * This function is called each time the user changes the group editable state in the GroupDockWidget.
	 * Updates the 'editable' state of the group.
	 * @param b is the new group 'editable' state (true of false)
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setEditableSelectableGroup(int b);

	/**
	 * This function is called each time the user changes the group applyStyle state in the GroupDockWidget.
	 * Updates the 'applyStyle' state of the group.
	 * Displays or not the group style according to the applyStyle state.
	 * @param state is the new group 'applyStyle' state (true of false)
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setDrawingStateSelectableGroup(int state);

	/**
	 * This function is called each time the user changes the group pen size in the GroupDockWidget.
	 * Updates the pen size of the group.
	 * @param size is the new group pen size
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectableGroupPenSize(int size);

	/**
	 * This function is called each time the user changes the group pen color in the GroupDockWidget.
	 * Updates the pen color of the group.
	 * @see ColorPickerWidget
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectableGroupPenColor();

	/**
	 * This function is called each time the user changes the group brush (background) color in the GroupDockWidget.
	 * Updates the brush (background) color of the group.
	 * @see ColorPickerWidget
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectableGroupBrushColor();

	/**
	 * This function is called each time the user changes the group pen style in the GroupDockWidget.
	 * Updates the pen style of the group.
	 * @see LineWidget
	 * @see LineStyleWidget
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void setSelectableGroupPenStyle();

	/**
	 * This function is called each time the user clicks on the ToolBar button to add a group.
	 * Creates a new group.
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void createStyleGroup();

	/**
	 * This function is called each time the user clicks on the delete button on the GroupDockWidget.
	 * Deletes the current group.
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void deleteStyleGroup();

	/**
	 * This function is called each time the index of the ToolBar group selection box changes.
	 * Switch between EmptyDockWidget (when the index is 0) and the GroupDockWidget (in the other case).
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void switchBetweenEmptyAndGroupDock(int currentIndex);

	/**
	 * This function is called each time the user creates a group or changes groups.
	 * Loads and displays the group properties in the GroupDockWidget.
	 * @see Groupable
	 * @see SelectableGroup
	 */
	void loadGroupDock();

	/**
	 * This function is called each time the index of the ToolBar group selection box changes.
	 * Disables the 'add group' text and button when the index is different from zero.
	 * Enables them in the other case.
	 */
	void setLockAddGroupButton(int i);

	/**
	 * This function is called each time the 'editable' check box state of the GroupDockWidget changes.
	 * Disables the group selection box if the current group is being edited.
	 * Enables it in the other case.
	 */
	void setLockChangeGroupButton(int editable);

	/**
	 * Sets the ToolBar group selection box's index to zero if the current selected group is not begin edited.
	 */
	void refreshGroupIndex();

	/**
	 * This function informs if the selected group is being edited or not.
	 * @return the state of the GroupDockWidget 'editable' check box
	 */
	bool isSelectedGroupEditable();

	/**
	 * This function is called each time the user activates or no the grid or changes its style.
	 * Shows or not the grid and if showed, uses its chosen style.
	 */
	void updateGrid();

	/**
	 * This function is called each time the user wants to quit the application, by clicking on the X button of the window or in the MenuBar, menu 'Fermer'.
	 * Shows a pop-up to confirm the choice of the user, if it confirms the choice, quits the application.
	 */
	void quitApplication();

	void closeTab(int index);

	void tabSelected();

	/** 
     * Updates the QComboBox containing the names of all groups of the current Automata shown.
	 */
	void updateGroupList();
};

#endif // VISUAL_AUTOMATA_AUTOMATA_CONTROLLER_HPP
