#include "automata_window.hpp"

namespace VisualAutomata {

AutomataWindow::AutomataWindow(std::unique_ptr<Automata> automaton) {

	automata_viewport_projection = new AutomataViewport(this);
	automata_viewport_geometry = new AutomataViewport(this);
	this->automaton = std::move(automaton);
	tabWidget = new QTabWidget(this);
	tabWidget->setTabPosition(QTabWidget::South);
	tabWidget->addTab(automata_viewport_projection, "Mode projection");
	tabWidget->addTab(automata_viewport_geometry, "Mode géométrie");
	tabWidget->setTabEnabled(1, false);

	setCentralWidget(tabWidget);
}

AutomataWindow::~AutomataWindow() {
	delete (automata_viewport_projection);
	delete (automata_viewport_geometry);
	delete (tabWidget);
}

AutomataViewport *AutomataWindow::getAutomataViewportProjection() const {
	return automata_viewport_projection;
}

AutomataViewport *AutomataWindow::getAutomataViewportGeometry() const {
	return automata_viewport_geometry;
}

int AutomataWindow::getViewportNumber() const {
	return tabWidget->currentIndex();
}

Automata *AutomataWindow::getAutomata() {
	return automaton.get();
}
}  // namespace VisualAutomata
