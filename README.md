# Visual Automata

This project is made as a PFA (project throughout the year) at ENSEIRB-MATMECA which consists in creating a graphical interface allowing to visualize, edit and create automata in a 2D space.

It is currently licensed under [GNU GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)

## Project members
- Clément Baude
- Aymeric Bled
- Simon Bullot
- David Gond
- Nicolas Le Quec
- Gaëtan Margueritte
- Marin Pau
- Vincent Ridacker

## Notes

The program is made in C++ and uses Qt to display the graphical interface.
Many of its functionalities are yet to be implemented but the basic manipulation
of an automaton is available.


Access to the :
- code documentation is [here](https://davidgond.gitlab.io/visual-automata/) (latest stable version).
- user guide is [here](https://gitlab.com/DavidGond/visual-automata/-/jobs/artifacts/master/raw/docs/visual-automata-userdoc.pdf?job=pages) (direct download)
- technical documentation is [here](https://gitlab.com/DavidGond/visual-automata/-/jobs/artifacts/master/raw/docs/visual-automata-technicaldoc.pdf?job=pages) (direct download)

## Build and run

To build the project, just create a `build` folder and move inside, then run `cmake ..`.

A compiler can be specified with `cmake -DCMAKE_CXX_COMPILER=g++ ..` and replacing `g++` with the desired compiler. Please note that only gcc and clang have been tested.

Once CMake has initiated the build process, just run `make` and it will build the application and the tests.

To just build the application, run `make visual_automata`.

The docs can be built using `make doc`, they will be placed in the `docs` folder at the project root.

The tests can be built using `make alltests`.

The build process may take a few minutes and can be accelerated by passing the `-j X` option to `make`, with `X` the number of jobs to execute in parallel.
